Imports System.Threading
Imports Microsoft.Win32
Imports System.IO

Module basGlobalFunctions

	Public glErrors As New RunErrors
	Public gsErr As String = ""
    Public bLocalTest As Boolean = False
    Public gsTempPath As String = Logging.LogFileLocation.TempDirectory.ToString

    Public gbResetReplacements As Boolean = True
    Public gsLogPath As String = ""

    Public goValueSustitution As New clsValueSubstitutions
    Public goManageProcess As New clsManageProcesses
    Public goTestValues As clsExampleListData

    Public Sub Wait(ByVal ms As Integer)
        Using wh As New ManualResetEvent(False)
            wh.WaitOne(ms)
        End Using
    End Sub

    Public Function MassReplace(ByVal StringIn As String, ByVal ReplaceList As String, ByVal ReplaceWith As String) As String
        Dim sReturn As String = "", sNextReplaceList As String = Mid(ReplaceList, 2), sReplaceChar As String
        Try

            sReplaceChar = Left(ReplaceList, 1)
            If ReplaceWith <> sReplaceChar Then
                sReturn = Replace(StringIn, sReplaceChar, ReplaceWith)
            Else
                sReturn = StringIn
            End If

            If Len(ReplaceList) > 1 Then
                sNextReplaceList = Mid(ReplaceList, 2)
                sReturn = MassReplace(sReturn, sNextReplaceList, ReplaceWith)
            End If

        Catch ex As Exception

            Dim oError As ErrorItem = glErrors.NewError(ex)
            'If bLocalTest Then MsgBox("ERROR " & oError.ToString)

        End Try
        Return sReturn
    End Function

    Public Function StringIsTrue(StrVal As String) As Boolean
        Dim bReturn As Boolean = False
        Try

            If IsNumeric(StrVal) And StrVal <> "" Then
                bReturn = Val(StrVal) <> 0
            Else
                bReturn = (StrVal = "True" Or StrVal = "T" Or StrVal = "Yes" Or StrVal = "Y")
            End If
        Catch ex As Exception
            Dim oError As ErrorItem = glErrors.NewError(ex)
        End Try
        Return bReturn
    End Function

    Public Function Strim(ByVal sString As String, Optional ByVal TrimChar As String = " ") As String
        Try
            Dim iTries As Integer = CInt(Math.Sqrt(sString.Length) + 1)

            For iCnt As Integer = 1 To iTries
                sString = Replace(sString, TrimChar & TrimChar, TrimChar)
            Next

            If Left(sString, 1) = TrimChar Then sString = sString.Substring(2)
            If sString > "" AndAlso Right(sString, 1) = TrimChar Then sString = Left(sString, sString.Length - 1)

        Catch ex As Exception

        End Try

        Return sString

    End Function

    Public Function bytesToString(ByVal lValIn As Long) As String
        Dim iScale As Long = 0, iNewScale As Integer = -1, lTemp As Long
        Dim dNumPart As Double = lValIn, lMin As Long, lMax As Long, sReturn As String = ""

        Try

            Do
                iNewScale = iNewScale + 1
                lTemp = CLng(1024 ^ iNewScale)
                lMin = lTemp * 3 : If lMin < 1024 Then lMin = 0
                lMax = CLng(1024 ^ (iNewScale + 1) * 3)
            Loop While lValIn > lMax

            dNumPart = CDbl(lValIn) / lTemp

            Select Case iNewScale
                Case 0 : sReturn = lValIn & " Byte" & IIf(lValIn > 1, "s", "").ToString
                Case 1 : sReturn = dNumPart.ToString("###0.###") & " Kb"
                Case 2 : sReturn = dNumPart.ToString("###0.###") & " Mb"
                Case Else : sReturn = dNumPart.ToString("######0.###") & " Gb"
            End Select


        Catch ex As Exception

            Dim oError As ErrorItem = glErrors.NewError(ex)
            'If bLocalTest Then MsgBox("ERROR " & oError.ToString)

        End Try

        Return sReturn
    End Function

    Public Function BytesFromString(ByVal sValIn As String) As Long
        Dim lReturn As Long = 0, dVal As Double, sSize As String
        Dim oRegex As New System.Text.RegularExpressions.Regex(My.Settings.RegexFileSize)
        Dim oRegexMatches As System.Text.RegularExpressions.Match

        Try
            sValIn = Replace(Trim(UCase(sValIn)), " ", "")
            If sValIn = "" Then
                dVal = 0
            Else
                If IsNumeric(sValIn) Then
                    dVal = CDbl(sValIn)
                Else
                    If oRegex.IsMatch(sValIn) Then
                        oRegexMatches = oRegex.Match(sValIn)
                        If IsNumeric(oRegexMatches.Groups("Value").Value) Then
                            dVal = CDbl(oRegexMatches.Groups("Value").Value)
                            sSize = oRegexMatches.Groups("Quantifier").Value

                            If Left(sSize, 1) = "K" Then
                                dVal = dVal * 1024
                            ElseIf Left(sSize, 1) = "M" Then
                                dVal = dVal * 1024 ^ 2
                            ElseIf Left(sSize, 1) = "G" Then
                                dVal = dVal * 1024 ^ 3
                            End If

                        End If
                    End If
                End If
            End If

            lReturn = CLng(dVal)

        Catch ex As Exception

            Dim oError As ErrorItem = glErrors.NewError(ex)
            'If bLocalTest Then MsgBox("ERROR " & oError.ToString)

        End Try

        Return lReturn
    End Function


    Public Sub LogFileEntry(ByVal sText As String, Optional ByVal ForErrorLogging As Boolean = False)
        Dim oLogDirectory As IO.DirectoryInfo, oLogFile As IO.FileInfo
        Dim oWriter As IO.StreamWriter, sLogPath As String

        Try
            If gsLogPath > "" Then
                sLogPath = ApplyGeneralTemplate(gsLogPath, Now)
                oLogFile = New IO.FileInfo(sLogPath)
                oLogDirectory = New IO.DirectoryInfo(oLogFile.DirectoryName)

                If Not oLogDirectory.Exists Then oLogDirectory.Create()

                If oLogDirectory.Exists Then
                    oWriter = oLogFile.AppendText()
                    oWriter.WriteLine(sText)
                    oWriter.Flush()
                    oWriter.Close()
                    oWriter.Dispose()
                End If
                oLogDirectory = Nothing
            End If

        Catch ex As Exception

            If Not ForErrorLogging Then
                Dim oError As ErrorItem = glErrors.NewError(ex)
                'If bLocalTest Then MsgBox("ERROR " & oError.ToString)
            Else
                'WatchFileConsole.txtCurrentActivity.Text = ex.ToString & vbCrLf & WatchFileConsole.txtCurrentActivity.Text
            End If

        End Try

    End Sub

    Public Function ApplyGeneralTemplate(ByVal sStringIn As String, ByVal oDate As Date) As String
        Dim sReturn As String = sStringIn
        Dim sSourceName As String = "", sDestName As String = ""
        Dim sRelativeSourceFolder As String = "", sRelativeSourceFile As String = ""
        Dim oValReplacer As clsValueSubstitutions, oVals As New Hashtable

        Try

            If oDate.Ticks = 0 Then oDate = Now

            oValReplacer = New clsValueSubstitutions
            oValReplacer.dPassedDate = oDate
            oValReplacer.OriginalString = sStringIn
            sReturn = oValReplacer.Result
            oValReplacer = Nothing

        Catch ex As Exception

            Dim oError As ErrorItem = glErrors.NewError(ex)
            'If bLocalTest Then MsgBox("ERROR " & oError.ToString)

        End Try

        Return sReturn
    End Function

    Public Function GetMIMEType(filepath As String) As String
        Dim regPerm As Security.Permissions.RegistryPermission = New Security.Permissions.RegistryPermission(Security.Permissions.RegistryPermissionAccess.Read, "\\HKEY_CLASSES_ROOT")
        Dim classesRoot As RegistryKey = Registry.ClassesRoot
        Dim fi = New FileInfo(filepath)
        Dim dotExt As String = LCase(fi.Extension)
        Dim typeKey As RegistryKey = classesRoot.OpenSubKey("MIME\Database\Content Type")
        Dim keyname As String, sReturn As String = "application/octet-stream"

        For Each keyname In typeKey.GetSubKeyNames()
            Dim curKey As RegistryKey = classesRoot.OpenSubKey("MIME\Database\Content Type\" & keyname)
            If curKey.GetValue("Extension").ToString.ToLower = dotExt.ToLower Then
                sReturn = keyname
                Exit For
            End If
        Next
        Return sReturn
    End Function





    Friend Sub ReportErrorGlobal(oErrItem As ErrorItem)
        Try
            MsgBox(Replace(Replace(oErrItem.ToString, "<br />", vbCrLf), "<br/>", vbCrLf))
        Catch ex As Exception
            MsgBox(ex.ToString)

        End Try
    End Sub


    Friend Sub RecordErrorGlobal(exIn As Exception, ExtraDets As ArrayList)
        Try

            Dim oErr As ErrorItem = glErrors.NewError(exIn)
            If ExtraDets IsNot Nothing AndAlso ExtraDets.Count > 0 Then oErr.sExtraDetail &= String.Join("<br/>", ExtraDets)
            gsErr &= "<br /><br />" & Replace(oErr.ToString, vbCrLf, "<br />")

            ReportErrorGlobal(oErr)

        Catch ex2 As Exception

        End Try

    End Sub

End Module
