﻿Imports System.IO
Imports System.Windows
Imports System.Windows.Controls
Imports System.Windows.Threading
Imports System.Threading
Imports System.Text.RegularExpressions

Public Class clsManageProcesses
    Protected Friend stopProcessing As Boolean = False

    Protected Friend Event ErrorOccured(oEvent As ErrorItem)
    Protected Friend Event ProcessFinished()
    Protected Friend Event ProcessX1Finished()
    Protected Friend Event ProcessX2Finished()
    Protected Friend Event ProcessX3Finished()
    Protected Friend Event ProcessX4Finished()

    'Protected Friend Delegate Sub NoArgDelegate()
    'Protected Friend Delegate Sub OneArgDelegate(arg As Object)
    'Protected Friend Delegate Sub TwoArgDelegate(arg1 As Object, arg2 As Object)
    'Protected Friend Delegate Sub ThreeArgDelegate(arg1 As Object, arg2 As Object, arg3 As Object)
    'Protected Friend Delegate Sub FourArgDelegate(arg1 As Object, arg2 As Object, arg3 As Object, arg4 As Object)
    'Protected Friend Delegate Sub FiveArgDelegate(arg1 As Object, arg2 As Object, arg3 As Object, arg4 As Object, arg5 As Object)

    Protected Friend oCallingForm As MainWindow
    'Protected Friend oControlWithDelegate As Control
    Private oActiveProcess As MainWindow.NoArgDelegate
    'Private oActiveProcessOneArg As MainWindow.OneArgDelegate
    Protected Friend oControlToPopulate As Control

    Public WithEvents oExampleRecords As clsExampleListData

    Protected oThread As Thread

    Dim sLastShow As Date


    Protected Friend Sub CancelProcess()
        Me.stopProcessing = True
        If oExampleRecords IsNot Nothing Then oExampleRecords.bAbort = True
    End Sub


    ' ========================================================================
    ' A threaded process - this time with one initial argument passed 
    ' of an instance oSomeClass of class clsExampleListData 
    ' Feedback is, in this case, updated via DoUpdateResponseX1InDataClass
    ' which accepts one argument and updates the calling form
    ' ========================================================================


    Protected Friend Sub ActivateProcessX1(oControlToUse As Control, oSomeClass As Object)
        Try
            oExampleRecords = New clsExampleListData(10000000)
            Me.stopProcessing = False
            If oSomeClass Is Nothing Then
                oSomeClass = oExampleRecords
            Else
                oExampleRecords = DirectCast(oSomeClass, clsExampleListData)
            End If

            Dim oActiveProcessOneArg As New MainWindow.OneArgDelegate(AddressOf MainProcessToDoProcessX1)
            ' The one argument is the processing class of oSomeClass

            oActiveProcessOneArg.BeginInvoke(oSomeClass, Nothing, Nothing)

        Catch ex As Exception
            Dim oErrItem As ErrorItem : oErrItem = glErrors.NewError(ex)
            ReportError(oErrItem)
        End Try
    End Sub


    Protected Friend Sub MainProcessToDoProcessX1(oSomeDataClass As clsExampleListData)
        ' Called to do the processing in a separate thread
        Try

            If oSomeDataClass Is Nothing Then oSomeDataClass = oExampleRecords Else oExampleRecords = oSomeDataClass

            AddHandler oSomeDataClass.NewPrimeFound, AddressOf oExampleRecords_NewPrimeFound

            sResult = oSomeDataClass.ProcessDataToResult

            DoUpdateResponseX1InDataClass(sResult) ' Use this function to provide feedback to the calling class of clsManageProcess

        Catch ex As Exception
            Dim oErrItem As ErrorItem : oErrItem = glErrors.NewError(ex)
            ReportError(oErrItem)

        End Try
    End Sub


    Protected Friend Sub DoUpdateResponseX1InDataClass(sResult As String)
        ' IMPORTANT - Don't call the processing in this sub, must be using the results only
        Try

            If oCallingForm Is Nothing OrElse oCallingForm.Dispatcher.CheckAccess() Then
                ' Running in this current thread to access the UI
                UpdateUIOnX1Response(sResult)
            Else
                ' Need to pass the call of this function onto the UI thread
                ' IMPOTANT : Pass the simple value result, not the object.
                ' If the object is passed, then the work is done on the UI thread to get any computed result, making threading pointless

                oCallingForm.Dispatcher.BeginInvoke(DispatcherPriority.Normal, New MainWindow.OneArgDelegate(AddressOf Me.DoUpdateResponseX1InDataClass), sResult)

            End If

        Catch ex As Exception
            Dim oErrItem As ErrorItem : oErrItem = glErrors.NewError(ex)
            ReportError(oErrItem)

        End Try
    End Sub



    Protected Friend Sub UpdateUIOnX1Response(sResult As String)
        Try
            ' Since the object is the same as the locally stored one, can get to the data from that as well as what is passed by delegate (Dispatcher) back
            oCallingForm.txtRecordCount.Text = oExampleRecords.Item(oExampleRecords.Keys(oExampleRecords.Count - 1)).sValue
            oCallingForm.txtResult.Text = sResult
            oCallingForm.cmdActivate.IsEnabled = True

            RaiseEvent ProcessFinished()
            RaiseEvent ProcessX1Finished()

        Catch ex As Exception
            Dim oErrItem As ErrorItem : oErrItem = glErrors.NewError(ex)
            ReportError(oErrItem)
        End Try

    End Sub



    Protected Friend Sub oExampleRecords_NewPrimeFound(iNumFound As Integer) Handles oExampleRecords.NewPrimeFound
        ' This event should be on same thread as the calling process on the handle associated class
        ' if not on UI thread, then call with delegate to UI thread to update UI
        If oCallingForm Is Nothing OrElse oCallingForm.Dispatcher.CheckAccess() Then
            ' Using a little trick to prevent the UI being flooded (1 Millisecond = 10,000 ticks, 1 second = 10,000,000 ticks)
            ' The value used is 1/10th of a second
            If Now.Ticks - sLastShow.Ticks > 1000000 Then
                oCallingForm.txtRecordCount.Text = iNumFound.ToString
                oCallingForm.txtRecordSquare.Text = oExampleRecords.lLastSquare.ToString
                sLastShow = Now()
            End If
        Else
            ' Need to call me on the UI thread. Same time interval as above
            If Now.Ticks - sLastShow.Ticks > 1000000 Then
                oCallingForm.Dispatcher.BeginInvoke(DispatcherPriority.Normal, New MainWindow.OneArgDelegate(AddressOf Me.oExampleRecords_NewPrimeFound), iNumFound)
            End If
        End If

    End Sub


    ' ========================================================================
    ' Other threaded process - this time no arguments passed so 
    ' all initial data interaction is via available classes and variables
    ' Feedback is, in this case, a single text box invoked by a separate routine
    ' so as to avoiud repeating calculations of the data class. 
    ' ========================================================================


    Protected Friend Sub ActivateProcessX2(oControlToPopulateIn As Control)
        ' Populate combobox control cmbTable
        Try

            Me.stopProcessing = False
            Me.oControlToPopulate = oControlToPopulateIn

            oExampleRecords = New clsExampleListData(10000000)

            oActiveProcess = New MainWindow.NoArgDelegate(AddressOf Me.ProcessingForDataThreadX2)
            oActiveProcess.BeginInvoke(Nothing, Nothing)

        Catch ex As Exception
            Dim oErrItem As ErrorItem : oErrItem = glErrors.NewError(ex)
            ReportError(oErrItem)
            'oControlWithDelegate.Dispatcher.BeginInvoke(DispatcherPriority.Normal, New OneArgDelegate(AddressOf Me.ReportError), oErrItem)
        End Try
    End Sub



    Protected Friend Sub ProcessingForDataThreadX2()
        'Dim oCombo As ComboBox, oBinding As Binding
        Dim oTxt As TextBox
        Try
            oTxt = oCallingForm.txtRecordCount
            DoSomeDataActionsX2(oTxt)

        Catch ex As Exception
            Dim oErrItem As ErrorItem : oErrItem = glErrors.NewError(ex)
            ReportError(oErrItem)

        End Try
    End Sub


    Protected Friend Sub DoSomeDataActionsX2(oTextControlToPopulate As Control)
        Dim sMessage As String
        Try
            sMessage = oExampleRecords.ProcessDataToResult().ToString

            ' Feedback is not necessary here, but can be done in a sub

            DoFeedbackDataActionsX2(oTextControlToPopulate, sMessage)

        Catch ex As Exception
            Dim oErrItem As ErrorItem : oErrItem = glErrors.NewError(ex)
            ReportError(oErrItem)

        End Try
    End Sub


    Protected Friend Sub DoFeedbackDataActionsX2(oTextControlToPopulate As Control, sMessage As String)
        Dim oTxt As TextBox

        Try
            If oCallingForm Is Nothing OrElse oCallingForm.Dispatcher.CheckAccess() Then
                oTxt = DirectCast(oTextControlToPopulate, TextBox)

                If TypeOf (oCallingForm) Is MainWindow And oTxt Is Nothing Then
                    oCallingForm = DirectCast(oCallingForm, MainWindow)
                    oTxt = oCallingForm.txtResult
                End If
                oTxt.Text = sMessage

                RaiseEvent ProcessFinished()
                RaiseEvent ProcessX2Finished()

                ' Running in this thread to access the UI
            Else
                ' Need to call me on the UI thread
                oCallingForm.Dispatcher.BeginInvoke(DispatcherPriority.Normal, New MainWindow.TwoArgDelegate(AddressOf Me.DoFeedbackDataActionsX2))
            End If


        Catch ex As Exception
            Dim oErrItem As ErrorItem : oErrItem = glErrors.NewError(ex)
            ReportError(oErrItem)

        End Try
    End Sub


    ' ========================================================================
    ' Other threaded process - this time no arguments passed so 
    ' all initial data interaction is via available classes and variables
    ' Feedback is, in this case, a single text box invoked by a separate routine
    ' so as to avoiud repeating calculations of the data class. 
    ' ========================================================================



    Protected Friend Sub DoSomeDataBindingX3(oControlToPopulate As Control)
        Try

            If oExampleRecords Is Nothing Then oExampleRecords = New clsExampleListData(10000000)
            oExampleRecords.ProcessDataToResult()

            DoFeedbackDataActionsX3(oControlToPopulate)

        Catch ex As Exception
            Dim oErrItem As ErrorItem : oErrItem = glErrors.NewError(ex)
            ReportError(oErrItem)

        End Try
    End Sub



    Protected Friend Sub DoFeedbackDataActionsX3(oTextControlToPopulate As Control)
        Try

            If oCallingForm Is Nothing OrElse oCallingForm.Dispatcher.CheckAccess() Then

                oCombo = DirectCast(oControlToPopulate, ComboBox)
                oBinding = New Binding()
                oBinding.Mode = BindingMode.OneTime
                oBinding.Source = oExampleRecords.Values ' @@@ Very important to use values for binding, not the list @@@

                BindingOperations.SetBinding(oCombo, ComboBox.ItemsSourceProperty, oBinding)

                oCombo.IsEnabled = oExampleRecords.Count > 0
                oCombo.DisplayMemberPath = "sValue2"

                ' Running in this thread to access the UI

                RaiseEvent ProcessFinished()
                RaiseEvent ProcessX3Finished()

            Else
                ' Need to call me on the UI thread
                oCallingForm.Dispatcher.BeginInvoke(DispatcherPriority.Normal, New MainWindow.OneArgDelegate(AddressOf Me.DoSomeDataBindingX3), oControlToPopulate)
            End If


        Catch ex As Exception

        End Try
    End Sub



    ' ========================================================================
    ' Other threaded process - this time no arguments passed so 
    ' all initial data interaction is via available classes and variables
    ' Feedback is, in this case, a single text box invoked by a separate routine
    ' so as to avoiud repeating calculations of the data class. 
    ' ========================================================================



    Protected Friend Sub DoSomeCompletedDataActionsX4()
        Try

            ' Do a variety of processing of things here without any form feedback until finished - if wanted

            DoFeedbackCompletedDataActionsX4()

        Catch ex As Exception
            Dim oErrItem As ErrorItem : oErrItem = glErrors.NewError(ex)
            ReportError(oErrItem)

        End Try
    End Sub


    Protected Friend Sub DoFeedbackCompletedDataActionsX4()
        Try
            If oCallingForm Is Nothing OrElse oCallingForm.Dispatcher.CheckAccess() Then
                oCallingForm.VerifyControls()

                RaiseEvent ProcessFinished()
                RaiseEvent ProcessX4Finished()

            Else
                oCallingForm.Dispatcher.BeginInvoke(DispatcherPriority.Normal, New MainWindow.NoArgDelegate(AddressOf oCallingForm.VerifyControls), Nothing)
            End If

        Catch ex As Exception
            Dim oErrItem As ErrorItem : oErrItem = glErrors.NewError(ex)
            ReportError(oErrItem)

        End Try
    End Sub


    ' ========================================================================
    ' Delegate based feedback on error
    ' Currently set to only display a message box but can do anything needed
    ' ========================================================================


    Protected Friend Sub ReportError(oErrItem As ErrorItem)
        Try
            If oCallingForm Is Nothing OrElse oCallingForm.Dispatcher.CheckAccess() Then
                ' Running in this thread to access the UI
                'MsgBox(oErrItem.ToString)
                RaiseEvent ErrorOccured(oErrItem)
            Else
                ' Need to call me on the UI thread
                oCallingForm.Dispatcher.BeginInvoke(DispatcherPriority.Normal, New MainWindow.OneArgDelegate(AddressOf Me.ReportError), oErrItem)
            End If
        Catch ex As Exception
            MsgBox(ex.ToString)

        End Try
    End Sub

End Class
