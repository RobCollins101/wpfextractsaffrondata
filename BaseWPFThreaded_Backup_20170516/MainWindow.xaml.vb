﻿Imports BaseWPFThreadedTest1c

Class MainWindow

    Protected Friend Delegate Sub NoArgDelegate()
    Protected Friend Delegate Sub OneArgDelegate(arg As Object)
    Protected Friend Delegate Sub TwoArgDelegate(arg1 As Object, arg2 As Object)
    Protected Friend Delegate Sub ThreeArgDelegate(arg1 As Object, arg2 As Object, arg3 As Object)
    Protected Friend Delegate Sub FourArgDelegate(arg1 As Object, arg2 As Object, arg3 As Object, arg4 As Object)
    Protected Friend Delegate Sub FiveArgDelegate(arg1 As Object, arg2 As Object, arg3 As Object, arg4 As Object, arg5 As Object)


    ' see http://www.codeproject.com/Articles/10311/What-s-up-with-BeginInvoke 
    Sub VerifyControls()
        Throw New NotImplementedException
    End Sub



    Private Sub Button_Click(sender As Object, e As RoutedEventArgs)

    End Sub



    Private Sub oMgProcess_ErrorOccured(oEvent As ErrorItem)

        MsgBox(oEvent.ToString)

    End Sub



    Private Sub oMgProcess_ProcessFinished()

        cmdAbort.Visibility = Visibility.Hidden
        cmdActivate.IsEnabled = True

    End Sub



    Private Sub cmdAbort_Click(sender As Object, e As RoutedEventArgs) Handles cmdAbort.Click

        goManageProcess.CancelProcess()

    End Sub



    Private Sub cmdActivate_Click(sender As Object, e As RoutedEventArgs) Handles cmdActivate.Click

        cmdActivate.IsEnabled = False
        cmdAbort.Visibility = Visibility.Visible

        txtResult.Text = ""

        AddHandler goManageProcess.ErrorOccured, AddressOf oMgProcess_ErrorOccured
        AddHandler goManageProcess.ProcessFinished, AddressOf oMgProcess_ProcessFinished

        goManageProcess.oCallingForm = Me
        'goManageProcess.oControlWithDelegate = txtResult
        goManageProcess.ActivateProcessX1(Me, goTestValues)

    End Sub
End Class
