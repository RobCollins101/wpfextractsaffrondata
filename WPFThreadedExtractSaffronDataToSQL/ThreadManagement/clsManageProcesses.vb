﻿Imports System.IO
Imports System.Windows
Imports System.Windows.Controls
Imports System.Windows.Threading
Imports System.Threading
Imports System.Text.RegularExpressions
Imports WPFThreadedBatchProcessing
Imports WPFExtractSaffronData

Public Class clsManageProcesses
    Protected Friend stopProcessing As Boolean = False

    Protected Friend Event ErrorOccured(oEvent As ErrorItem)
    Protected Friend Event ProcessFinished()
    Protected Friend Event ProcessFailed(FaileMessage As String)


    'Protected Friend Event ProcessX1Finished()
    'Protected Friend Event ProcessX2Finished()
    'Protected Friend Event ProcessX3Finished()
    'Protected Friend Event ProcessX4Finished()

    'Protected Friend Delegate Sub NoArgDelegate()
    'Protected Friend Delegate Sub OneArgDelegate(arg As Object)
    'Protected Friend Delegate Sub TwoArgDelegate(arg1 As Object, arg2 As Object)
    'Protected Friend Delegate Sub ThreeArgDelegate(arg1 As Object, arg2 As Object, arg3 As Object)
    'Protected Friend Delegate Sub FourArgDelegate(arg1 As Object, arg2 As Object, arg3 As Object, arg4 As Object)
    'Protected Friend Delegate Sub FiveArgDelegate(arg1 As Object, arg2 As Object, arg3 As Object, arg4 As Object, arg5 As Object)

    Protected Friend oCallingForm As MainWindow
    'Protected Friend oControlWithDelegate As Control
    Private oActiveProcess As MainWindow.NoArgDelegate
    'Private oActiveProcessOneArg As MainWindow.OneArgDelegate
    Protected Friend oControlToPopulate As Control

    'Public WithEvents oExampleRecords As clsExampleListData
    Public WithEvents oTransferProcess As clsExtractData


    Protected oThread As Thread

    Dim sLastShow As Date



    Protected Friend Sub CancelProcess()
        Me.stopProcessing = True
        'If oExampleRecords IsNot Nothing Then oExampleRecords.bAbort = True
        If oTransferProcess IsNot Nothing Then oTransferProcess.bAbort = True
    End Sub



    ' ========================================================================
    ' A threaded process - this time with one initial argument passed 
    ' of an instance oSomeClass of class clsExampleListData 
    ' Feedback is, in this case, updated via DoUpdateResponseX1InDataClass
    ' which accepts one argument and updates the calling form
    ' ========================================================================


    Protected Friend Sub ActivateProcessTransferUVFiles(Optional oSomeClass As Object = Nothing)
        Try
            'oExampleRecords = New clsExampleListData(10000000)
            oTransferProcess = New clsExtractData()
            Me.stopProcessing = False
            If oSomeClass Is Nothing Then
                oSomeClass = New clsExtractData
                'oSomeClass = oExampleRecords
            Else
                oTransferProcess = DirectCast(oSomeClass, clsExtractData)
                'oExampleRecords = DirectCast(oSomeClass, clsExampleListData)
            End If

            Dim oActiveProcessOneArg As New MainWindow.OneArgDelegate(AddressOf MainProcessToDoProcessTransferUVFiles)
            ' The one argument is the processing class of oTransferProcess

            oActiveProcessOneArg.BeginInvoke(oTransferProcess, Nothing, Nothing)

        Catch ex As Exception
            Dim oErrItem As ErrorItem : oErrItem = glErrors.NewError(ex)
            ReportError(oErrItem)
        End Try
    End Sub



    Protected Friend Sub MainProcessToDoProcessTransferUVFiles(oSomeDataClass As clsExtractData)
        ' Called to do the processing in a separate thread
        Dim sResult As String = ""
        Try

            'If oSomeDataClass Is Nothing Then oSomeDataClass = oExampleRecords Else oExampleRecords = oSomeDataClass
            If oSomeDataClass Is Nothing Then oSomeDataClass = oTransferProcess Else oTransferProcess = oSomeDataClass

            RemoveHandler oSomeDataClass.FileCompleted, AddressOf oTransferProcess_FileCompleted
            RemoveHandler oSomeDataClass.FileFailed, AddressOf oTransferProcess_FileFailed
            RemoveHandler oSomeDataClass.FileStarted, AddressOf oTransferProcess_FileStarted
            RemoveHandler oSomeDataClass.ProcessCompleted, AddressOf oTransferProcess_ProcessCompleted
            RemoveHandler oSomeDataClass.Processfailed, AddressOf oTransferProcess_Processfailed
            RemoveHandler oSomeDataClass.UpdateStepCount, AddressOf oTransferProcess_UpdatePercentagePoint

            AddHandler oSomeDataClass.FileCompleted, AddressOf oTransferProcess_FileCompleted
            AddHandler oSomeDataClass.FileFailed, AddressOf oTransferProcess_FileFailed
            AddHandler oSomeDataClass.FileStarted, AddressOf oTransferProcess_FileStarted
            AddHandler oSomeDataClass.ProcessCompleted, AddressOf oTransferProcess_ProcessCompleted
            AddHandler oSomeDataClass.Processfailed, AddressOf oTransferProcess_Processfailed
            AddHandler oSomeDataClass.UpdateStepCount, AddressOf oTransferProcess_UpdatePercentagePoint


            sResult = oSomeDataClass.ProcessDataToResult

            DoUpdateResponseProcessingComplete(sResult) ' Use this function to provide feedback to the calling class of clsManageProcess

        Catch ex As Exception
            Dim oErrItem As ErrorItem : oErrItem = glErrors.NewError(ex)
            ReportError(oErrItem)

        End Try
    End Sub



    Protected Friend Sub DoUpdateResponseProcessingComplete(sResult As String)
        ' IMPORTANT - Don't call the processing in this sub, must be using the results only
        Try

            If oCallingForm Is Nothing OrElse oCallingForm.Dispatcher.CheckAccess() Then
                ' Running in this current thread to access the UI
                'UpdateUIOnProcessingComplete(sResult)
            Else
                ' Need to pass the call of this function onto the UI thread
                ' IMPOTANT : Pass the simple value result, not the object.
                ' If the object is passed, then the work is done on the UI thread to get any computed result, making threading pointless

                oCallingForm.Dispatcher.BeginInvoke(DispatcherPriority.Normal, New MainWindow.OneArgDelegate(AddressOf Me.DoUpdateResponseProcessingComplete), sResult)

            End If

        Catch ex As Exception
            Dim oErrItem As ErrorItem : oErrItem = glErrors.NewError(ex)
            ReportError(oErrItem)

        End Try
    End Sub



    Protected Friend Sub UpdateUIOnProcessingComplete(sResult As String)
        Try
            ' Since the object is the same as the locally stored one, can get to the data from that as well as what is passed by delegate (Dispatcher) back
            '        oCallingForm.prgProgress.Value = oCallingForm.prgProgress.Maximum
            '        oCallingForm.txtRecordMax.Text = CallingObject.iNoOfRecords.ToString
            '        oCallingForm.txtRecordCount.Text = CallingObject.iNoOfRecords.ToString
            '        oCallingForm.txtMessage.Text &= vbCrLf & "File " & sFileName & " finished" & vbCrLf &
            '            "Data file " & CallingObject.sCurrFileName & " is at " & CallingObject.DestinationFolder


            '        RaiseEvent ProcessFinished()
            '        RaiseEvent ProcessX1Finished()

        Catch ex As Exception
            Dim oErrItem As ErrorItem : oErrItem = glErrors.NewError(ex)
            ReportError(oErrItem)
        End Try

    End Sub


    ' ========================================================================
    ' Other threaded process - this time no arguments passed so 
    ' all initial data interaction is via available classes and variables
    ' Feedback is, in this case, a single text box invoked by a separate routine
    ' so as to avoiud repeating calculations of the data class. 
    ' ========================================================================



    'Protected Friend Sub ActivateProcessX2(oControlToPopulateIn As Control)
    '    ' Populate combobox control cmbTable
    '    Try

    '        Me.stopProcessing = False
    '        Me.oControlToPopulate = oControlToPopulateIn

    '        oExampleRecords = New clsExampleListData(10000000)

    '        oActiveProcess = New MainWindow.NoArgDelegate(AddressOf Me.ProcessingForDataThreadX2)
    '        oActiveProcess.BeginInvoke(Nothing, Nothing)

    '    Catch ex As Exception
    '        Dim oErrItem As ErrorItem : oErrItem = glErrors.NewError(ex)
    '        ReportError(oErrItem)
    '        'oControlWithDelegate.Dispatcher.BeginInvoke(DispatcherPriority.Normal, New OneArgDelegate(AddressOf Me.ReportError), oErrItem)
    '    End Try
    'End Sub



    'Protected Friend Sub ProcessingForDataThreadX2()
    '    'Dim oCombo As ComboBox, oBinding As Binding
    '    Dim oTxt As TextBox
    '    Try
    '        oTxt = oCallingForm.txtRecordCount
    '        DoSomeDataActionsX2(oTxt)

    '    Catch ex As Exception
    '        Dim oErrItem As ErrorItem : oErrItem = glErrors.NewError(ex)
    '        ReportError(oErrItem)

    '    End Try
    'End Sub



    'Protected Friend Sub DoSomeDataActionsX2(oTextControlToPopulate As Control)
    '    Dim sMessage As String
    '    Try
    '        sMessage = oExampleRecords.ProcessDataToResult().ToString

    '        ' Feedback is not necessary here, but can be done in a sub

    '        DoFeedbackDataActionsX2(oTextControlToPopulate, sMessage)

    '    Catch ex As Exception
    '        Dim oErrItem As ErrorItem : oErrItem = glErrors.NewError(ex)
    '        ReportError(oErrItem)

    '    End Try
    'End Sub



    'Protected Friend Sub DoFeedbackDataActionsX2(oTextControlToPopulate As Control, sMessage As String)
    '    Dim oTxt As TextBox

    '    Try
    '        If oCallingForm Is Nothing OrElse oCallingForm.Dispatcher.CheckAccess() Then
    '            oTxt = DirectCast(oTextControlToPopulate, TextBox)

    '            If TypeOf (oCallingForm) Is MainWindow And oTxt Is Nothing Then
    '                oCallingForm = DirectCast(oCallingForm, MainWindow)
    '                oTxt = oCallingForm.txtResult
    '            End If
    '            oTxt.Text = sMessage

    '            RaiseEvent ProcessFinished()
    '            RaiseEvent ProcessX2Finished()

    '            ' Running in this thread to access the UI
    '        Else
    '            ' Need to call me on the UI thread
    '            oCallingForm.Dispatcher.BeginInvoke(DispatcherPriority.Normal, New MainWindow.TwoArgDelegate(AddressOf Me.DoFeedbackDataActionsX2))
    '        End If


    '    Catch ex As Exception
    '        Dim oErrItem As ErrorItem : oErrItem = glErrors.NewError(ex)
    '        ReportError(oErrItem)

    '    End Try
    'End Sub


    '' ========================================================================
    '' Other threaded process - this time no arguments passed so 
    '' all initial data interaction is via available classes and variables
    '' Feedback is, in this case, a populkation into a combo invoked by a separate routine
    '' so as to avoiud repeating calculations of the data class. 
    '' ========================================================================



    'Protected Friend Sub DoSomeDataBindingX3(oControlToPopulate As Control)
    '    Try

    '        If oExampleRecords Is Nothing Then oExampleRecords = New clsExampleListData(10000000)
    '        oExampleRecords.ProcessDataToResult()

    '        DoFeedbackDataActionsX3(oControlToPopulate)

    '    Catch ex As Exception
    '        Dim oErrItem As ErrorItem : oErrItem = glErrors.NewError(ex)
    '        ReportError(oErrItem)

    '    End Try
    'End Sub



    'Protected Friend Sub DoFeedbackDataActionsX3(oTextControlToPopulate As Control)
    '    Try

    '        If oCallingForm Is Nothing OrElse oCallingForm.Dispatcher.CheckAccess() Then

    '            'oCombo = DirectCast(oControlToPopulate, ComboBox)
    '            'oBinding = New Binding()
    '            'oBinding.Mode = BindingMode.OneTime
    '            'oBinding.Source = oExampleRecords.Values ' @@@ Very important to use values for binding, not the list @@@

    '            'BindingOperations.SetBinding(oCombo, ComboBox.ItemsSourceProperty, oBinding)

    '            'oCombo.IsEnabled = oExampleRecords.Count > 0
    '            'oCombo.DisplayMemberPath = "sValue2"

    '            ' Running in this thread to access the UI

    '            RaiseEvent ProcessFinished()
    '            RaiseEvent ProcessX3Finished()

    '        Else
    '            ' Need to call me on the UI thread
    '            oCallingForm.Dispatcher.BeginInvoke(DispatcherPriority.Normal, New MainWindow.OneArgDelegate(AddressOf Me.DoSomeDataBindingX3), oControlToPopulate)
    '        End If


    '    Catch ex As Exception

    '    End Try
    'End Sub



    '' ========================================================================
    '' Other threaded process - this time no arguments passed so 
    '' all initial data interaction is via available classes and variables
    '' Feedback is, in this case, a single text box invoked by a separate routine
    '' so as to avoiud repeating calculations of the data class. 
    '' ========================================================================



    'Protected Friend Sub DoSomeCompletedDataActionsX4()
    '    Try

    '        ' Do a variety of processing of things here without any form feedback until finished - if wanted

    '        DoFeedbackCompletedDataActionsX4()

    '    Catch ex As Exception
    '        Dim oErrItem As ErrorItem : oErrItem = glErrors.NewError(ex)
    '        ReportError(oErrItem)

    '    End Try
    'End Sub



    'Protected Friend Sub DoFeedbackCompletedDataActionsX4()
    '    Try
    '        If oCallingForm Is Nothing OrElse oCallingForm.Dispatcher.CheckAccess() Then
    '            oCallingForm.VerifyControls()

    '            RaiseEvent ProcessFinished()
    '            RaiseEvent ProcessX4Finished()

    '        Else
    '            oCallingForm.Dispatcher.BeginInvoke(DispatcherPriority.Normal, New MainWindow.NoArgDelegate(AddressOf oCallingForm.VerifyControls), Nothing)
    '        End If

    '    Catch ex As Exception
    '        Dim oErrItem As ErrorItem : oErrItem = glErrors.NewError(ex)
    '        ReportError(oErrItem)

    '    End Try
    'End Sub



    ' ========================================================================
    ' Delegate based feedback on error
    ' Currently set to only display a message box but can do anything needed
    ' ========================================================================



    Protected Friend Sub ReportError(oErrItem As ErrorItem)
        Try
            If oCallingForm Is Nothing OrElse oCallingForm.Dispatcher.CheckAccess() Then
                ' Running in this thread to access the UI
                'MsgBox(oErrItem.ToString)
                RaiseEvent ErrorOccured(oErrItem)
            Else
                ' Need to call me on the UI thread
                oCallingForm.Dispatcher.BeginInvoke(DispatcherPriority.Normal, New MainWindow.OneArgDelegate(AddressOf Me.ReportError), oErrItem)
            End If
        Catch ex As Exception
            Dim oErrItem2 As ErrorItem : oErrItem2 = glErrors.NewError(ex)
            ReportError(oErrItem2)

        End Try
    End Sub

    Private Sub oTransferProcess_FileCompleted(CallingObject As clsExtractData, sFileName As String) Handles oTransferProcess.FileCompleted

        Try

            If oCallingForm Is Nothing OrElse oCallingForm.Dispatcher.CheckAccess() Then
                ' Running in this thread to access the UI
                'MsgBox(oErrItem.ToString)
                oCallingForm.lblFilenameInBar.Content = sFileName & " Done"
                oCallingForm.prgProgress.Value = oCallingForm.prgProgress.Maximum
                oCallingForm.txtRecordMax.Text = CallingObject.iNoOfRecords.ToString
                oCallingForm.txtRecordCount.Text = CallingObject.iNoOfRecords.ToString

                'oCallingForm.AddToTextLogArea(Now.ToString("T") & " : File " & sFileName & " finished")

                'oCallingForm.txtMessage.Text &= vbCrLf & Now.ToString("T") & " : File " & sFileName & " finished" & vbCrLf &
                '"Data file " & CallingObject.sCurrFileName & " is at " & CallingObject.DestinationFolder
                'oCallingForm.txtMessage.ScrollToEnd()


            Else
                ' Need to call me on the UI thread
                oCallingForm.Dispatcher.BeginInvoke(DispatcherPriority.Normal, New MainWindow.TwoArgDelegate(AddressOf Me.oTransferProcess_FileCompleted), CallingObject, sFileName)
            End If

        Catch ex As Exception
            Dim oErrItem As ErrorItem : oErrItem = glErrors.NewError(ex)
            ReportError(oErrItem)

        End Try


    End Sub

    Private Sub oTransferProcess_FileFailed(CallingObject As clsExtractData, sFileName As String, sReasonFailure As String) Handles oTransferProcess.FileFailed

        Try

            If oCallingForm Is Nothing OrElse oCallingForm.Dispatcher.CheckAccess() Then
                ' Running in this thread to access the UI
                'MsgBox(oErrItem.ToString)
                oCallingForm.AddToTextLogArea(Now.ToString("T") & " : File " & sFileName & " processing failed" & vbCrLf & sReasonFailure)

                'oCallingForm.txtMessage.Text &= vbCrLf & Now.ToString("T") & " : File " & sFileName & " processing failed" & vbCrLf & sReasonFailure
                'oCallingForm.txtMessage.ScrollToEnd()

            Else
                ' Need to call me on the UI thread
                oCallingForm.Dispatcher.BeginInvoke(DispatcherPriority.Normal, New MainWindow.ThreeArgDelegate(AddressOf Me.oTransferProcess_FileFailed), CallingObject, sFileName, sReasonFailure)
            End If

        Catch ex As Exception
            Dim oErrItem As ErrorItem : oErrItem = glErrors.NewError(ex)
            ReportError(oErrItem)

        End Try

    End Sub

    Private Sub oTransferProcess_FileStarted(CallingObject As clsExtractData, sFileName As String) Handles oTransferProcess.FileStarted

        Try

            If oCallingForm Is Nothing OrElse oCallingForm.Dispatcher.CheckAccess() Then
                ' Running in this thread to access the UI
                'MsgBox(oErrItem.ToString)
                oCallingForm.txtFileName.Text = sFileName & " " & CallingObject.iFileNo.ToString + " of " & CallingObject.iNoOFFiles.ToString
                oCallingForm.lblFilenameInBar.Content = sFileName
                oCallingForm.prgProgress.Value = 0
                oCallingForm.txtRecordMax.Text = CallingObject.iNoOfRecords.ToString
                oCallingForm.txtRecordCount.Text = 0

                oCallingForm.AddToTextLogArea(Now.ToString("T") & " : File " & sFileName & " selection started")

                'oCallingForm.txtMessage.Text &= vbCrLf & Now.ToString("T") & " : File " & sFileName & " selection started"
                'oCallingForm.txtMessage.ScrollToEnd()

            Else
                ' Need to call me on the UI thread
                oCallingForm.Dispatcher.BeginInvoke(DispatcherPriority.Normal, New MainWindow.TwoArgDelegate(AddressOf Me.oTransferProcess_FileStarted), CallingObject, sFileName)
            End If

        Catch ex As Exception
            Dim oErrItem As ErrorItem : oErrItem = glErrors.NewError(ex)
            ReportError(oErrItem)

        End Try

    End Sub

    Private Sub oTransferProcess_UpdatePercentagePoint(CallingObject As clsExtractData, iStepCount As Integer) Handles oTransferProcess.UpdateStepCount
        Dim lTicks As Long
        Try

            If oCallingForm Is Nothing OrElse oCallingForm.Dispatcher.CheckAccess() Then
                ' Running in this thread to access the UI
                'MsgBox(oErrItem.ToString)
                lTicks = EstEnd(CallingObject.iNoOfRecords, CallingObject.iRecordNo, CallingObject.oFileLoader.dStartOfRecordProcessing).Ticks - Now.Ticks
                If lTicks > 1000 Then Try : oCallingForm.lblFilenameInBar.Content = CallingObject.sCurrFileName & " est end : " & New Date(lTicks).ToString("T") : Catch ex As Exception : End Try
                If oCallingForm.prgProgress.Visibility = Visibility.Hidden Then oCallingForm.prgProgress.Visibility = Visibility.Visible
                oCallingForm.prgProgress.Value = iStepCount
                oCallingForm.txtRecordMax.Text = CallingObject.iNoOfRecords.ToString
                oCallingForm.txtRecordCount.Text = CallingObject.iRecordNo.ToString

            Else
                ' Need to call me on the UI thread
                oCallingForm.Dispatcher.BeginInvoke(DispatcherPriority.Normal, New MainWindow.TwoArgDelegate(AddressOf Me.oTransferProcess_UpdatePercentagePoint), CallingObject, iStepCount)
            End If

        Catch ex As Exception
            Dim oErrItem As ErrorItem : oErrItem = glErrors.NewError(ex)
            ReportError(oErrItem)

        End Try

    End Sub

    Private Sub oTransferProcess_ProcessCompleted(CallingObject As clsExtractData) Handles oTransferProcess.ProcessCompleted

        Try

            If oCallingForm Is Nothing OrElse oCallingForm.Dispatcher.CheckAccess() Then
                ' Running in this thread to access the UI
                'MsgBox(oErrItem.ToString)
                RaiseEvent ProcessFinished()
            Else
                ' Need to call me on the UI thread
                oCallingForm.Dispatcher.BeginInvoke(DispatcherPriority.Normal, New MainWindow.OneArgDelegate(AddressOf Me.oTransferProcess_ProcessCompleted), CallingObject)
            End If

        Catch ex As Exception
            Dim oErrItem As ErrorItem : oErrItem = glErrors.NewError(ex)
            ReportError(oErrItem)

        End Try

    End Sub

    Private Sub oTransferProcess_Processfailed(CallingObject As clsExtractData, sFileName As String, sReasonFail As String) Handles oTransferProcess.Processfailed
        Dim sMsg As String = ""

        Try

            If oCallingForm Is Nothing OrElse oCallingForm.Dispatcher.CheckAccess() Then
                ' Running in this thread to access the UI
                'MsgBox(oErrItem.ToString)
                sMsg = "ERROR : File " & sFileName & vbCrLf & sReasonFail

                RaiseEvent ProcessFailed(sMsg)
            Else
                ' Need to call me on the UI thread
                oCallingForm.Dispatcher.BeginInvoke(DispatcherPriority.Normal, New MainWindow.ThreeArgDelegate(AddressOf Me.oTransferProcess_Processfailed), CallingObject, sFileName, sReasonFail)
            End If

        Catch ex As Exception
            Dim oErrItem As ErrorItem : oErrItem = glErrors.NewError(ex)
            ReportError(oErrItem)

        End Try

    End Sub

    Private Sub oTransferProcess_evReportError(CallingObject As clsExtractData, oErr As ErrorItem, sReason As String) Handles oTransferProcess.evReportError
        Dim sMsg As String = ""
        Try
            If oCallingForm Is Nothing OrElse oCallingForm.Dispatcher.CheckAccess() Then
                ' Running in this thread to access the UI
                'MsgBox(oErrItem.ToString)
                sMsg = "ERROR" & vbCrLf & oErr.ToString & vbCrLf & sReason

                RaiseEvent ProcessFailed(sMsg)
            Else
                ' Need to call me on the UI thread
                oCallingForm.Dispatcher.BeginInvoke(DispatcherPriority.Normal, New MainWindow.ThreeArgDelegate(AddressOf Me.oTransferProcess_evReportError), CallingObject, oErr, sReason)
            End If

        Catch ex As Exception
            Dim oErrItem As ErrorItem : oErrItem = glErrors.NewError(ex)
            ReportError(oErrItem)

        End Try
    End Sub

    Private Sub oTransferProcess_PassMessage(CallingObject As clsExtractData, sMessage As String) Handles oTransferProcess.PassMessage
        Dim sMsg As String = ""
        Try
            If oCallingForm Is Nothing OrElse oCallingForm.Dispatcher.CheckAccess() Then

                oCallingForm.AddToTextLogArea(Now.ToString("T") & " : " & sMessage)

                'oCallingForm.txtMessage.Text &= vbCrLf & Now.ToString("T") & " : " & sMessage
                'oCallingForm.txtMessage.ScrollToEnd()

            Else
                ' Need to call me on the UI thread
                oCallingForm.Dispatcher.BeginInvoke(DispatcherPriority.Normal, New MainWindow.TwoArgDelegate(AddressOf Me.oTransferProcess_PassMessage), CallingObject, sMessage)
            End If

        Catch ex As Exception
            Dim oErrItem As ErrorItem : oErrItem = glErrors.NewError(ex)
            ReportError(oErrItem)

        End Try

    End Sub

    Private Sub oTransferProcess_FileSelectionCompleted(CallingObject As clsExtractData, sFileName As String, iNoOfRecords As Integer) Handles oTransferProcess.FileSelectionCompleted
        Try

            If oCallingForm Is Nothing OrElse oCallingForm.Dispatcher.CheckAccess() Then
                ' Running in this thread to access the UI
                'MsgBox(oErrItem.ToString)
                If oCallingForm.prgProgress.Visibility = Visibility.Hidden Then oCallingForm.prgProgress.Visibility = Visibility.Visible
                oCallingForm.prgProgress.Value = 0
                oCallingForm.txtRecordMax.Text = iNoOfRecords.ToString
                oCallingForm.txtRecordCount.Text = "0"

                oCallingForm.AddToTextLogArea(Now.ToString("T") & " : File " & sFileName & " selection completed with " & iNoOfRecords.ToString & " selected")

                'oCallingForm.txtMessage.Text &= vbCrLf & Now.ToString("T") & " : File " & sFileName & " selection completed with " & iNoOfRecords.ToString & " selected"
                'oCallingForm.txtMessage.ScrollToEnd()

            Else
                ' Need to call me on the UI thread
                oCallingForm.Dispatcher.BeginInvoke(DispatcherPriority.Normal, New MainWindow.ThreeArgDelegate(AddressOf Me.oTransferProcess_FileSelectionCompleted), CallingObject, sFileName, iNoOfRecords)
            End If

        Catch ex As Exception
            Dim oErrItem As ErrorItem : oErrItem = glErrors.NewError(ex)
            ReportError(oErrItem)

        End Try
    End Sub
End Class
