﻿Imports System.IO
Imports System.Windows
Imports System.Windows.Controls
Imports System.Windows.Threading
Imports System.Threading
Imports System.Text.RegularExpressions

Public Class clsManageProcesses
    Protected Friend stopProcessing As Boolean = False

    'Protected Friend Delegate Sub NoArgDelegate()
    'Protected Friend Delegate Sub OneArgDelegate(arg As Object)
    'Protected Friend Delegate Sub TwoArgDelegate(arg1 As Object, arg2 As Object)
    'Protected Friend Delegate Sub ThreeArgDelegate(arg1 As Object, arg2 As Object, arg3 As Object)
    'Protected Friend Delegate Sub FourArgDelegate(arg1 As Object, arg2 As Object, arg3 As Object, arg4 As Object)
    'Protected Friend Delegate Sub FiveArgDelegate(arg1 As Object, arg2 As Object, arg3 As Object, arg4 As Object, arg5 As Object)

    Protected Friend oCallingForm As MainWindow
    'Protected Friend oControlWithDelegate As Control
    Private oActiveProcess As MainWindow.NoArgDelegate
    'Private oActiveProcessOneArg As MainWindow.OneArgDelegate
    Protected Friend oControlToPopulate As Control

    Public WithEvents oExampleRecords As New clsExampleListData

    Protected oThread As Thread

    Dim sLastShow As Date


    Protected Friend Sub CancelProcess()
        Me.stopProcessing = True
    End Sub


    Protected Friend Sub ActivateProcessX1(oControlToUse As Control, oSomeClass As Object)
        Try
            Me.stopProcessing = False
            If oSomeClass Is Nothing Then
                oSomeClass = oExampleRecords
            Else
                oExampleRecords = DirectCast(oSomeClass, clsExampleListData)
            End If

            Dim oActiveProcessOneArg As New MainWindow.OneArgDelegate(AddressOf MainProcessToDoProcessX1)

            oActiveProcessOneArg.BeginInvoke(oSomeClass, Nothing, Nothing)

        Catch ex As Exception
            Dim oErrItem As ErrorItem : oErrItem = glErrors.NewError(ex)
            ReportError(oErrItem)
        End Try
    End Sub


    Protected Friend Sub MainProcessToDoProcessX1(oSomeDataClass As clsExampleListData)
        ' Called to do the processing in a separate thread
        Try

            'If oSomeDataClass Is Nothing Then oSomeDataClass = New clsExampleListData
            If oSomeDataClass Is Nothing Then oSomeDataClass = oExampleRecords

            AddHandler oSomeDataClass.NewPrimeFound, AddressOf oExampleRecords_NewPrimeFound

            DoUpdateResponseX1InDataClass(oSomeDataClass)
            'DoUpdateResponseX1InDataClass(oExampleRecords)

        Catch ex As Exception
            Dim oErrItem As ErrorItem : oErrItem = glErrors.NewError(ex)
            ReportError(oErrItem)

        End Try
    End Sub


    Protected Friend Sub DoUpdateResponseX1InDataClass(oSomeDataClass As clsExampleListData)
        Dim sResult As String

        Try

            sResult = oSomeDataClass.ProcessDataToResult
            If oCallingForm Is Nothing OrElse oCallingForm.Dispatcher.CheckAccess() Then
                ' Running in this thread to access the UI
                UpdateUIOnX1Response(sResult)
                'oCallingForm.txtResult.Text = sResult
            Else
                ' Need to call me on the UI thread
                ' Pass the result, not the object.
                ' If the object is passed, then the work is done on the UI thread to get any computed result, making threading pointless
                oCallingForm.Dispatcher.BeginInvoke(DispatcherPriority.Normal, New MainWindow.OneArgDelegate(AddressOf Me.UpdateUIOnX1Response), sResult)
            End If

        Catch ex As Exception
            Dim oErrItem As ErrorItem : oErrItem = glErrors.NewError(ex)
            ReportError(oErrItem)

        End Try
    End Sub



    Protected Friend Sub UpdateUIOnX1Response(sResult As String)
        Try
            ' Sonce the object is the same as the locally stored one, can get to the data from that as well as what is passed by delegate (Dispatcher) back
            oCallingForm.txtRecordCount.Text = oExampleRecords.Item(oExampleRecords.Keys(oExampleRecords.Count - 1)).sValue
            oCallingForm.txtResult.Text = sResult
            oCallingForm.cmdActivate.IsEnabled = True

        Catch ex As Exception
            Dim oErrItem As ErrorItem : oErrItem = glErrors.NewError(ex)
            ReportError(oErrItem)
        End Try

    End Sub



    Protected Friend Sub oExampleRecords_NewPrimeFound(iNumFound As Integer) Handles oExampleRecords.NewPrimeFound
        ' This event should be on same thread as the calling process on the handle associated class
        ' if not on UI thread, then call with delegate to UI thread to update UI
        If oCallingForm Is Nothing OrElse oCallingForm.Dispatcher.CheckAccess() Then
            ' Using a little trick to prevent the UI being flooded (1 Millisecond = 10,000 ticks, 1 second = 10,000,000 ticks)
            If Now.Ticks - sLastShow.Ticks > 1000000 Then
                oCallingForm.txtRecordCount.Text = iNumFound.ToString
                'System.Windows.Forms.Application.DoEvents()
                sLastShow = Now()
            End If
        Else
            ' Need to call me on the UI thread
            'oCallingForm.Dispatcher.BeginInvoke(DispatcherPriority.Normal, New MainWindow.OneArgDelegate(AddressOf Me.ReportError), iNumFound.ToString) '
            oCallingForm.Dispatcher.BeginInvoke(DispatcherPriority.Normal, New MainWindow.OneArgDelegate(AddressOf Me.oExampleRecords_NewPrimeFound), iNumFound)
        End If

    End Sub

    Protected Friend Sub ActivateProcessX2(oControlToPopulateIn As Control)
        ' Populate combobox control cmbTable
        Try
            Me.stopProcessing = False

            Me.oControlToPopulate = oControlToPopulateIn

            oActiveProcess = New MainWindow.NoArgDelegate(AddressOf Me.ProcessingForDataThreadX2)

            oActiveProcess.BeginInvoke(Nothing, Nothing)

        Catch ex As Exception
            Dim oErrItem As ErrorItem : oErrItem = glErrors.NewError(ex)
            ReportError(oErrItem)
            'oControlWithDelegate.Dispatcher.BeginInvoke(DispatcherPriority.Normal, New OneArgDelegate(AddressOf Me.ReportError), oErrItem)
        End Try
    End Sub



    Protected Friend Sub ProcessingForDataThreadX2()
        'Dim oCombo As ComboBox, oBinding As Binding
        Dim oTxt As TextBox
        Try
            oTxt = oCallingForm.txtRecordCount
            DoSomeDataActionsX2(oTxt)

        Catch ex As Exception
            Dim oErrItem As ErrorItem : oErrItem = glErrors.NewError(ex)
            ReportError(oErrItem)

        End Try
    End Sub


    Protected Friend Sub DoSomeDataActionsX2(oTextControlToPopulate As Control)
        Dim oTxt As TextBox
        Try

            If oCallingForm Is Nothing OrElse oCallingForm.Dispatcher.CheckAccess() Then
                oTxt = DirectCast(oTextControlToPopulate, TextBox)

                oTxt.Text = oExampleRecords.Count.ToString

                If TypeOf (oCallingForm) Is MainWindow Then
                    oCallingForm = DirectCast(oCallingForm, MainWindow)
                    oTxt = oCallingForm.txtResult

                    oTxt.Text = oExampleRecords.ProcessDataToResult
                End If

                ' Running in this thread to access the UI
            Else
                ' Need to call me on the UI thread
                oCallingForm.Dispatcher.BeginInvoke(DispatcherPriority.Normal, New MainWindow.OneArgDelegate(AddressOf Me.DoSomeDataActionsX2))
            End If


        Catch ex As Exception

        End Try
    End Sub


    Protected Friend Sub DoSomeDataBindingX3(oControlToPopulate As Control)
        Dim oCombo As ComboBox, oBinding As Binding
        Try

            If oCallingForm Is Nothing OrElse oCallingForm.Dispatcher.CheckAccess() Then

                oCombo = DirectCast(oControlToPopulate, ComboBox)
                oBinding = New Binding()
                oBinding.Mode = BindingMode.OneTime
                oBinding.Source = oExampleRecords.Values ' @@@ Very important to use values for binding, not the list @@@

                BindingOperations.SetBinding(oCombo, ComboBox.ItemsSourceProperty, oBinding)

                oCombo.IsEnabled = oExampleRecords.Count > 0
                oCombo.DisplayMemberPath = "sValue2"

                ' Running in this thread to access the UI
            Else
                ' Need to call me on the UI thread
                oCallingForm.Dispatcher.BeginInvoke(DispatcherPriority.Normal, New MainWindow.OneArgDelegate(AddressOf Me.DoSomeDataBindingX3), oControlToPopulate)
            End If


        Catch ex As Exception

        End Try
    End Sub


    Protected Friend Sub DoSomeCompletedDataActionsX4()
        Try

            If oCallingForm Is Nothing OrElse oCallingForm.Dispatcher.CheckAccess() Then
                oCallingForm.VerifyControls()
            Else
                oCallingForm.Dispatcher.BeginInvoke(DispatcherPriority.Normal, New MainWindow.NoArgDelegate(AddressOf oCallingForm.VerifyControls), Nothing)
            End If

        Catch ex As Exception
            Dim oErrItem As ErrorItem : oErrItem = glErrors.NewError(ex)
            ReportError(oErrItem)

        End Try
    End Sub


    Protected Friend Sub ReportError(oErrItem As ErrorItem)
        Try
            If oCallingForm Is Nothing OrElse oCallingForm.Dispatcher.CheckAccess() Then
                ' Running in this thread to access the UI
                MsgBox(oErrItem.ToString)
            Else
                ' Need to call me on the UI thread
                oCallingForm.Dispatcher.BeginInvoke(DispatcherPriority.Normal, New MainWindow.OneArgDelegate(AddressOf Me.ReportError), oErrItem)
            End If
        Catch ex As Exception
            MsgBox(ex.ToString)

        End Try
    End Sub

End Class
