﻿Option Compare Text

Imports System.Reflection
Imports Microsoft.VisualBasic
'Imports MSScriptControl
Imports System.Text.RegularExpressions
Imports System.Net

Imports System.CodeDom.Compiler

' =====================================================================================================================
' Class to manage substitutions in a string based on key/value pairs
'-----------------------------------------------------------------------
' v2.0.00 : 1 Aug 2014 :    Convert passed key.value data into sortedlist(New CaseInsensitiveComparer())
'                           Also apply a RecordError subroutine to simplify error handling variations
'-----------------------------------------------------------------------
' v2.0.01 : 28 Aug 2014 :   Use #if #end if to ascertain default key value pairs depending on type
'                           of program being developed
'-----------------------------------------------------------------------
' v2.0.02 : 7 Sept 2014 :    Amend OriginalString to always refresh values if PUT is invoked
'-----------------------------------------------------------------------
' v2.0.03 : 17 Sept 2014 :  Fixed error in second and subsequent identification of a replacement in 
'                           a large string. Added variable iPrevMatchEnd
'-----------------------------------------------------------------------
' v2.1.00 : 30 Jan 2015 :  Found out the MSScriptControl only functions if compiled up on Dell4ykt44j.
'						MEasures put in to prevent fatal errors causing crashes
'-----------------------------------------------------------------------
' v2.1.01 : 9 Feb 2015 : Identified calculations being missed off for <> (not equal to)
'-----------------------------------------------------------------------
' v2.2.0.0 : 24 Aug 2015 : Replace using MSScriptControl with embedded class clsEvalExpression
'					Placed the class at the bottom of this file
'  =====================================================================================================================


' Examples. Note that line feeds not ok in the substitution brackets, but ok outside them
'	{{TestValue}}-[[TestValue2]]
'	((If [[TestValue]] <> "Wot"))stuff{{endif}}
'	((If [[TestValue]]))stuff{{endif}}
'	{{beginif [[fred]]}}Fred {{if [[jones]]}}Jones{{endif}}<<if {{Bloggs}}>>Bloggs((endif))((endif))
'	{{beginif [[fred]]}}Fred {{if "[[jones]]" <> "No" AND "@@Jones@@" <> ""}}Jones{{endif}}<<if {{Bloggs}}>>Bloggs((endif))((endif))
'	{{beginif [[fred]]}}Fred{{if "[[jones]]" <> "No" AND "@@Jones@@" <> ""}} Jones{{endif}}<<if {{Bloggs}}>> Bloggs((endif)) [[eval 2*len("{{Bloggs}}")]]((endif))
'	
'
'		can use opening brackets if, beginif, ifbegin,begin 
'		then a value to be not empty or calculation to a positive result to test
'		then the closing brackets
'		then the data to show if the condition produces a positive result, including line feeds
'		then endif, ifend, end in the opening and closing substitution brackets

'	[[eval left("((Test))",((Size)))]]
'	Uses marker eval, evaluate, compute, calculate, calc
'	Can use any VB single result expression

'	Acceptable paired value markers
'		{{value}}
'		[[value]]
'		((value))
'		<<value>>
'		##value##
'		@@value@@
'		%%value%%


Public Class clsValueSubstitutions
	Inherits List(Of SubstituteEntry)

	'Protected Friend oFixedSubstituteList As New SortedList(Of String, String)
	Protected Friend oFixedSubstituteList As New SortedList(New CaseInsensitiveComparer())
	'Protected Friend oVariableSubstituteList As New SortedList(Of String, String)
	'Protected Friend oCurrentSubstituteStrings As New SortedList(Of String, String)
	Protected Friend oVariableSubstituteList As New SortedList(New CaseInsensitiveComparer())
	Protected Friend oCurrentSubstituteStrings As New SortedList(New CaseInsensitiveComparer())

	'Protected Friend oAllSubstituteStrings As New SortedList(Of String, String)
	Protected Friend oAllSubstituteStrings As New SortedList(New CaseInsensitiveComparer())

	Protected Friend OtherValues As New Hashtable

	Private sOriginalString As String = ""
	Protected Friend dPassedDate As Date = Now

	Protected Friend StartTokens() As String = {"[[", "%%", "<<", "((", "{{", "@@", "##"}
	Protected Friend EndTokens() As String = {"]]", "%%", ">>", "))", "}}", "@@", "##"}

	Public Function GetSubstitutionForString(TemplateString As String) As String
		Dim sReturn As String = TemplateString
		Try

			Me.OriginalString = sReturn
			sReturn = Me.Result

		Catch ex As Exception
			Dim aDetail As New ArrayList
			RecordError(ex, aDetail)

		End Try
		Return sReturn
	End Function

	Public Property OriginalString As String
		Get
			Return sOriginalString
		End Get
		Set(value As String)
			'If value <> sOriginalString Then
			sOriginalString = value
			SplitString(Nothing, True)
			'End If
		End Set
	End Property


	Public Function ContainsKey(Key As String) As Boolean
		Dim bReturn As Boolean = False
		Try

			bReturn = Me.oAllSubstituteStrings.ContainsKey(Key)

		Catch ex As Exception
			Dim aDetail As New ArrayList
			RecordError(ex, aDetail)

		End Try
		Return bReturn
	End Function


	Public Sub ResetAll()
		Try
			Me.Clear()
			Me.oAllSubstituteStrings.Clear()
			Me.oCurrentSubstituteStrings.Clear()
			Me.oFixedSubstituteList.Clear()
			Me.OtherValues.Clear()
			sOriginalString = ""


		Catch ex As Exception
			Dim aDetail As New ArrayList
			RecordError(ex, aDetail)

		End Try
	End Sub


	Public Function ValueOf(Key As String) As String
		Dim sReturn As String = ""
		Try

			If Me.oAllSubstituteStrings.ContainsKey(Key) Then sReturn = oAllSubstituteStrings.Item(Key).ToString

		Catch ex As Exception
			Dim aDetail As New ArrayList
			RecordError(ex, aDetail)

		End Try
		Return sReturn
	End Function

	Public ReadOnly Property Result As String
		Get
			Dim sReturn As String = "", oValItem As SubstituteEntry
			Dim iValidSection As Integer = -1
			Try

				For Each oValItem In Me
					If iValidSection > 0 Then
						If oValItem.BlockLevel < iValidSection Then iValidSection = -1
					Else
						If oValItem.TokenType = "if" Or oValItem.TokenType = "ifbegin" Or oValItem.TokenType = "beginif" Or oValItem.TokenType = "begin" Then
							If Not oValItem.EvalIsValid Then iValidSection = oValItem.BlockLevel
						Else
							sReturn &= oValItem.Result
						End If

					End If

				Next

			Catch ex As Exception
				Dim aDetail As New ArrayList
				RecordError(ex, aDetail)

			End Try
			Return sReturn

		End Get
	End Property

	'Protected Friend Function PopulateReplacementArray(Optional ByRef aExtraStringsIn As SortedList(Of String, String) = Nothing, Optional ByVal ResetValues As Boolean = False) As SortedList(Of String, String)
	Protected Friend Sub PopulateReplacementArray(Optional ByRef aExtraStringsIn As SortedList(Of String, String) = Nothing, Optional ByVal ResetValues As Boolean = False)
		Dim iPosLast As Integer = 1, sKey As String = "", sValue As String = "", sNewValue As String = ""
		Dim sTemp As String = "", sNewKey As String = "", oAssembly As Assembly, oAssemblyName As AssemblyName
		'Dim oTempContextMatches As New SortedList(Of String, String)
		Dim oTempContextMatches As New SortedList(New CaseInsensitiveComparer())
		Dim sTemp2 As String = "", oTemp As Object = Nothing
		Dim dtFinYear As Date, iFinYear As Integer, iFinMonth As Integer, iFinDays As Integer, iFinWeek As Integer, iMilliseconds As Integer
		Dim iYear As Integer, iMonth As Integer, iMonthDay As Integer, iYearDay As Integer, iWeek As Integer, iWeekDay As Integer
		Dim iHour As Integer, iMinute As Integer, iSecond As Integer, sDayName As String, lTicks As Long
		Dim aParts() As String, sUserId As String = "", sDomain As String = "", sClientCompName As String = "", sClientDomain As String = ""
		Dim oReturn As New SortedList(New CaseInsensitiveComparer()), oDNSEntry As IPHostEntry
		Dim aExtraStrings As New SortedList(New CaseInsensitiveComparer()), sTempPath As String = "", bInTest As Boolean = False

		Try

			If aExtraStringsIn IsNot Nothing Then aExtraStrings = New SortedList(aExtraStringsIn, New CaseInsensitiveComparer()) Else aExtraStrings = New SortedList(New CaseInsensitiveComparer())
			'If aExtraStrings Is Nothing Then aExtraStrings = New SortedList(Of String, String)


			'If oFixedSubstituteList.Count = 0 Or ResetValues Or gbResetReplacements Then
			If oFixedSubstituteList.Count = 0 Or ResetValues Or gbResetReplacements Or oAllSubstituteStrings.Count = 0 Then

				gbResetReplacements = False

				oFixedSubstituteList.Clear()

				aParts = Split(My.User.Name, "\")
				If UBound(aParts) > 0 Then
					sDomain = aParts(0)
					sUserId = aParts(UBound(aParts))
				Else
					sUserId = My.User.Name
				End If

				sClientCompName = My.Computer.Name

				oDNSEntry = System.Net.Dns.GetHostEntry(sClientCompName)

				sClientCompName = oDNSEntry.HostName
				aParts = Split(sClientCompName, ".")
				If UBound(aParts) > 0 Then
					sClientCompName = aParts(0)
					aParts(0) = ""
					sClientDomain = Strings.Join(aParts, ".")
				End If
				sUserId = My.User.Name

				oAssembly = Assembly.GetExecutingAssembly
				oAssemblyName = New AssemblyName(oAssembly.FullName)

				oTempContextMatches.Add("DOMAIN", sDomain)
				oTempContextMatches.Add("DOMAINID", sDomain)
				oTempContextMatches.Add("DOMAINNAME", sDomain)
				oTempContextMatches.Add("NETWORKID", sDomain)
				oTempContextMatches.Add("NAME", sUserId)
				oTempContextMatches.Add("USER", sUserId)
				oTempContextMatches.Add("USERID", sUserId)
				oTempContextMatches.Add("LOGINNAME", My.User.Name)
				oTempContextMatches.Add("LOGINUSERID", My.User.Name)
				oTempContextMatches.Add("LOGIN", My.User.Name)
				oTempContextMatches.Add("CLIENTCOMPUTERADDRESS", oDNSEntry.AddressList.ToString)
				oTempContextMatches.Add("CLIENTCOMPUTERNAME", sClientCompName)
				oTempContextMatches.Add("CLIENTCOMPUTERDOMAIN", sClientDomain)
				oTempContextMatches.Add("HOSTCOMPUTERNAME", My.Computer.Name)
				oTempContextMatches.Add("ASSEMBLYNAME", oAssembly.FullName)
				oTempContextMatches.Add("ASSEMBLY", oAssembly.FullName)
				oTempContextMatches.Add("ASSEMBLYVERSION", oAssemblyName.Version.ToString)
				oTempContextMatches.Add("VERSION", oAssemblyName.Version.ToString)
				oTempContextMatches.Add("APPLICATIONDATE", System.IO.File.GetLastWriteTime(oAssembly.Location).ToString)
				oTempContextMatches.Add("ASSEMBLYDATE", System.IO.File.GetLastWriteTime(oAssembly.Location).ToString)
				'oTempContextMatches.Add("APPLICATIONNAME", My.Application.Info.AssemblyName & ".exe")
				'oTempContextMatches.Add("APPLICATIONDIRECTORY", My.Application.Info.DirectoryPath)
				'oTempContextMatches.Add("APPLICATIONFOLDER", My.Application.Info.DirectoryPath)
				'oTempContextMatches.Add("APPLICATIONPATH", My.Application.Info.DirectoryPath & "\" & My.Application.Info.AssemblyName & ".exe")
				oTempContextMatches.Add("CURRENTDIRECTORY", My.Computer.FileSystem.CurrentDirectory)
				oTempContextMatches.Add("TEMP", System.IO.Path.GetTempPath)
				oTempContextMatches.Add("TMP", System.IO.Path.GetTempPath)
				oTempContextMatches.Add("TEMPFOLDER", System.IO.Path.GetTempPath)
				oTempContextMatches.Add("TEMPDIRECTORY", System.IO.Path.GetTempPath)

				'http://msdn.microsoft.com/en-us/library/7ah135z7.aspx
				'http://msdn.microsoft.com/en-us/magazine/cc188706.aspx
				'http://msdn.microsoft.com/en-us/library/ms233781%28v=vs.100%29.aspx
				'http://www.tutorialspoint.com/vb.net/vb.net_directives.htm

				Try

#If _MYTYPE = "Windows" Then
				oTempContextMatches.Add("APPLICATIONNAME", My.Application.Info.AssemblyName & ".exe")
				oTempContextMatches.Add("APPLICATIONDIRECTORY", My.Application.Info.DirectoryPath)
				oTempContextMatches.Add("APPLICATIONFOLDER", My.Application.Info.DirectoryPath)
				oTempContextMatches.Add("APPLICATIONPATH", My.Application.Info.DirectoryPath & "\" & My.Application.Info.AssemblyName & ".exe")
				stemppath = My.Application.Info.DirectoryPath
#ElseIf _MYTYPE = "WindowsForms" Then
				oTempContextMatches.Add("APPLICATIONNAME", My.Application.Info.AssemblyName & ".exe")
				oTempContextMatches.Add("APPLICATIONDIRECTORY", My.Application.Info.DirectoryPath)
				oTempContextMatches.Add("APPLICATIONFOLDER", My.Application.Info.DirectoryPath)
				oTempContextMatches.Add("APPLICATIONPATH", My.Application.Info.DirectoryPath & "\" & My.Application.Info.AssemblyName & ".exe")
				stemppath = My.Application.Info.DirectoryPath
#ElseIf _MYTYPE = "Web" Then
					oTempContextMatches.Add("RequestURL", My.Request.Url.AbsoluteUri)
					oTempContextMatches.Add("RequestFilepath", My.Request.FilePath)
					oTempContextMatches.Add("ApplicationPhysicalFilepath", My.Request.PhysicalApplicationPath)
					oTempContextMatches.Add("APPLICATIONPATH", My.Request.ApplicationPath)
					'oTempContextMatches.Add("APPLICATIONNAME", My.Request.FilePathObject.filename)
					stemppath = My.Request.Url.AbsoluteUri
#Else
					oTempContextMatches.Add("APPLICATIONNAME", My.Application.Info.AssemblyName & ".exe")
					oTempContextMatches.Add("APPLICATIONDIRECTORY", My.Application.Info.DirectoryPath)
					oTempContextMatches.Add("APPLICATIONFOLDER", My.Application.Info.DirectoryPath)
					oTempContextMatches.Add("APPLICATIONPATH", My.Application.Info.DirectoryPath & "\" & My.Application.Info.AssemblyName & ".exe")
					sTempPath = My.Application.Info.DirectoryPath
#End If

					If sTempPath <> "" Then bInTest = (sTempPath.IndexOf("test", StringComparison.CurrentCultureIgnoreCase) > 0 Or sTempPath.IndexOf("dev", StringComparison.CurrentCultureIgnoreCase) > 0)

				Catch ex As Exception

				End Try

				For Each oEnvKey In Environment.GetEnvironmentVariables.Keys
					sKey = "" & UCase(oEnvKey.ToString) & ""
					sValue = Environment.GetEnvironmentVariables.Item(oEnvKey).ToString
					If Not oTempContextMatches.ContainsKey(sKey) Then oTempContextMatches.Add(sKey, sValue) Else oTempContextMatches(sKey) = sValue
				Next

				For Each sKey In oTempContextMatches.Keys
					sValue = oTempContextMatches.Item(sKey).ToString
					If Not oFixedSubstituteList.ContainsKey(sKey) Then oFixedSubstituteList.Add(sKey, sValue) Else oFixedSubstituteList(sKey) = sValue
				Next

			End If

			oReturn = New SortedList(New CaseInsensitiveComparer())	'  New SortedList(Of String, String)
			For Each sKey In oFixedSubstituteList.Keys
				sValue = oFixedSubstituteList.Item(sKey).ToString
				If Not oReturn.ContainsKey(sKey) Then oReturn.Add(sKey, sValue) Else oReturn(sKey) = sValue
			Next

			oVariableSubstituteList.Clear()

			iMonthDay = DatePart(DateInterval.Day, dPassedDate)
			iYearDay = DatePart(DateInterval.DayOfYear, dPassedDate)
			iMonth = DatePart(DateInterval.Month, dPassedDate)
			iYearDay = DatePart(DateInterval.Year, dPassedDate)
			iYear = DatePart(DateInterval.Year, dPassedDate)
			iWeekDay = DatePart(DateInterval.Weekday, dPassedDate)
			sDayName = WeekdayName(iWeekDay, , Microsoft.VisualBasic.FirstDayOfWeek.Sunday)
			iWeek = DatePart(DateInterval.WeekOfYear, dPassedDate)

			iFinYear = Year(Now) + CInt(IIf(Month(dPassedDate) < 4, -1, 0))
			dtFinYear = New Date(iFinYear, 4, 1)
			iFinMonth = CInt(DateDiff(DateInterval.Month, dtFinYear, dPassedDate) + 1)
			iFinDays = CInt(DateDiff(DateInterval.DayOfYear, dtFinYear, dPassedDate) + 1)
			iFinWeek = CInt(DateDiff(DateInterval.WeekOfYear, dtFinYear, dPassedDate) + 1)

			iHour = DatePart(DateInterval.Hour, dPassedDate)
			iMinute = DatePart(DateInterval.Minute, dPassedDate)
			iSecond = DatePart(DateInterval.Second, dPassedDate)
			iMilliseconds = dPassedDate.Millisecond
			lTicks = dPassedDate.Ticks

			oTempContextMatches.Add("date", dPassedDate.ToShortDateString)
			oTempContextMatches.Add("fulldate", dPassedDate.ToString("dd MMMM yyyy"))
			oTempContextMatches.Add("GenericDate", dPassedDate.ToString("dd-MMM-yyyy"))
			oTempContextMatches.Add("dateshort", dPassedDate.ToShortDateString)
			oTempContextMatches.Add("datesort", dPassedDate.ToString("yyyyMMdd"))
			oTempContextMatches.Add("sortdate", dPassedDate.ToString("yyyyMMdd"))
			oTempContextMatches.Add("datelong", dPassedDate.ToLongDateString)
			oTempContextMatches.Add("dateUTC", dPassedDate.ToString("o"))
			oTempContextMatches.Add("datetimesort", dPassedDate.ToString("yyyyMMddHHmmssffff"))
			oTempContextMatches.Add("time", dPassedDate.ToShortTimeString)
			oTempContextMatches.Add("timeshort", dPassedDate.ToShortTimeString)
			oTempContextMatches.Add("timelong", dPassedDate.ToLongTimeString)
			oTempContextMatches.Add("timesort", dPassedDate.ToString("HHmmss"))
			oTempContextMatches.Add("day", iMonthDay.ToString)
			oTempContextMatches.Add("d", iMonthDay.ToString)
			oTempContextMatches.Add("dd", iMonthDay.ToString("00"))
			oTempContextMatches.Add("days", iYearDay.ToString)
			oTempContextMatches.Add("findays", iFinDays.ToString)
			oTempContextMatches.Add("ww", iWeek.ToString("00"))
			oTempContextMatches.Add("week", iWeek.ToString)
			oTempContextMatches.Add("weekday", iWeekDay.ToString)
			oTempContextMatches.Add("weekdayname", sDayName)
			oTempContextMatches.Add("finww", iFinWeek.ToString("00"))
			oTempContextMatches.Add("finweek", iFinWeek.ToString)
			oTempContextMatches.Add("mm", iMonth.ToString("00"))
			oTempContextMatches.Add("mmm", MonthName(iMonth, True))
			oTempContextMatches.Add("mon", MonthName(iMonth, True))
			oTempContextMatches.Add("monthshort", MonthName(iMonth, True))
			oTempContextMatches.Add("month", MonthName(iMonth, False))
			oTempContextMatches.Add("monthlong", MonthName(iMonth, False))
			oTempContextMatches.Add("mmmm", MonthName(iMonth, False))
			oTempContextMatches.Add("finmm", iFinMonth.ToString("00"))
			oTempContextMatches.Add("year", iYear.ToString("0000"))
			oTempContextMatches.Add("yyyy", iYear.ToString("0000"))
			oTempContextMatches.Add("yy", (iYear Mod 100).ToString("00"))
			oTempContextMatches.Add("finyear", iFinYear.ToString("0000"))
			oTempContextMatches.Add("finyyyy", iFinYear.ToString("0000"))
			oTempContextMatches.Add("finyy", (iFinYear Mod 100).ToString("00"))
			oTempContextMatches.Add("yyyymm", iYear.ToString("0000") & iMonth.ToString("00"))
			oTempContextMatches.Add("mmyyyy", iMonth.ToString("00") & iYear.ToString("0000"))
			oTempContextMatches.Add("yyyymmdd", iYear.ToString("0000") & iMonth.ToString("00") & iMonthDay.ToString("00"))
			oTempContextMatches.Add("ddmmyyyy", iMonthDay.ToString("00") & iMonth.ToString("00") & iYear.ToString("0000"))
			oTempContextMatches.Add("hour", iHour.ToString)
			oTempContextMatches.Add("hours", iHour.ToString)
			oTempContextMatches.Add("hh", iHour.ToString("00"))
			oTempContextMatches.Add("h", iHour.ToString)
			oTempContextMatches.Add("hhmm", iHour.ToString("00") & iMinute.ToString("00"))
			oTempContextMatches.Add("min", iMinute.ToString("00"))
			oTempContextMatches.Add("minute", iMinute.ToString)
			oTempContextMatches.Add("minutes", iMinute.ToString)
			oTempContextMatches.Add("nn", iMinute.ToString("00"))
			oTempContextMatches.Add("n", iMinute.ToString)
			oTempContextMatches.Add("hhmmss", iHour.ToString("00") & iMinute.ToString("00") & iSecond.ToString("00"))
			oTempContextMatches.Add("ss", iSecond.ToString("00"))
			oTempContextMatches.Add("s", iSecond.ToString)
			oTempContextMatches.Add("sec", iSecond.ToString)
			oTempContextMatches.Add("second", iSecond.ToString)
			oTempContextMatches.Add("seconds", iSecond.ToString)
			oTempContextMatches.Add("milliseconds", iMilliseconds.ToString)
			oTempContextMatches.Add("millisecond", iMilliseconds.ToString)
			oTempContextMatches.Add("mill", iMilliseconds.ToString)
			oTempContextMatches.Add("tick", lTicks.ToString)
			oTempContextMatches.Add("ticks", lTicks.ToString)

			oTempContextMatches.Add("TestOrLive", IIf(bInTest, "Test", "Live").ToString)
			'oTempContextMatches.Add("TESTOrLIVE", IIf(gbInTest, "TEST", "LIVE").ToString)
			'oTempContextMatches.Add("testOrlive", IIf(gbInTest, "test", "live").ToString)

			oTempContextMatches.Add("LIVEOrNothing", IIf(bInTest, "", "LIVE").ToString)
			'oTempContextMatches.Add("LiveOrNothing", IIf(gbInTest, "", "Live").ToString)
			'oTempContextMatches.Add("liveOrNothing", IIf(gbInTest, "", "live").ToString)

			oTempContextMatches.Add("TestOrNothing", IIf(bInTest, "Test", "").ToString)
			'oTempContextMatches.Add("TESTOrNothing", IIf(gbInTest, "TEST", "").ToString)
			'oTempContextMatches.Add("testOrNothing", IIf(gbInTest, "test", "").ToString)

			oTempContextMatches.Add("TESTOrAPP", IIf(bInTest, "TEST", "APP").ToString)
			'oTempContextMatches.Add("TestOrApp", IIf(gbInTest, "Test", "App").ToString)
			'oTempContextMatches.Add("testOrapp", IIf(gbInTest, "test", "app").ToString)

			For Each oEnvKey In Environment.GetEnvironmentVariables.Keys
				sKey = "" & UCase(oEnvKey.ToString) & ""
				sValue = Environment.GetEnvironmentVariables.Item(oEnvKey).ToString
				If Not oTempContextMatches.ContainsKey(sKey) Then oTempContextMatches.Add(sKey, sValue) Else oTempContextMatches(sKey) = sValue
			Next

			For Each sKey In oTempContextMatches.Keys
				sValue = oTempContextMatches.Item(sKey).ToString
				If Not oVariableSubstituteList.ContainsKey(sKey) Then oVariableSubstituteList.Add(sKey, sValue) Else oVariableSubstituteList.Item(sKey) = sValue

			Next

			For Each sKey In oVariableSubstituteList.Keys
				sValue = oVariableSubstituteList.Item(sKey).ToString
				If Not oReturn.ContainsKey(sKey) Then oReturn.Add(sKey, sValue) Else oReturn.Item(sKey) = sValue
			Next

			If aExtraStrings.Count > 0 Then
				For Each sKey In aExtraStrings.Keys
					If Not aExtraStrings.ContainsKey(sKey) Then sValue = aExtraStrings.Item(sKey).ToString
				Next
			End If

			For Each sKey In aExtraStrings.Keys
				Try

					oTemp = aExtraStrings.Item(sKey)
					If oTemp Is Nothing Then sValue = "" Else sValue = oTemp.ToString
					If Not oReturn.ContainsKey(sKey) Then oReturn.Add(sKey, sValue) Else oReturn.Item(sKey) = sValue
				Catch ex2 As Exception
					If Not oReturn.ContainsKey(sKey) Then oReturn.Add(sKey, "")
				End Try
			Next

			oAllSubstituteStrings = oReturn

		Catch ex As Exception
			Dim aDetail As New ArrayList
			RecordError(ex, aDetail)

		End Try

		'Return oReturn
	End Sub


	Protected Friend Sub SplitString(aExtraStringsIn As SortedList(Of String, String), Optional UseTokenMarks As Boolean = False, Optional ResetValues As Boolean = False)
		Dim sReturn As String = sOriginalString, sUpdateIgnore As String = Space(Len(sReturn))
		Dim iPosLast As Integer = 1, oVal As SubstituteEntry, iMatchlength As Integer
		Dim iPrevMatchEnd As Integer = 0, iPrevToNewLength As Integer = 0
		Dim sRegexKey As String = "", sKeyRaw As String = "", sKeyUsed As String
		Dim sValue As String = "", sNewValue As String = "", sTemp As String = "", sNewKey As String = ""
		Dim oTempContextMatches As New SortedList(New CaseInsensitiveComparer()) ' New SortedList(Of String, String)
		Dim sTemp2 As String = ""
		Dim sUserId As String = "", sDomain As String = ""
		Dim iPosMarker As Integer, oMatches As MatchCollection, oMatch As Match, iCurrMatchStart As Integer
		Dim oReconstructLine As New Hashtable, iCharPos As Integer = 1, iTokenPos As Integer = 0, iAftMatchPos As Integer = 0
		Dim sCurrValString As String, sBefore As String, sAfter As String, sPrevValString As String = ""
		Dim aUsedTokenStart As ArrayList, aUsedTokenEnd As ArrayList, sTokenStart As String, sTokenEnd As String, sEvalString As String
		Dim sTokenType As String = "", sLeftOver As String = "", iNewBlockLevel As Integer, bSplitStringSection As Boolean = False
		Dim iPrevIndex As Integer = 0, iNextIndex As Integer = 0, iCurrMatchEnd As Integer, sPrevToken As String
		Dim iBaseCharPos As Integer, iMatchCharPos As Integer, iBefCharPos As Integer, iAftCharPos As Integer, bForceNewVal As Boolean = False
		Dim aExtraStrings As New SortedList(New CaseInsensitiveComparer()), iTemp As Integer, oCurrSubstitutes As New SortedList(New CaseInsensitiveComparer())
		Dim gBefore As Group, gTag As Group, gAfter As Group

		Try

			Me.Clear()
			Me.AddNew(sReturn, iCharPos, False, sReturn)
			'If aExtraStrings Is Nothing Then aExtraStrings = New SortedList(Of String, String)
			If aExtraStringsIn IsNot Nothing Then aExtraStrings = New SortedList(aExtraStringsIn, New CaseInsensitiveComparer()) Else aExtraStrings = New SortedList(New CaseInsensitiveComparer())

			aUsedTokenStart = New ArrayList
			aUsedTokenEnd = New ArrayList

			If UseTokenMarks Then
				aUsedTokenStart = New ArrayList(StartTokens.ToArray)
				aUsedTokenEnd = New ArrayList(EndTokens.ToArray)
				For iTokenPos = aUsedTokenStart.Count - 1 To 0 Step -1
					If sReturn.IndexOf(aUsedTokenStart(iTokenPos).ToString, StringComparison.CurrentCultureIgnoreCase) < 0 Then
						aUsedTokenStart.RemoveAt(iTokenPos)
						aUsedTokenEnd.RemoveAt(iTokenPos)
					End If
				Next
			End If

			'If aUsedTokenStart.Count = 0 Then aUsedTokenStart.Add("")
			'If aUsedTokenEnd.Count = 0 Then aUsedTokenEnd.Add("")

			If sReturn <> "" And aUsedTokenStart.Count > 0 And aUsedTokenEnd.Count > 0 Then
				If oAllSubstituteStrings Is Nothing Then oAllSubstituteStrings = New SortedList(New CaseInsensitiveComparer())
				If ResetValues Or gbResetReplacements Or oAllSubstituteStrings.Count = 0 Then PopulateReplacementArray(aExtraStringsIn, ResetValues) ' Create all the needed values

				iPosMarker = 0
				sRegexKey = ""
				For iTokenPos = 0 To aUsedTokenStart.Count - 1

					sTokenStart = aUsedTokenStart(iTokenPos).ToString
					sTokenEnd = aUsedTokenEnd(iTokenPos).ToString

					' Split the string into block sections based on key words, each with it's own syntax.
					If sTokenStart <> "" Then
						sRegexKey &= IIf(sRegexKey <> "", "|", "").ToString & _
							"(?<TokenStart>" & RegexMetaString(sTokenStart) & _
							"\s*)(?<TokenType>[a-z0-9_.]+)(?<FirstDelim>\s*[|:;\s]?\s*)?(?<LeftOver>[^" & RegexMetaString(Left(sTokenEnd, 1)) & _
							"]+)?(?<TokenEnd>\s*" & RegexMetaString(sTokenEnd) & ")"
					End If
				Next ' token delimiter types

				Do While iPosMarker < Me.Count
					oVal = Me.Item(iPosMarker)
					iBaseCharPos = oVal.CharPos
					sCurrValString = oVal.OrigString
					iPosMarker += 1
					iCharPos = iBaseCharPos
					sPrevToken = oVal.TokenType
					sPrevValString = oVal.OrigString

					oMatches = Regex.Matches(sCurrValString, sRegexKey, RegexOptions.IgnoreCase Or RegexOptions.Multiline)
					iPrevIndex = 0

					iBefCharPos = 0
					'For iMatchPos = 0 To oMatches.Count - 1
					For Each oMatch In oMatches

						If iBefCharPos = 0 Then iBefCharPos = iBaseCharPos
						iMatchCharPos = iBaseCharPos + oMatch.Index - 1
						If oMatch.NextMatch IsNot Nothing AndAlso oMatch.NextMatch.Success Then
							iNextIndex = oMatch.NextMatch.Index
						Else
							iNextIndex = sCurrValString.Length
						End If

						Try
							gBefore = oMatch.Groups("TokenStart")
							gTag = oMatch.Groups("TokenType")
							gAfter = oMatch.Groups("TokenEnd")

							sTokenType = oMatch.Groups("TokenType").Value
							sLeftOver = oMatch.Groups("LeftOver").Value
							sEvalString = ""
							iNewBlockLevel = oVal.BlockLevel
							bSplitStringSection = False
							bForceNewVal = False
							Select Case sTokenType
								Case "If", "beginif", "ifbegin", "begin"
									iNewBlockLevel += 1
									bSplitStringSection = True
									sEvalString = sLeftOver
									sLeftOver = ""
								Case "endif", "ifend", "end"
									iNewBlockLevel -= 1
									bSplitStringSection = True
									bForceNewVal = True
								Case "eval", "evaluate", "compute", "calculate", "calc"
									bSplitStringSection = True
									sEvalString = sLeftOver
									sLeftOver = ""
									bForceNewVal = True
								Case Else
									If oAllSubstituteStrings.ContainsKey(sTokenType) AndAlso Not oCurrSubstitutes.ContainsKey(sTokenType) Then oCurrSubstitutes.Add(sTokenType, oAllSubstituteStrings.Item(sTokenType))
							End Select

							If bSplitStringSection Then

								sBefore = ""
								sAfter = ""
								'If oMatch.Index < oVal.OrigString.Length Then sBefore = oVal.OrigString.Substring(0, oMatch.Index - oVal.CharPos + 1)
								iMatchlength = oMatch.Index - iPrevIndex
								iCurrMatchStart = gBefore.Index
								iCurrMatchEnd = gAfter.Index + gAfter.Length
								iAftCharPos = iMatchCharPos + oMatch.Length
								'iCurrMatchEnd = oMatch.Index + oMatch.Length
								If oMatch.Index - iPrevIndex > 0 Then sBefore = sCurrValString.Substring(iPrevIndex, oMatch.Index - iPrevIndex)
								If iCurrMatchEnd < sCurrValString.Length Then
									sAfter = sCurrValString.Substring(iCurrMatchEnd, iNextIndex - iCurrMatchEnd)
								End If
								iPrevIndex = iAftCharPos

								If sBefore <> "" Or bForceNewVal Or sPrevToken <> "" Then
									oVal.OrigString = sBefore
									oVal.Result = sBefore
									iCharPos = iBaseCharPos + sBefore.Length
									'oVal = New SubstEntry(sLeftOver, iCharPos, True, sLeftOver, sEvalString, iNewBlockLevel, sTokenType)
									oVal = Me.AddNew(sLeftOver, iMatchCharPos, True, sLeftOver, sEvalString, iNewBlockLevel, sTokenType)
									'Me.Insert(iPosMarker, oVal)
									iPosMarker += 1
								Else
									oVal.OrigString = sLeftOver
									oVal.Result = sLeftOver
									oVal.HasBeenSubstituted = True
									oVal.BlockLevel = iNewBlockLevel
									oVal.EvalExpression = sEvalString
									oVal.TokenType = sTokenType
								End If

								iCharPos = oVal.CharPos + oMatch.Length

								If sAfter <> "" Then
									'oVal = New SubstEntry(sAfter, iCharPos, True, sAfter, "", iNewBlockLevel, "")
									oVal = Me.AddNew(sAfter, iAftCharPos, False, sAfter, "", iNewBlockLevel, "")
									'Me.Insert(iPosMarker, oVal)
									iPosMarker += 1
									iCharPos = oVal.CharPos + sAfter.Length
									iBefCharPos = iAftCharPos

								End If
							End If ' splitting up the tokenised section
						Catch ex2 As Exception

						End Try
						sPrevToken = oVal.TokenType
						sPrevValString = oVal.OrigString
					Next '  matches collection cycle

					If iCharPos < sCurrValString.Length And iPosMarker > 1 Then
						sAfter = sCurrValString.Substring(iCharPos)
						oVal = Me.AddNew(sAfter, iMatchCharPos, False, sAfter, "", iNewBlockLevel, "")
					Else
						sAfter = ""
					End If
				Loop ' loop for string sections



				' Check all strings for substitution using the label and value array


				For Each sKeyRaw In oCurrSubstitutes.Keys

					If sKeyRaw <> "" Then
						sNewValue = oCurrSubstitutes.Item(sKeyRaw).ToString
						iPosLast = 1
						sRegexKey = ""
						sKeyUsed = sKeyRaw
						For iTokenPos = 0 To aUsedTokenStart.Count - 1

							sTokenStart = aUsedTokenStart(iTokenPos).ToString
							sTokenEnd = aUsedTokenEnd(iTokenPos).ToString

							sKeyUsed = sTokenStart & sKeyRaw & sTokenEnd

							sRegexKey &= IIf(sRegexKey <> "", "|", "").ToString & IIf(sTokenStart = "", "(?<=^|[^a-z0-9_.])", "").ToString & "(?<FieldName>" & RegexMetaString(sKeyUsed) & ")" & IIf(sTokenEnd = "", "(?=$|[^a-z0-9_.])", "").ToString
						Next

						iPosMarker = 0
						Do While iPosMarker < Me.Count
							oVal = Me.Item(iPosMarker)
							iBaseCharPos = oVal.CharPos
							sCurrValString = oVal.OrigString
							iPrevIndex = 0
							iNextIndex = oVal.OrigString.Length
							iPosMarker += 1
							iCharPos = iBaseCharPos
							iBefCharPos = 0
							sPrevToken = oVal.TokenType
							sPrevValString = oVal.OrigString


							If Not oVal.HasBeenSubstituted And sCurrValString.Length >= sKeyUsed.Length Then
								sBefore = ""
								sAfter = ""
								oMatches = Regex.Matches(sCurrValString, sRegexKey, RegexOptions.IgnoreCase Or RegexOptions.Multiline)
								If oMatches.Count > 0 Then
									'iCharPos = oVal.CharPos

									iPrevMatchEnd = 0
									For Each oMatch In oMatches
										If iBefCharPos = 0 Then iBefCharPos = iBaseCharPos
										iMatchCharPos = iBaseCharPos + oMatch.Index - 1


										If oMatch.NextMatch IsNot Nothing AndAlso oMatch.NextMatch.Success Then iNextIndex = oMatch.NextMatch.Index Else iNextIndex = sCurrValString.Length
										sBefore = ""
										sAfter = ""


										'If oMatch.Index < oVal.OrigString.Length Then sBefore = oVal.OrigString.Substring(0, oMatch.Index - oVal.CharPos + 1)
										'If oMatch.Index > 0 Then sBefore = oVal.OrigString.Substring(0, oMatch.Index)
										iPrevToNewLength = oMatch.Index - iPrevMatchEnd
										iMatchlength = oMatch.Index - iPrevIndex
										iAftCharPos = iMatchCharPos + oMatch.Length
										iAftMatchPos = oMatch.Index + oMatch.Length
										If iPrevToNewLength > 0 Then sBefore = sCurrValString.Substring(iPrevMatchEnd, iPrevToNewLength)
										'If oMatch.Index + oMatch.Length < oVal.OrigString.Length Then sAfter = oVal.OrigString.Substring(oMatch.Index + oMatch.Length)
										'If oMatch.Index + oMatch.Length < sCurrValString.Length And iAftCharPos < sCurrValString.Length Then
										If oMatch.Index + oMatch.Length < sCurrValString.Length And iAftMatchPos < sCurrValString.Length Then
											'iTemp = iNextIndex - iAftCharPos
											iTemp = iNextIndex - iAftMatchPos
											If iTemp >= sCurrValString.Length - iAftMatchPos Then
												'sAfter = sCurrValString.Substring(iAftCharPos)
												sAfter = sCurrValString.Substring(iAftMatchPos)
											Else
												'sAfter = sCurrValString.Substring(iAftCharPos, iTemp)
												sAfter = sCurrValString.Substring(iAftMatchPos, iTemp)
											End If

										End If
										iPrevIndex = iAftCharPos
										iPrevMatchEnd = iAftMatchPos

										If sBefore <> "" Or sPrevToken <> "" Then
											oVal.OrigString = sBefore
											oVal.Result = sBefore
											iCharPos = oVal.CharPos + sBefore.Length
											'oVal = New SubstEntry(oMatch.Value, iCharPos, True, sNewValue, "", oVal.BlockLevel, "")
											oVal = Me.AddNew(oMatch.Value, iCharPos, True, sNewValue, "", oVal.BlockLevel, "")
											'Me.Insert(iPosMarker, oVal)
											iPosMarker += 1
										Else
											oVal.OrigString = oMatch.Value
											oVal.Result = sNewValue
											oVal.HasBeenSubstituted = True
										End If

										iCharPos = oVal.CharPos + oMatch.Length

										If sAfter <> "" Then
											'oVal = New SubstEntry(sAfter, iCharPos, True, sAfter, "", oVal.BlockLevel, "")
											'oVal = Me.AddNew(sAfter, iCharPos, False, sAfter, "", oVal.BlockLevel, "")
											oVal = Me.AddNew(sAfter, iAftCharPos, False, sAfter, "", oVal.BlockLevel, "")
											'Me.Insert(iPosMarker, oVal)
											'iPosMarker += 1
											iCharPos = oVal.CharPos + sAfter.Length
										End If

										sPrevToken = oVal.TokenType
										sPrevValString = oVal.OrigString
									Next ' Matches count
								End If ' Has matches end block

							End If ' Match value length could be in tokenised string
						Loop ' Loop for string sections

					End If
				Next ' Substitution key value pairs
			End If

		Catch ex As Exception
			Dim aDetail As New ArrayList
			aDetail.Add("Examining string of """ & sReturn & """")
			If sKeyRaw > "" Then aDetail.Add("Last key of """ & sKeyRaw & """ with value """ & sNewValue & """")
			RecordError(ex, aDetail)

		End Try

	End Sub


	Private Function RegexMetaString(sOrig As String) As String
		Dim sReturn As String = sOrig, sVal As String = ""
		Dim aMetaVals() As String = {"\", "$", "<", ">", "{", "}", "[", "]", "(", ")", "|", ".", "*", "?", "+"}
		Try

			For Each sVal In aMetaVals
				sReturn = sReturn.Replace(sVal, "\" & sVal)
			Next

		Catch ex As Exception
			Dim aDetail As New ArrayList
			aDetail.Add("Examining string of """ & sReturn & """")
			If sVal > "" Then aDetail.Add("Last Meta of """ & sVal)
			RecordError(ex, aDetail)

		End Try
		Return sReturn
	End Function

	Public Sub New()

	End Sub

	Public Sub New(StringToUpdate As String)
		Try
			PopulateReplacementArray()
			Me.OriginalString = StringToUpdate

		Catch ex As Exception
			Dim aDetail As New ArrayList
			aDetail.Add("Opening with string of """ & StringToUpdate & """")
			RecordError(ex, aDetail)

		End Try

	End Sub

	Public Sub New(StringToUpdate As String, oVariableSubstitutes As SortedList(Of String, String))
		Try
			PopulateReplacementArray(oVariableSubstitutes)
			Me.OriginalString = StringToUpdate

		Catch ex As Exception
			Dim aDetail As New ArrayList
			aDetail.Add("Opening with string of """ & StringToUpdate & """")
			RecordError(ex, aDetail)

		End Try

	End Sub

	Public Sub AddNewValues(ValuesList As SortedList(Of String, String))
		Dim aExtraStrings As New SortedList(New CaseInsensitiveComparer())
		Dim sValue As String, sKey As String
		Try

			If oAllSubstituteStrings Is Nothing OrElse oAllSubstituteStrings.Count = 0 Then
				PopulateReplacementArray(ValuesList, False)

			Else

				aExtraStrings = New SortedList(ValuesList, New CaseInsensitiveComparer())

				For Each sKey In aExtraStrings.Keys
					Try

						sValue = aExtraStrings.Item(sKey).ToString
						If Not oAllSubstituteStrings.ContainsKey(sKey) Then oAllSubstituteStrings.Add(sKey, sValue)
					Catch ex2 As Exception
						If Not oAllSubstituteStrings.ContainsKey(sKey) Then oAllSubstituteStrings.Add(sKey, "")
					End Try
				Next

			End If

		Catch ex As Exception
			Dim aDetail As New ArrayList
			RecordError(ex, aDetail)

		End Try
	End Sub


	Public Sub AddNewValue(TokenKey As String, Value As String)
		Try

			Try
				If Not oAllSubstituteStrings.ContainsKey(TokenKey) Then oAllSubstituteStrings.Add(TokenKey, Value)
			Catch ex2 As Exception
				If Not oAllSubstituteStrings.ContainsKey(TokenKey) Then oAllSubstituteStrings.Add(TokenKey, "")
			End Try

		Catch ex As Exception
			Dim aDetail As New ArrayList
			RecordError(ex, aDetail)

		End Try
	End Sub


	Public Function AddNew(OrigString As String, Position As Integer, HasBeenSustituted As Boolean, NewString As String) As SubstituteEntry
		Dim oNewVal As SubstituteEntry = Nothing
		Try
			oNewVal = Me.AddNew(OrigString, Position, HasBeenSustituted, NewString, "", 1, "")
		Catch ex As Exception
			Dim aDetail As New ArrayList
			RecordError(ex, aDetail)

		End Try
		Return oNewVal
	End Function


	Public Function AddNew(OrigString As String, Position As Integer, HasBeenSustituted As Boolean, NewString As String, EvalExpression As String, BlockLevel As Integer, TokenType As String) As SubstituteEntry
		Dim oNewVal As SubstituteEntry = Nothing, oVal As SubstituteEntry = Nothing, iPos As Integer
		Try
			oNewVal = New SubstituteEntry(OrigString, Position, HasBeenSustituted, NewString, EvalExpression, BlockLevel, TokenType)
			oNewVal.oParentCol = Me

			For iPos = 0 To Me.Count - 1
				oVal = Me.Item(iPos)
				If oVal.CharPos > Position Then Exit For
				If oVal.CharPos = Position Then
					iPos += 1
					Exit For
				End If
				oVal = Nothing
			Next

			If oVal IsNot Nothing Then
				Me.Insert(iPos, oNewVal)
			Else
				Me.Add(oNewVal)
			End If

		Catch ex As Exception

			Dim aDetail As New ArrayList
			RecordError(ex, aDetail)

		End Try
		Return oNewVal
	End Function


	Protected Friend Sub RecordError(exIn As Exception, ExtraDets As ArrayList)
		Try

			'Dim oErr As wRuntimeError = goErrs.NewError(exIn)
			'If ExtraDets IsNot Nothing AndAlso ExtraDets.Count > 0 Then oErr.sExtraDetail &= String.Join("<br/>", ExtraDets)
			'gsErr &= "<br /><br />" & Replace(oErr.ToString, vbCrLf, "<br />")

		Catch ex As Exception

		End Try
	End Sub

End Class

Public Class SubstituteEntry
	Implements IComparable(Of SubstituteEntry)

	Protected Friend CharPos As Integer = 0
	Protected Friend OrigString As String = ""
	Protected Friend HasBeenSubstituted As Boolean = False
	Dim sResult As String = ""
	Protected Friend BlockLevel As Integer = 1
	Protected Friend EvalExpression As String = ""
	Protected Friend TokenType As String = ""

	Protected Friend oParentCol As clsValueSubstitutions

	Protected Friend Property Result As String
		Get
			If sResult = "" And EvalExpression <> "" Then
				sResult = Me.EvalResult.ToString
			End If
			Return sResult
		End Get
		Set(value As String)
			sResult = value
		End Set
	End Property



	Public Function CompareTo(other As SubstituteEntry) As Integer Implements IComparable(Of SubstituteEntry).CompareTo
		Return Me.CharPos.CompareTo(other.CharPos)
	End Function

	Public Sub New(OrigString As String, Position As Integer, HasBeenSustituted As Boolean, NewString As String)

		Me.New(OrigString, Position, HasBeenSustituted, NewString, "", 1, "")

	End Sub

	Public Sub New(OrigString As String, Position As Integer, HasBeenSustituted As Boolean, NewString As String, EvalExpression As String, BlockLevel As Integer, TokenType As String)

		Me.OrigString = OrigString
		Me.CharPos = Position
		Me.HasBeenSubstituted = HasBeenSustituted
		Me.Result = NewString
		Me.EvalExpression = EvalExpression
		Me.BlockLevel = BlockLevel
		Me.TokenType = TokenType

	End Sub


	Public Sub New()

	End Sub


	Public Function EvalIsValid() As Boolean
		Dim oChildSubstitution As New clsValueSubstitutions
		Dim bReturn As Boolean = False, sReplacedOrig As String = ""
		Dim oEvalResponse As Object = Nothing
		Try

			If EvalExpression = "" Then
				bReturn = (Result <> "")
			Else
				oEvalResponse = Me.EvalResult

				bReturn = oEvalResponse IsNot Nothing AndAlso (IsNumeric(oEvalResponse) AndAlso oEvalResponse.ToString <> "" AndAlso CDbl(oEvalResponse) <> 0) Or _
						(TypeOf (oEvalResponse) Is Boolean AndAlso CBool(oEvalResponse)) Or _
						(Not (TypeOf (oEvalResponse) Is Boolean Or IsNumeric(oEvalResponse)) And oEvalResponse.ToString <> "")

			End If


		Catch ex As Exception
			Dim aDetail As New ArrayList
			If EvalExpression <> "" Then aDetail.Add("Generating eval string of """ & EvalExpression & """")
			If sReplacedOrig <> "" Then aDetail.Add("Evaluating string of """ & sReplacedOrig & """")
			If oEvalResponse IsNot Nothing Then aDetail.Add("Eval result type is " & oEvalResponse.GetType.ToString & " with value of """ & oEvalResponse.ToString & """")

			RecordError(ex, aDetail)

		End Try
		Return bReturn
	End Function


	Public Function EvalResult() As Object
		Dim oChildSubstitution As New clsValueSubstitutions
		Dim bReturn As Boolean = False, sReplacedOrig As String = ""
		'Dim oScript As MSScriptControl.ScriptControl, oEvalResponse As Object = Nothing
		Dim oEvalResponse As Object = Nothing
		Try

			If EvalExpression = "" Then
				oEvalResponse = ""
			Else
				'oChildSubstitution = New clsValueSustitutions(EvalExpression, oParentCol.oAllSubstituteStrings)
				oChildSubstitution = New clsValueSubstitutions
				oChildSubstitution.oAllSubstituteStrings = oParentCol.oAllSubstituteStrings
				oChildSubstitution.OriginalString = EvalExpression

				sReplacedOrig = oChildSubstitution.Result
				If Regex.IsMatch(EvalExpression, "[(+\-*^/=<>!]") Then

					Try

						'oScript = New MSScriptControl.ScriptControl
						'oScript.Language = "VBSCRIPT"
						'oEvalResponse = oScript.Eval(sReplacedOrig)
						oEvalResponse = New clsEvalExpression(sReplacedOrig).Result

					Catch ex As Exception
						'oEvalResponse = "Syntax Error in evaluating """ & sReplacedOrig & """"
						oEvalResponse = ex.TargetSite.ReflectedType.ToString & vbCrLf & _
							ex.Source & vbCrLf & _
							"Error in evaluating """ & sReplacedOrig & """" & vbCrLf & _
							ex.Message & vbCrLf & _
							ex.StackTrace
					End Try

				ElseIf oParentCol.oAllSubstituteStrings.ContainsKey(EvalExpression) Then
					oEvalResponse = oParentCol.oAllSubstituteStrings.Item(EvalExpression)

				Else
					oEvalResponse = sReplacedOrig
				End If

			End If

			If oEvalResponse.ToString = """""" Then oEvalResponse = ""

		Catch ex As Exception
			Dim aDetail As New ArrayList
			If EvalExpression <> "" Then aDetail.Add("Generating eval string of """ & EvalExpression & """")
			If sReplacedOrig <> "" Then aDetail.Add("Evaluating string of """ & sReplacedOrig & """")
			If oEvalResponse IsNot Nothing Then aDetail.Add("Eval result type is " & oEvalResponse.GetType.ToString & " with value of """ & oEvalResponse.ToString & """")

			RecordError(ex, aDetail)

		End Try
		Return oEvalResponse

	End Function



	Protected Friend Sub RecordError(exIn As Exception, ExtraDets As ArrayList)
		Try

            Dim oErr As ErrorItem = glErrors.NewError(exIn, ExtraDets)

			'Dim oErr As wRuntimeError = goErrs.NewError(exIn)
			'If ExtraDets IsNot Nothing AndAlso ExtraDets.Count > 0 Then oErr.sExtraDetail &= String.Join("<br/>", ExtraDets)
			'gsErr &= "<br /><br />" & Replace(oErr.ToString, vbCrLf, "<br />")

		Catch ex As Exception

		End Try

	End Sub

End Class


' ==================================================================================
' Version : 1.0.0
' Author  : R Collins of TDC
' Date    : 24 August 2015
' ==================================================================================

Public Class clsEvalExpression

	Private sExpression As String = ""
	Private sResult As String = ""
	Private sCompileErrors As New ArrayList
	Private sClassTemplate As String

	Public ReadOnly Property Result As Object
		Get
			Dim oResult As Object = Nothing
			Try
				oResult = Evaluate()

			Catch ex As Exception
				Dim aDetail As New ArrayList
				RecordError(ex, aDetail)

			End Try
			Return oResult
		End Get
	End Property


	Public Property ClassTemplate As String
		Get
			Try
				If sClassTemplate = "" Then
					sClassTemplate = "Imports System " & vbCrLf & _
					"Imports Microsoft " & vbCrLf & _
					"Imports Microsoft.VisualBasic " & vbCrLf & _
					"Imports Microsoft.VisualBasic.Strings " & vbCrLf & _
					"Imports System.Math " & vbCrLf & _
					"Namespace AnyNamespace " & vbCrLf & _
						"Public Class AnyClass " & vbCrLf & _
							"Public Function AnyMethod() As object " & vbCrLf & _
								"Return {0}" & vbCrLf & _
							"End Function " & vbCrLf & _
						"End Class " & vbCrLf & _
					"End Namespace"
				End If
			Catch ex As Exception
				Dim aDetail As New ArrayList
				RecordError(ex, aDetail)

			End Try

			Return sClassTemplate
		End Get
		Set(value As String)

			sClassTemplate = value

		End Set
	End Property


	Public Property Expression As String
		Get
			Return sExpression
		End Get
		Set(value As String)
			sExpression = value
			sResult = ""
		End Set
	End Property


	Public ReadOnly Property CompileErrorList As ArrayList
		Get
			Return sCompileErrors
		End Get
	End Property

	Public ReadOnly Property CompileErrorsAsString As String
		Get
			Dim sResult As String = ""
			Try
				If sCompileErrors.Count > 0 Then sResult = Join(sCompileErrors.ToArray, vbCrLf)

			Catch ex As Exception
				Dim aDetail As New ArrayList
				RecordError(ex, aDetail)

			End Try

			Return sResult
		End Get
	End Property


	Public Function Evaluate(Expression As String) As Object
		sExpression = Expression
		sResult = ""

		Return Me.Result
		' Evaluate()
	End Function


	Public Function Evaluate() As Object
		Dim oReturn As Object = Nothing, oReturnClass As MethodInfo
		Dim oCompParams As New CompilerParameters
		Dim oCompResults As CompilerResults
		Dim aAssembly As Assembly, oAssemblyClass As Object
		Dim sClassCode As String = ""
		Dim sNamespace As String = "AnyNamespace", sClassFullName As String = "AnyNamespace.AnyClass"
		Dim sClassName As String = "AnyClass", sFunctionName As String = "AnyMethod"
		Dim aTypes() As Type, oAssembledType As Type, aMethods() As MethodInfo, oMethodTest As MethodInfo

		Try
			sResult = ""

			sClassCode = String.Format(Me.ClassTemplate, sExpression)

			oCompParams.GenerateExecutable = False
			oCompParams.GenerateInMemory = True

			oCompResults = New VBCodeProvider().CompileAssemblyFromSource(oCompParams, sClassCode)

			If oCompResults.Errors.Count > 0 Then
				For Each oErr In oCompResults.Errors
					sCompileErrors.Add(oErr.ToString)
				Next

			Else

				aAssembly = oCompResults.CompiledAssembly
				aTypes = aAssembly.GetTypes()
				For iPos = aTypes.Count - 1 To 0 Step -1
					oAssembledType = aTypes(iPos)
					If oAssembledType.IsVisible Then
						sNamespace = oAssembledType.Namespace
						sClassFullName = oAssembledType.FullName
						sClassName = oAssembledType.Name
						aMethods = oAssembledType.GetMethods()
						For iPos2 = 0 To aMethods.Count - 1
							oMethodTest = aMethods(iPos2)
							If oMethodTest.DeclaringType.Namespace = sNamespace And oMethodTest.DeclaringType.Name = sClassName Then
								sFunctionName = oMethodTest.Name
								Exit For
							End If

						Next
						Exit For
					End If

				Next

				oAssemblyClass = aAssembly.CreateInstance(sClassFullName)

				oReturnClass = oAssemblyClass.GetType().GetMethod(sFunctionName)
				oReturn = oReturnClass.Invoke(oAssemblyClass, Nothing)

				sResult = oReturn.ToString

			End If

		Catch ex As Exception
			Dim aDetail As New ArrayList

			aDetail.Add("Expression is " & sExpression)
			aDetail.Add("Class generated is  " & sClassCode)

			RecordError(ex, aDetail)

		End Try

		Return oReturn
	End Function


	Public Sub New(Expression As String)
		sExpression = Expression
	End Sub




	Public Sub New()

	End Sub


	Public Shadows Function ToString() As String
		Dim sReturn As String = "", oReturn As Object = Nothing
		Try

			oReturn = Evaluate()
			If oReturn IsNot Nothing Then sReturn = oReturn.ToString

		Catch ex As Exception

			Dim aDetail As New ArrayList
			aDetail.Add("Return is " & oReturn.ToString)
			RecordError(ex, aDetail)

		End Try

		Return sReturn
	End Function




	Protected Friend Sub RecordError(exIn As Exception, ExtraDets As ArrayList)
		Try

            Dim oErr As ErrorItem = glErrors.NewError(exIn, ExtraDets)
			'If ExtraDets IsNot Nothing AndAlso ExtraDets.Count > 0 Then oErr.sExtraDetail &= String.Join("<br/>", ExtraDets)
			'gsErr &= "<br /><br />" & Replace(oErr.ToString, vbCrLf, "<br />")

		Catch ex As Exception


		End Try

	End Sub
End Class
