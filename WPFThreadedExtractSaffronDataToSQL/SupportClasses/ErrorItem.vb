Imports System
Public Class ErrorItem

    Protected Friend sErrorCode As String = ""
    Protected Friend sErrorMsg As String = ""
    Protected Friend sErrorType As String = ""
    Protected Friend sModule As String = ""
    Protected Friend sFunction As String = ""
	Protected Friend sExtraDetail As String
	Protected Friend sLineNo As Long = 0
	Protected Friend oSourceEx As Exception = Nothing
	Protected Friend oStackList As New List(Of String)

	Protected Friend Sub New(ByVal ex As Exception)
		If TypeOf ex Is Threading.ThreadAbortException Then
			'Application.DoEvents()
		Else
			oSourceEx = ex
			sErrorMsg = oSourceEx.Message
			sErrorType = oSourceEx.GetType.ToString
			sModule = Me.GetType.ToString
			If oSourceEx.TargetSite IsNot Nothing AndAlso oSourceEx.TargetSite.ReflectedType IsNot Nothing Then sFunction = oSourceEx.TargetSite.ReflectedType.ToString & "  "
			sFunction &= oSourceEx.Source

			StackToString()
			LogFileEntry(ToString, True)
		End If

	End Sub

    Protected Friend Sub New(ByVal ex As Exception, ByVal aExtra As ArrayList)
        If TypeOf ex Is Threading.ThreadAbortException Then
            'Application.DoEvents()
        Else
            oSourceEx = ex
            sErrorMsg = oSourceEx.Message
            sErrorType = oSourceEx.GetType.ToString
            sModule = Me.GetType.ToString
            If oSourceEx.TargetSite IsNot Nothing AndAlso oSourceEx.TargetSite.ReflectedType IsNot Nothing Then sFunction = oSourceEx.TargetSite.ReflectedType.ToString & "  "
            sFunction &= oSourceEx.Source
            sExtraDetail = Join(aExtra.ToArray, vbCrLf)
            StackToString()
            LogFileEntry(ToString, True)
        End If

    End Sub

	Protected Friend Sub New(ByVal ex As Exception, ByVal Extra As String)
		If TypeOf ex Is Threading.ThreadAbortException Then
			'Application.DoEvents()
		Else
			oSourceEx = ex
			sErrorMsg = oSourceEx.Message
			sErrorType = oSourceEx.GetType.ToString
			sModule = Me.GetType.ToString
			If oSourceEx.TargetSite IsNot Nothing AndAlso oSourceEx.TargetSite.ReflectedType IsNot Nothing Then sFunction = oSourceEx.TargetSite.ReflectedType.ToString & "  "
			sFunction &= oSourceEx.Source
			sExtraDetail = Extra
			StackToString()
			LogFileEntry(ToString, True)
		End If
	End Sub

	Protected Friend Sub New()
	End Sub

	Public Overrides Function ToString() As String
		Dim sOut As String = ""
		sOut = sErrorType & " Error located in module " & sModule & " in function " & sFunction
		If sLineNo > 0 Then sOut = sOut & " at line " & sLineNo.ToString
		If sExtraDetail > "" Then sOut = sOut & vbCrLf & " executing " & sExtraDetail
		sOut = sOut & vbCrLf & sErrorMsg
		For Each sTemp In Me.oStackList
			sOut = sOut & vbCrLf & "   " & sTemp
		Next
		Return sOut
	End Function


    Private Sub StackToString()
        Dim sResponse As String, oFS As StackFrame, oFrames As StackFrame()
        Dim sTemp As String
        Dim oStack As New StackFrame(2, True)
        Dim oST As New StackTrace(oStack)
        Try
            oStackList = New List(Of String)
            If oSourceEx IsNot Nothing Then
                oStack = New StackFrame(2, True)
                oST = New StackTrace(oStack)
                oFrames = oST.GetFrames
                For Each oFS In oFrames
                    sTemp = "File : " & oFS.GetFileName
                    sResponse = sTemp & Space(50 - sTemp.Length)
                    sTemp = "  Line " & oFS.GetFileLineNumber
                    sResponse &= sTemp & Space(12 - sTemp.Length)
                    sTemp = "  Col " & oFS.GetFileColumnNumber
                    sResponse &= sTemp & Space(10 - sTemp.Length)
                    sTemp = "  Procedure : " & oFS.GetMethod.Name
                    sResponse &= sTemp & Space(50 - sTemp.Length)
                    oStackList.Add(sResponse)
                Next
            End If

        Catch ex As Exception

        End Try
    End Sub

End Class
