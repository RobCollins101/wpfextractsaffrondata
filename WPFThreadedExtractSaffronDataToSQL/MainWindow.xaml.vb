﻿Imports System.Data
Imports System.Windows.Threading
'Imports WPFThreadedBatchProcessing

Class MainWindow

	Protected Friend Delegate Sub NoArgDelegate()
	Protected Friend Delegate Sub OneArgDelegate(arg As Object)
	Protected Friend Delegate Sub TwoArgDelegate(arg1 As Object, arg2 As Object)
	Protected Friend Delegate Sub ThreeArgDelegate(arg1 As Object, arg2 As Object, arg3 As Object)
	Protected Friend Delegate Sub FourArgDelegate(arg1 As Object, arg2 As Object, arg3 As Object, arg4 As Object)
	Protected Friend Delegate Sub FiveArgDelegate(arg1 As Object, arg2 As Object, arg3 As Object, arg4 As Object, arg5 As Object)

	Dim WithEvents oMgProcess As clsManageProcesses

	' see http://www.codeproject.com/Articles/10311/What-s-up-with-BeginInvoke 
	Sub VerifyControls()
		Throw New NotImplementedException
	End Sub



	Private Sub cmdAbort_Click(sender As Object, e As RoutedEventArgs) Handles cmdAbort.Click

		cmdAbort.Visibility = False
		goManageProcess.CancelProcess()

	End Sub



	Private Sub cmdActivate_Click(sender As Object, e As RoutedEventArgs) Handles cmdActivate.Click
		Dim oTemp As Object, sLogFilePath As String
		Dim oBuilder As New SqlClient.SqlConnectionStringBuilder

		cmdActivate.IsEnabled = False
		txtUVServer.IsEnabled = False
		txtUVDatabase.IsEnabled = False
		txtUVUser.IsEnabled = False
		txtUVPassword.IsEnabled = False
		txtSQLServer.IsEnabled = False
		txtSQLDatabase.IsEnabled = False
		'If txtDestinationFolder.Text = "" Then
		'    txtMessage.Text = "No destination folder"
		'    cmdActivate.IsEnabled = True

		'ElseIf Not IO.Directory.Exists(txtDestinationFolder.text) Then
		'    txtMessage.Text = "Destination folder does not exist"
		'    cmdActivate.IsEnabled = True

		'Else

		'My.Settings.LastDestinationFolder = txtDestinationFolder.Text

		My.Settings.UVServer = txtUVServer.Text
		My.Settings.UVDatabase = txtUVDatabase.Text
		My.Settings.UVUser = txtUVUser.Text
		My.Settings.UVPassword = txtUVPassword.Password
		My.Settings.SQLServer = txtSQLServer.Text
		My.Settings.SQLDatabase = txtSQLDatabase.Text

		My.Settings.Save()

		gsSQLConnectString = GetMySetting("DestinationSQLDatabase")

		If My.Settings.SQLDatabase <> "" And My.Settings.SQLServer <> "" Then

			oBuilder.ConnectionString = gsSQLConnectString
			oBuilder.DataSource = My.Settings.SQLServer
			oBuilder.InitialCatalog = My.Settings.SQLDatabase
			gsSQLConnectString = oBuilder.ConnectionString

		End If

		gUniverseDBSystem = My.Settings.UVServer
		gUniverseDBAccount = My.Settings.UVDatabase
		gUniverseDBUserID = My.Settings.UVUser
		gUniverseDBPassword = My.Settings.UVPassword


		oTemp = GetMySetting("LogFilePath")
		If oTemp IsNot Nothing AndAlso TypeOf (oTemp) Is String Then
			sLogFilePath = oTemp.ToString
			IO.File.Delete(sLogFilePath)
			'IO.File.WriteAllText(sLogFilePath, "")
		End If

		My.Settings.Save()

		cmdAbort.Visibility = Visibility.Visible

		'txtMessage.Text = "Process started"

		txtMessage.Text = ""
		AddToTextLogArea("Process started")

		If goExtractData Is Nothing Then goExtractData = New clsExtractData
		'goExtractData.DestinationFolder = txtDestinationFolder.Text
		prgProgress.Maximum = 1000
		goExtractData.iTotalSteps = prgProgress.Maximum

		RemoveHandler goManageProcess.ErrorOccured, AddressOf oMgProcess_ErrorOccured
		RemoveHandler goManageProcess.ProcessFinished, AddressOf oMgProcess_ProcessFinished
		RemoveHandler goManageProcess.ProcessFailed, AddressOf oMgProcess_ProcessFailed


		AddHandler goManageProcess.ErrorOccured, AddressOf oMgProcess_ErrorOccured
		AddHandler goManageProcess.ProcessFinished, AddressOf oMgProcess_ProcessFinished
		AddHandler goManageProcess.ProcessFailed, AddressOf oMgProcess_ProcessFailed

		goManageProcess.oCallingForm = Me
		'goManageProcess.oControlWithDelegate = txtResult
		goExtractData.bSpecialOnly = chkDoSpecialFiles.IsChecked
		goExtractData.bMissingOnly = chkDoNewOnly.IsChecked

		goManageProcess.ActivateProcessTransferUVFiles(goExtractData)

		'End If


	End Sub

	Public Sub ForceRedrawWindowWithHack(element As UIElement)

		Dim emptyAction As Action
		element.Dispatcher.Invoke(DispatcherPriority.Render, emptyAction)

	End Sub

	Private Sub oMgProcess_ErrorOccured(oEvent As ErrorItem) Handles oMgProcess.ErrorOccured

		cmdAbort.Visibility = Visibility.Hidden
		cmdActivate.IsEnabled = True
		txtUVServer.IsEnabled = True
		txtUVDatabase.IsEnabled = True
		txtUVUser.IsEnabled = True
		txtUVPassword.IsEnabled = True
		txtSQLServer.IsEnabled = True
		txtSQLDatabase.IsEnabled = True
		chkDoSpecialFiles.IsEnabled = True
		chkDoNewOnly.IsEnabled = True

		'txtMessage.Text &= oEvent.ToString
		'txtMessage.ScrollToEnd()

		AddToTextLogArea(oEvent.ToString)

	End Sub

	Private Sub oMgProcess_ProcessFailed(FailedMessage As String) Handles oMgProcess.ProcessFailed

		cmdAbort.Visibility = Visibility.Hidden
		cmdActivate.IsEnabled = True
		txtUVServer.IsEnabled = True
		txtUVDatabase.IsEnabled = True
		txtUVUser.IsEnabled = True
		txtUVPassword.IsEnabled = True
		txtSQLServer.IsEnabled = True
		txtSQLDatabase.IsEnabled = True
		chkDoSpecialFiles.IsEnabled = True
		chkDoNewOnly.IsEnabled = True
		'txtMessage.Text &= FailedMessage
		'txtMessage.ScrollToEnd()

		AddToTextLogArea(FailedMessage)

	End Sub

	Private Sub oMgProcess_ProcessFinished() Handles oMgProcess.ProcessFinished

		cmdAbort.Visibility = Visibility.Hidden
		cmdActivate.IsEnabled = True
		txtUVServer.IsEnabled = True
		txtUVDatabase.IsEnabled = True
		txtUVUser.IsEnabled = True
		txtUVPassword.IsEnabled = True
		txtSQLServer.IsEnabled = True
		txtSQLDatabase.IsEnabled = True
		chkDoSpecialFiles.IsEnabled = True
		chkDoNewOnly.IsEnabled = True

		'txtMessage.Text &= vbCrLf & vbCrLf & "PRocess finished"
		'txtMessage.ScrollToEnd()

		AddToTextLogArea(vbCrLf & vbCrLf & "Process finished")

		prgProgress.Visibility = Visibility.Hidden

	End Sub


	Public Sub AddToTextLogArea(sTextLines As String)
		Dim oTemp As Object, sLogFilePath As String
		Try

			oTemp = GetMySetting("LogFilePath")
			If oTemp IsNot Nothing AndAlso TypeOf (oTemp) Is String Then
				sLogFilePath = oTemp.ToString
				IO.File.AppendAllText(sLogFilePath, vbCrLf & sTextLines)
			End If

			txtMessage.Text &= IIf(txtMessage.Text <> "", vbCrLf, "").ToString & sTextLines
			txtMessage.ScrollToEnd()

		Catch ex As Exception

		End Try
	End Sub


	'Private Sub cmdBrowseDestination_Click(sender As Object, e As RoutedEventArgs) Handles cmdBrowseDestination.Click
	'    Dim oDLG As New FolderBrowserDialog, sFolder As String, oResponse As Forms.DialogResult

	'    Try
	'        sFolder = txtDestinationFolder.Text
	'        If sFolder = "" Then sFolder = IO.Path.GetTempPath

	'        oDLG.SelectedPath = sFolder
	'        oResponse = oDLG.ShowDialog()
	'        If oResponse = Forms.DialogResult.OK Then
	'            txtDestinationFolder.Text = oDLG.SelectedPath
	'        End If

	'    Catch ex As Exception

	'    End Try

	'End Sub

	Private Sub MainWindow_Loaded(sender As Object, e As RoutedEventArgs) Handles Me.Loaded
		Try
			'txtDestinationFolder.Text = My.Settings.LastDestinationFolder

		Catch ex As Exception

		End Try
	End Sub

	Private Sub MainWindow_Initialized(sender As Object, e As EventArgs) Handles Me.Initialized
		Try

			txtUVServer.Text = My.Settings.UVServer
			txtUVDatabase.Text = My.Settings.UVDatabase
			txtUVUser.Text = My.Settings.UVUser
			txtUVPassword.Password = My.Settings.UVPassword
			txtSQLServer.Text = My.Settings.SQLServer
			txtSQLDatabase.Text = My.Settings.SQLDatabase

		Catch ex As Exception

		End Try
	End Sub
End Class
