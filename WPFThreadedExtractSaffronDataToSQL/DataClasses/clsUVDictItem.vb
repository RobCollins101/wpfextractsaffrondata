﻿
Public Class clsUVDictItem
	Protected Friend UVDictName As String = ""
	Protected Friend UVDictNameOnExport As String = ""
	Protected Friend ColumnHeading As String = ""
	Protected Friend DictType As String = ""
	Protected Friend AttrNo As String = ""
	Protected Friend IConv As String = ""
	Protected Friend OConv As String = ""
	Protected Friend Length As Integer = 10
	Protected Friend KeyField As Boolean = False
	Protected Friend HasData As Boolean = False
	Protected Friend Indexed As Boolean = False
	Protected Friend Property iAttrNo As Integer
		Get
			Dim iReturn As Integer = 0
			If IsNumeric(AttrNo) Then iReturn = CInt(Math.Floor(Val(AttrNo)))
			Return iReturn
		End Get
		Set(value As Integer)
			AttrNo = value.ToString
		End Set
	End Property



	Function ColumnHeadingClean() As String
		Dim sReturn As String
		sReturn = StrConv(UVDictNameOnExport.Replace(",", " ").Replace(":", " ").Replace("@", " ").Replace(UV_VM, " ").Replace(UV_SM, " ").Replace(Oth_VM, " ").Replace(Oth_SM, " ").Replace(".", " ").Replace("(", " ").Replace(")", " ").Replace("<", " ").Replace(">", " ").Replace("/", " ").Replace("\", " ").Replace("[", " ").Replace("]", " ").Replace("-", " ").Replace("+", " ").Replace("?", " ").Replace("'", " ").Replace("`", " ").Replace("""", " ").Replace("*", " ").Replace("#", " ").Replace("|", " ").Replace("$", " ").Replace("%", " ").Replace("^", " ").Replace("&", " ").Replace(",", " "), VbStrConv.ProperCase).Replace(" ", "")
		If Left(sReturn, 1) = "_" Then sReturn = AttrNo & sReturn
		If IsNumeric(Left(sReturn, 1)) Then sReturn = "Attr" & sReturn
		If sReturn = "" Then sReturn = "Attr" & iAttrNo.ToString("D3")

		Return sReturn
	End Function



	Function AsSQLColumnDefn(bIDPrimary As Boolean) As String
		Dim sReturn As String = ""
		Try

			sReturn &= IIf(sReturn <> "", "," & vbCrLf, "")
			sReturn &= Me.ColumnHeadingClean

			If (Me.KeyField And bIDPrimary) Or iAttrNo = 0 Then
				sReturn &= " nvarchar(50)"
				sReturn &= " NOT NULL PRIMARY KEY"

			ElseIf Me.Indexed Then
				sReturn &= " nvarchar(MAX)"

			ElseIf Me.OConv = "" Then
				sReturn &= " nvarchar(MAX)"
				'sReturn &= " nvarchar(" & IIf(KeyField, Length.ToString, "MAX").ToString & ")"

			Else
				sReturn &= " nvarchar(MAX)"
				'sReturn &= " nvarchar(" & IIf(KeyField, Length.ToString, "MAX").ToString & ")"
			End If

		Catch ex As Exception
			ReportErrorGlobal(New ErrorItem(ex))
		End Try
		Return sReturn
	End Function


	Function AsSQLIndexColumnDefn() As String
		Dim sReturn As String = ""
		Try

			sReturn &= IIf(sReturn <> "", "," & vbCrLf, "")
			sReturn &= Me.ColumnHeadingClean

			If Me.Indexed Or Me.KeyField Or iAttrNo = 0 Then
				sReturn &= " nvarchar(50)"

			ElseIf Me.OConv = "" Then
				sReturn &= " nvarchar(MAX)"
				'sReturn &= " nvarchar(" & IIf(KeyField, Length.ToString, "MAX").ToString & ")"

			Else
				sReturn &= " nvarchar(MAX)"
				'sReturn &= " nvarchar(" & IIf(KeyField, Length.ToString, "MAX").ToString & ")"
			End If

		Catch ex As Exception
			ReportErrorGlobal(New ErrorItem(ex))
		End Try
		Return sReturn
	End Function


End Class


