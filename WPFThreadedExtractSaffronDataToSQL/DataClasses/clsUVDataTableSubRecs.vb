﻿
Imports System.Data.SqlClient

Public Class clsDataTableSubRecs
	Protected Friend sSubTableName As String = ""
	Protected aSourceDictItems As New SortedList(Of Integer, clsUVDictItem)
	Protected aSourceDataValues As New SortedList(Of Integer, String)
	Protected oParentKeyFieldName As clsUVDictItem

	Protected Friend oParentObj As clsUVData

	Protected Friend Sub GenerateSubTable()
		Dim sQuery As String = "", sQueryIndex As String = "", sQueryFields As String = ""
		Dim iMax As Integer, iPos As Integer
		Dim oDictItem As clsUVDictItem, oCmd As SqlCommand
		Dim sKeyField As String = ""
		Try

			sQuery = "IF OBJECT_ID('dbo.[" & sSubTableName & "]', 'U') IS NOT NULL " & vbCrLf
			sQuery &= "BEGIN " & vbCrLf
			sQuery &= "DROP TABLE dbo.[" & sSubTableName & "]; " & vbCrLf
			sQuery &= "END" & vbCrLf
			sQuery &= "BEGIN" & vbCrLf
			sQuery &= "CREATE TABLE [dbo].[" & sSubTableName & "](" & vbCrLf

			sQueryFields &= "UID [int] IDENTITY(10000000,1) PRIMARY KEY NOT NULL,"

			If oParentObj.moKeyField IsNot Nothing Then
				'sKeyField = oParentObj.moKeyField.ColumnHeadingClean
				sKeyField = oParentKeyFieldName.ColumnHeadingClean
				'sQueryFields &= oParentObj.moKeyField.AsSQLIndexColumnDefn()
				sQueryFields &= oParentKeyFieldName.AsSQLIndexColumnDefn

				sQueryIndex &= "CREATE NONCLUSTERED INDEX [" &
				 "IX_" & sSubTableName & "_" & sKeyField & "] " &
				 "ON dbo.[" & sSubTableName & "] " &
				 "(" & sKeyField & " ASC) " &
				 "WITH (" &
				 "PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, " &
				 "DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON" &
				 ");"

				'Else
				'	sKeyField = "ParentUID"
			End If

			sQueryFields &= ", SubValue integer"

			iMax = aSourceDictItems.Keys.Count - 1
			For iPos = 0 To iMax

				oDictItem = aSourceDictItems(aSourceDictItems.Keys(iPos))
				sQueryFields &= IIf(sQueryFields <> "", ", ", "").ToString & oDictItem.AsSQLIndexColumnDefn()

				If oDictItem.Indexed And oDictItem.ColumnHeadingClean <> sKeyField Then
					sQueryIndex &= "CREATE NONCLUSTERED INDEX [" &
				 "IX_" & sSubTableName & "_" & oDictItem.ColumnHeadingClean & "] " &
				 "ON dbo.[" & sSubTableName & "] " &
				 "(" & oDictItem.ColumnHeadingClean & " ASC) " &
				 "WITH (" &
				 "PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, " &
				 "DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON" &
				 ");"

				End If
			Next

			sQuery &= sQueryFields

			sQuery &= ");" & vbCrLf
			sQuery &= sQueryIndex & vbCrLf
			sQuery &= "END" & vbCrLf


			oCmd = New SqlCommand(sQuery, oParentObj.moDBSQLConnection)
			oCmd.ExecuteNonQuery()


		Catch ex As Exception

			ReportErrorGlobal(New ErrorItem(ex))

		End Try
	End Sub

	Protected Friend Sub PopulateFromValues(asValSet As SortedList(Of Integer, String))
		Dim sQuery As String = "", sQueryStart As String = "", sQueryValList As String = ""
		Dim iMaxDicts As Integer, iDictPos As Integer
		Dim oDictItem As clsUVDictItem, sSplitChar As String = "|"
		Dim iMaxRecs As Integer, iRecNo As Integer, sVal As String
		Dim aVals As ArrayList, aValRecs As New SortedList(Of Integer, ArrayList), iKey As Integer
		Dim oCmd As SqlCommand, oKeyAttr As clsUVDictItem, sKeyArray As String
		Dim sFieldArray As String

		Try
			oKeyAttr = aSourceDictItems.Values(0)
			If oKeyAttr IsNot Nothing Then
				sKeyArray = asValSet(oKeyAttr.iAttrNo)
				If sKeyArray <> "" Then

					aVals = New ArrayList(Split(sKeyArray, sSplitChar))
					iMaxRecs = aVals.Count - 1

					sQueryStart = "INSERT INTO " & sSubTableName &
				 "  ("
					If oParentKeyFieldName IsNot Nothing Then sQueryStart &= oParentKeyFieldName.ColumnHeadingClean & "," Else sQueryStart &= "ParentUID,"

					sQueryStart &= "SubValue,"

					iMaxDicts = aSourceDictItems.Count - 1
					For iDictPos = 0 To iMaxDicts

						'iKey = aSourceDictItems.IndexOfKey(iDictPos)
						oDictItem = aSourceDictItems.Values(iDictPos)
						sQueryValList &= oDictItem.ColumnHeadingClean & IIf(iDictPos < iMaxDicts, ",", "").ToString
						sFieldArray = asValSet(oDictItem.iAttrNo)
						aValRecs.Add(iDictPos, New ArrayList(Split(sFieldArray, sSplitChar)))
					Next

					sQueryStart &= sQueryValList & ") VALUES ("

					For iRecNo = 0 To iMaxRecs

						sQuery = sQueryStart & "'" & oParentObj.msKeyValue.Replace("'", "''") & "'"
						sQuery &= "," & (iRecNo + 1).ToString

						For iDictPos = 0 To iMaxDicts
							'iKey = aSourceDictItems.IndexOfKey(iDictPos)
							oDictItem = aSourceDictItems.Values(iDictPos)
							aVals = aValRecs(iDictPos)
							sVal = ""
							If aVals.Count > iRecNo Then sVal = aVals(iRecNo)
							sQuery = sQuery & ", '" & sVal.Replace("'", "''") & "'"
						Next
						'oDictItem = aSourceDictItems(0)
						'sQuery &= oDictItem.AsSQLColumnDefn(False)

						sQuery &= ");"

						oCmd = New SqlCommand(sQuery, oParentObj.moDBSQLConnection)
						oCmd.ExecuteNonQuery()

					Next
				End If
			End If

		Catch ex As Exception

			ReportErrorGlobal(New ErrorItem(ex))

		End Try
	End Sub


	Protected Friend Sub GenStructureFromString(sDefinitionString As String)
		Dim sColNumbers As String, oDictItem As clsUVDictItem = Nothing
		Dim aDicts As ArrayList, iCharPos As Integer
		Dim iPosOfList As Integer, iMaxOfList As Integer, iKey As Integer
		Dim iMaxDict As Integer, bIndexDict As Boolean
		Dim sKey As String

		Try

			iCharPos = sDefinitionString.IndexOf("(")
			If iCharPos > 0 Then
				sSubTableName = Trim(sDefinitionString.Substring(0, iCharPos).Replace(vbTab, "").Replace("'", "").Replace("%", "").Replace("?", "").Replace("[", "").Replace("]", ""))
				sColNumbers = sDefinitionString.Substring(iCharPos + 1).Replace("(", "").Replace(")", "").Replace(" ", "")

				aDicts = New ArrayList(Split(sColNumbers, ","))
				iMaxOfList = aDicts.Count - 1

				iMaxDict = oParentObj.mcDictItems.Count - 1
				For Each oDictItem In oParentObj.mcDictItems.Values
					If oDictItem.iAttrNo = 0 Then Exit For
				Next
				If oDictItem IsNot Nothing Then oParentKeyFieldName = oDictItem

				For iPosOfList = 0 To iMaxOfList
					bIndexDict = False
					sKey = aDicts(iPosOfList)
					If sKey.Substring(0, 1) = "+" Then
						bIndexDict = True
						sKey = sKey.Substring(1)
					End If
					iKey = CInt(Math.Floor(Val(sKey)))

					For Each oDictItem In oParentObj.mcDictItems.Values
						If oDictItem.iAttrNo = iKey Then Exit For
					Next

					oDictItem.Indexed = bIndexDict Or oDictItem.Indexed
					aSourceDictItems.Add(iKey, oDictItem)

				Next

			End If

		Catch ex As Exception

			ReportErrorGlobal(New ErrorItem(ex))

		End Try
	End Sub

End Class
