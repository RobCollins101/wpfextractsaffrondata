﻿Imports System.Collections.Generic
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports System.Text.RegularExpressions
Imports IBMU2.UODOTNET

Public Class clsUVData


	Protected Friend mcDictItems As New SortedList(Of String, clsUVDictItem)

	Protected Friend sUVFileName As String = ""
	Protected Friend sSelectQueryCondition As String = ""
	Protected Friend sDictPrefix As String = ""
	Protected Friend mcDictAttr As New List(Of String)

	Protected Friend moUVDictAsFile As UniDictionary
	Protected Friend moUVDictItem As UniDynArray
	Protected Friend moUVSelItem As UniDynArray
	Protected Friend moUVFile As UniFile
	Protected Friend moUVRecord As UniDynArray
	Protected Friend moUVCommand As UniCommand
	Protected Friend msUVSelListName As String = ""

	Protected Friend msFieldDelimiterUsed As String = "|"

	Protected Friend iUVRecordsSelected As Integer = 0
	Protected Friend iUVRecordPosition As Integer = 0
	Protected Friend bMissingIDsOnly As Boolean = False

	Public Event FileProcessingStarted(CallingObject As clsUVData)
	Public Event PassMessage(CallingObject As clsUVData, sMessage As String)
	Public Event NoRecordsProcessed(CallingObject As clsUVData, sReason As String)
	Public Event RecordsSelected(CallingObject As clsUVData, RecordCount As Integer)
	Public Event RecordProcessed(CallingObject As clsUVData, RecordAt As Integer)
	Public Event AllRecordsProcessed(CallingObject As clsUVData)

	Public Event evReportError(CallingObject As clsUVData, oErr As ErrorItem, sReason As String)

	Protected Friend sUVResponse As String

	Protected Friend bAbort As Boolean = False
	Protected Friend sValDelim As String = "|"
	Protected Friend dStartOfRecordProcessing As Date = Now

	Public sDBConnection As String = ""
	Protected Friend moDBSQLConnection As SqlConnection

	Protected Friend maSubTableGenerators As New List(Of clsDataTableSubRecs)
	Protected Friend maSubTables As New ArrayList

	Protected Friend moKeyField As clsUVDictItem
	Protected Friend msKeyValue As String
	'Protected Friend msSQLKeyFieldName As String

	'Protected Friend aSQLKeys As New List(Of String)(StringComparer.CurrentCultureIgnoreCase)
	Protected Friend aSQLPrimaryKeys As New List(Of String)

	Public Sub New()
		Dim oTemp As Object = Nothing

		'oTemp = GetMySetting("DestinationSQLDatabase")
		oTemp = gsSQLConnectString
		If sDBConnection = "" And oTemp IsNot Nothing Then sDBConnection = oTemp.ToString

	End Sub

	Protected Friend Function LoadDictionary() As Boolean
		Dim sCommand As String, selListDict As UniSelectList = Nothing
		Dim oDictRec As clsUVDictItem, iPos As Integer
		Dim bReturn As Boolean = False, oOthDictRec As clsUVDictItem, BAttrFound As Boolean
		Dim bIgnore As Boolean = False, sTemp As String
		Dim sAttrNo As String, iAttrNo As Integer, sDictID As String, sDictHeader As String
		Try
			RaiseEvent PassMessage(Me, "Loading dictionary items for " & sUVFileName)
			mcDictItems.Clear()
			If sUVFileName <> "" And OpenUV(gUniverseDBSystem, gUniverseDBAccount, gUniverseDBUserID, gUniverseDBPassword) Then
				moUVCommand = gUVCon.CreateUniCommand
				sCommand = "SSELECT DICT " & sUVFileName & " IF F1 = ""A"" ""S"" ""D"" "
				moUVCommand.Command = sCommand
				moUVCommand.Execute()


				moUVDictAsFile = gUVCon.CreateUniDictionary(sUVFileName)
				moUVDictAsFile.Open()

				If moUVDictAsFile.IsFileOpen Then

					If msUVSelListName = "" Then msUVSelListName = "SELECT.OF." & sUVFileName

					If moUVCommand.CommandAtSelected <> 0 Then
						selListDict = gUVCon.CreateUniSelectList(0)
						moUVDictAsFile.RecordID = selListDict.Next
						Do Until selListDict.LastRecordRead
							Try
								oDictRec = Nothing
								moUVDictAsFile.Read()
								moUVDictItem = moUVDictAsFile.Record

								bIgnore = False

								If (IsNumeric(moUVDictAsFile.RecordID) And moUVDictAsFile.RecordID <> "" And moUVDictItem.Extract(2).StringValue <> moUVDictAsFile.RecordID) Then
									bIgnore = True

								Else
									sAttrNo = moUVDictItem.Extract(2).StringValue
									If sAttrNo <> "" Then iAttrNo = CInt(sAttrNo) Else iAttrNo = 0
									sDictID = moUVDictAsFile.RecordID
									sDictHeader = moUVDictItem.Extract(3).StringValue

									'If Regex.IsMatch(sDictID, "^[A-Za0z]\d+$") And sDictID.Substring(0, 1) & CInt(sAttrNo).ToString <> sDictID Then sDictID = sDictID.Substring(0, 1) & CInt(sAttrNo).ToString

									If Regex.IsMatch(sDictID, "^[A-Za-z]\d+$") AndAlso CInt(sDictID.Substring(1)) <> iAttrNo Then
										bIgnore = True
									Else

										oDictRec = New clsUVDictItem
										oDictRec.UVDictName = sDictID
										oDictRec.DictType = moUVDictItem.Extract(1).StringValue
										oDictRec.AttrNo = sAttrNo
										oDictRec.ColumnHeading = sDictHeader
										oDictRec.IConv = moUVDictItem.Extract(7).StringValue
										oDictRec.OConv = moUVDictItem.Extract(8).StringValue
										If Text.RegularExpressions.Regex.IsMatch(sDictID, "^A\d+$") Then
											'oDictRec.UVDictNameOnExport = Replace(StrConv(Replace(oDictRec.ColumnHeading, ".", " "), VbStrConv.ProperCase), " ", 
											oDictRec.UVDictNameOnExport = sDictHeader
										ElseIf IsNumeric(sAttrNo) And sDictHeader = "" Then
											'oDictRec.UVDictNameOnExport = Replace(StrConv(Replace(oDictRec.UVDictName, ".", " "), VbStrConv.ProperCase), " ", "")
											oDictRec.UVDictNameOnExport = sDictID
										Else
											bIgnore = True
											'    oDictRec.UVDictNameOnExport = Replace(StrConv(Replace(oDictRec.UVDictName, ".", " "), VbStrConv.ProperCase), " ", "")
										End If

										If moKeyField Is Nothing And oDictRec.iAttrNo = 0 And oDictRec.IConv = "" And oDictRec.OConv = "" Then moKeyField = oDictRec

									End If
								End If


								If Not bIgnore And oDictRec IsNot Nothing Then
									oDictRec.UVDictNameOnExport &= "_" & iAttrNo.ToString("000")

									sTemp = moUVDictItem.Extract(9).StringValue : If IsNumeric(sTemp) Then oDictRec.Length = CInt(sTemp)
									oDictRec.KeyField = (oDictRec.iAttrNo = 0 And oDictRec.UVDictName = "A0")

									If Not mcDictItems.ContainsKey(oDictRec.UVDictName) Then mcDictItems.Add(oDictRec.UVDictName, oDictRec)
								End If

							Catch ex As Exception
								ReportError(ex)
							End Try
							moUVDictAsFile.RecordID = selListDict.Next
							If bAbort Then Exit Do
						Loop

					End If
					If selListDict IsNot Nothing Then selListDict.Dispose()
					selListDict = Nothing

				End If
				'End If

				moUVCommand.Dispose()
				moUVCommand = Nothing

				If Not bAbort Then
					For iPos = 0 To 1000
						BAttrFound = False
						oDictRec = Nothing
						If mcDictItems.ContainsKey(iPos.ToString) Then oDictRec = mcDictItems.Item(iPos.ToString)
						If oDictRec Is Nothing Then

							oDictRec = New clsUVDictItem
							oDictRec.UVDictName = iPos.ToString
							oDictRec.DictType = "A"
							oDictRec.iAttrNo = iPos
							oDictRec.UVDictNameOnExport = "Attr" & iPos.ToString("000")

							For Each oOthDictRec In mcDictItems.Values
								If oOthDictRec.iAttrNo = iPos And Not Regex.IsMatch(oOthDictRec.UVDictName, "^A(ttr)?\d+$") Then
									If (oOthDictRec.OConv = "" OrElse (Left(oOthDictRec.OConv, 1) <> "A" And Left(oOthDictRec.OConv, 1) <> "F" And Left(oOthDictRec.OConv, 1) <> "T" And Left(oOthDictRec.OConv, 1) <> "G") And oOthDictRec.OConv.LastIndexOf(";") < 1) Then
										oDictRec.UVDictNameOnExport = oOthDictRec.UVDictNameOnExport & IIf(Right(oOthDictRec.UVDictNameOnExport, 3) = oOthDictRec.iAttrNo.ToString("000"), "", "_" & oOthDictRec.iAttrNo.ToString("000")).ToString
										oDictRec.ColumnHeading = oOthDictRec.ColumnHeading
										oDictRec.OConv = oOthDictRec.OConv
										oDictRec.IConv = oOthDictRec.IConv
										BAttrFound = True
										Exit For
									End If
								End If
							Next


							If Not BAttrFound Then
								If mcDictItems.ContainsKey("A" & iPos) Then
									oOthDictRec = mcDictItems.Item("A" & iPos)
									oDictRec.UVDictNameOnExport = oOthDictRec.UVDictNameOnExport & IIf(Right(oOthDictRec.UVDictNameOnExport, 3) = oOthDictRec.iAttrNo.ToString("000"), "", "_" & oOthDictRec.iAttrNo.ToString("000")).ToString
									oDictRec.ColumnHeading = oOthDictRec.ColumnHeading
									oDictRec.OConv = oOthDictRec.OConv
									oDictRec.IConv = oOthDictRec.IConv
								Else
									For Each oOthDictRec In mcDictItems.Values
										If oOthDictRec.iAttrNo = iPos And (oOthDictRec.OConv = "" OrElse (Left(oOthDictRec.OConv, 1) <> "A" And Left(oOthDictRec.OConv, 1) <> "F" And Left(oOthDictRec.OConv, 1) <> "T" And Left(oOthDictRec.OConv, 1) <> "G" And oOthDictRec.OConv.LastIndexOf(";") < 1)) Then
											oDictRec.UVDictNameOnExport = oOthDictRec.UVDictNameOnExport & IIf(Right(oOthDictRec.UVDictNameOnExport, 3) = oOthDictRec.iAttrNo.ToString("000"), "", "_" & oOthDictRec.iAttrNo.ToString("000")).ToString
											oDictRec.ColumnHeading = oOthDictRec.ColumnHeading
											oDictRec.OConv = oOthDictRec.OConv
											oDictRec.IConv = oOthDictRec.IConv
											Exit For
										End If
									Next
								End If
							End If

							If mcDictItems.ContainsKey(oDictRec.UVDictName) Then
								mcDictItems.Item(oDictRec.UVDictName).UVDictNameOnExport = oDictRec.UVDictNameOnExport
							Else
								oDictRec.KeyField = (iPos = 0)
								If Not mcDictItems.ContainsKey(oDictRec.UVDictName) Then mcDictItems.Add(oDictRec.UVDictName, oDictRec)

							End If
							If Not mcDictItems.ContainsKey(oDictRec.AttrNo) Then mcDictItems.Add(oDictRec.AttrNo, oDictRec)


						End If

						If moKeyField Is Nothing And iPos = 0 Then moKeyField = oDictRec
						If bAbort Then Exit For
					Next

					bReturn = True
					RaiseEvent PassMessage(Me, "All dictionary items loaded")
				Else
					RaiseEvent PassMessage(Me, "dictionary items loading aborted")

				End If
			End If
		Catch ex As Exception
			ReportError(ex)
		End Try
		Return bReturn
	End Function

	Public Function LoadData() As Boolean
		Dim sCommand As String, selListRecords As UniSelectList
		Dim oFileRec As Hashtable, iPos As Integer
		Dim sAttrKey As String, sValue As String, sRawValue As String, oDictItem As clsUVDictItem
		Dim bDestinationPrepared As Boolean = False ', oWriterStream As IO.TextWriter = Nothing
		Dim sDataLine As String, aTemp As String(), aTemp2 As String(), iRecCountSave As Integer = 100
		Dim bReturn As Boolean = False, sTempKey As String, sTempVal As String
		Dim sInsertTemplate As String = "", oSQLCmd As SqlCommand, dUVBaseDate As Date
		Dim iAttemptCount As Integer = 1, iNoOfAttr As Integer, aRecordVals As SortedList(Of Integer, String)
		Dim bInsertLine As Boolean = False

		Try
			dUVBaseDate = Date.Parse("31 Dec 1967")
			If Not bAbort Then

				Do While iAttemptCount > 0 And iAttemptCount < 3 And Not bAbort And giErrorCountdown > 0
					Try
						If sUVFileName <> "" And OpenUV(gUniverseDBSystem, gUniverseDBAccount, gUniverseDBUserID, gUniverseDBPassword) Then
							'mcDataRecords.Clear()


							If bAbort Then
								RaiseEvent PassMessage(Me, "File items loading aborted")

							Else
								If gbSQLStructureOnly Then
									CreateSubTables(maSubTables)
									CreateSQLTable(sUVFileName)

								Else
									RaiseEvent FileProcessingStarted(Me)
									moUVCommand = gUVCon.CreateUniCommand
									sCommand = "SSELECT " & sUVFileName & " " & sSelectQueryCondition
									moUVCommand.Command = sCommand
									moUVCommand.Execute()

									iUVRecordsSelected = moUVCommand.CommandAtSelected


									moUVFile = gUVCon.CreateUniFile(sUVFileName)
									moUVFile.Open()

									sDataLine = ""


									If moUVFile.IsFileOpen Then

										If msUVSelListName = "" Then msUVSelListName = "SELECT.OF." & sUVFileName

										If moUVCommand.CommandAtSelected > 0 Then
											RaiseEvent RecordsSelected(Me, iUVRecordsSelected)

											selListRecords = gUVCon.CreateUniSelectList(0)


#Region "ProcessRecords"
											If moDBSQLConnection Is Nothing Then moDBSQLConnection = New SqlConnection(sDBConnection)
											If moDBSQLConnection.State <> System.Data.ConnectionState.Open Then moDBSQLConnection.Open()

											moUVFile.RecordID = selListRecords.Next
											dStartOfRecordProcessing = Now
											Do Until selListRecords.LastRecordRead Or giErrorCountdown < 1
												Try
													oFileRec = New Hashtable

													If Not bAbort Then

														iUVRecordPosition += 1

														If moUVFile.RecordID <> "" Then

															msKeyValue = moUVFile.RecordID
															bInsertLine = True

															bInsertLine = (aSQLPrimaryKeys.BinarySearch(msKeyValue) < 0)

															If bInsertLine Then

																moUVFile.Read()
																moUVRecord = moUVFile.Record
																iNoOfAttr = moUVRecord.Count()

																oFileRec("0") = moUVFile.RecordID
																For Each sAttrKey In mcDictAttr
																	sValue = ""
																	If mcDictItems.ContainsKey(sAttrKey) Then

																		oDictItem = mcDictItems.Item(sAttrKey)
																		If oDictItem.iAttrNo <= iNoOfAttr Then
																			If oDictItem.iAttrNo = 0 Then sRawValue = moUVFile.RecordID Else sRawValue = moUVRecord.Extract(oDictItem.iAttrNo).StringValue
																			If oDictItem.OConv <> "" Then sValue = gUVCon.Oconv(sRawValue, oDictItem.OConv) Else sValue = sRawValue
																		End If
																	Else
																		If IsNumeric(sAttrKey) AndAlso CInt(sAttrKey) <= iNoOfAttr Then
																			If sAttrKey <= 0 Then
																				sRawValue = moUVFile.RecordID
																			Else
																				sRawValue = moUVRecord.Extract(CInt(Val(sAttrKey))).StringValue
																			End If
																			sValue = sRawValue
																		End If
																	End If
																	oFileRec(sAttrKey) = sValue

																	If bAbort Then Exit For

																Next
															End If
														End If
													End If


													If bAbort Then
														RaiseEvent PassMessage(Me, "File items loading aborted")
														Exit Do

													Else

														If moUVFile.RecordID <> "" And bInsertLine Then


															If Not bDestinationPrepared Or (moDBSQLConnection Is Nothing OrElse moDBSQLConnection.State <> ConnectionState.Open) Then
																bDestinationPrepared = True
																CreateSubTables(maSubTables)
																sInsertTemplate = CreateSQLTable(sUVFileName)

															End If


															If sInsertTemplate <> "" Then
																aRecordVals = New SortedList(Of Integer, String)

																oSQLCmd = New SqlCommand()
																oSQLCmd.Connection = moDBSQLConnection

																oSQLCmd.CommandText = sInsertTemplate
																oSQLCmd.CommandType = System.Data.CommandType.Text
																ReDim aTemp(mcDictAttr.Count - 1)
																For iPos = 0 To mcDictAttr.Count - 1
																	sAttrKey = mcDictAttr(iPos)
																	If mcDictItems.ContainsKey(sAttrKey) Then
																		oDictItem = mcDictItems(sAttrKey)
																		sValue = ""
																		If oFileRec.ContainsKey(sAttrKey) Then sValue = oFileRec(sAttrKey)
																		aTemp(iPos) = sValue

																		If oDictItem.OConv <> "" Then
																			aTemp2 = Split(Replace(sValue, UV_VM, Oth_VM), Oth_VM)
																			For iPosTemp = 0 To aTemp2.Count - 1
																				sTempVal = aTemp2(iPosTemp)
																				If sTempVal <> "" Then
																					If (oDictItem.OConv.Substring(0, 1) <> "M" Or sTempVal <> "0") Then
																						Try
																							Select Case oDictItem.OConv
																								Case "MD2" : aTemp2(iPosTemp) = (Val(sTempVal) / 100).ToString("F2")
																								Case "D2", "D4" : aTemp2(iPosTemp) = DateAdd(DateInterval.Day, CInt(sTempVal), dUVBaseDate).ToString("dd MMM yyyy")
																								Case Else
																									aTemp2(iPosTemp) = gUVCon.Oconv(sTempVal, oDictItem.OConv)
																							End Select
																						Catch ex As Exception
																						End Try

																					End If

																				End If
																				If bAbort Then Exit For
																			Next
																			sValue = Join(aTemp2, Oth_VM)
																		End If

																		If oDictItem.IConv <> "" Then
																			aTemp2 = Split(Replace(sValue, UV_VM, Oth_VM), Oth_VM)
																			For iPosTemp = 0 To aTemp2.Count - 1
																				sTempVal = aTemp2(iPosTemp)
																				If sTempVal <> "" Then
																					If (oDictItem.IConv.Substring(0, 1) <> "M" Or sTempVal <> "0") Then
																						Try
																							Select Case oDictItem.IConv
																								Case "MD2" : aTemp2(iPosTemp) = (Val(sTempVal) / 100).ToString("F2")
																								Case "D2", "D4" : aTemp2(iPosTemp) = DateAdd(DateInterval.Day, CInt(sTempVal), dUVBaseDate).ToString("dd MMM yyyy")
																								Case Else
																									aTemp2(iPosTemp) = gUVCon.Oconv(sTempVal, oDictItem.IConv)
																							End Select
																						Catch ex As Exception
																						End Try
																					End If
																				End If
																				If bAbort Then Exit For
																			Next
																			sValue = Join(aTemp2, Oth_VM)
																		End If

																		If oDictItem.KeyField And oDictItem.ColumnHeadingClean = moKeyField.ColumnHeadingClean Then
																			If bMissingIDsOnly And aSQLPrimaryKeys.Count > 0 Then
																				bInsertLine = (aSQLPrimaryKeys.BinarySearch(sValue) < 0)
																			End If
																		End If

																		If Not bInsertLine Then Exit For

																		sTempKey = "@" & oDictItem.ColumnHeadingClean

																		oSQLCmd.Parameters.Add(New SqlParameter(sTempKey, SqlDbType.NVarChar, 8000))
																		sValue = sValue.Replace(Oth_VM, sValDelim).Replace(Oth_SM, sValDelim).Replace(UV_VM, sValDelim)

																		If aRecordVals.ContainsKey(oDictItem.iAttrNo) Then aRecordVals(oDictItem.iAttrNo) = sValue Else aRecordVals.Add(oDictItem.iAttrNo, sValue)

																		oSQLCmd.Parameters(sTempKey).Value = sValue
																		If sValue <> "" Then oDictItem.HasData = True
																		If sValue.Length > oDictItem.Length Then oDictItem.Length = sValue.Length
																	End If
																	If bAbort Then Exit For
																Next

																If bInsertLine Then

																	oSQLCmd.ExecuteNonQuery()

																	For Each oSubTable In maSubTableGenerators
																		oSubTable.PopulateFromValues(aRecordVals)
																	Next

																End If
															End If

														End If
														RaiseEvent RecordProcessed(Me, iUVRecordPosition)

														moUVFile.RecordID = selListRecords.Next
													End If
												Catch ex As Exception
													If Not selListRecords.LastRecordRead Then
														RaiseEvent PassMessage(Me, "Error on record " & moUVFile.RecordID & vbCrLf & ex.Message)
														Try : moUVFile.RecordID = selListRecords.Next : Catch ex2 As Exception : End Try
														giErrorCountdown -= 1
													End If

												End Try
											Loop

											selListRecords.Dispose()
											selListRecords = Nothing

											oSQLCmd = Nothing
											moDBSQLConnection.Close()
											moDBSQLConnection = Nothing
#End Region

											If Not bAbort Then
												bReturn = True

												RaiseEvent AllRecordsProcessed(Me)
											End If
										Else
											RaiseEvent NoRecordsProcessed(Me, "No Records selected")
										End If
									Else
										RaiseEvent NoRecordsProcessed(Me, "File " & sUVFileName & " could not be opened")

									End If
									RemoveBlankColumns()

								End If
								Try : moUVCommand.Dispose() : Catch ex As Exception : End Try
								moUVCommand = Nothing

								Try : moUVFile.Close() : Catch ex As Exception : End Try
								Try : moUVFile.Dispose() : Catch ex As Exception : End Try
								moUVFile = Nothing

							End If

						Else
							If sUVFileName = "" Then
								RaiseEvent NoRecordsProcessed(Me, "No filename provided")
							Else

								RaiseEvent NoRecordsProcessed(Me, "Universe was not opened")
							End If

						End If
						iAttemptCount = 0

						RaiseEvent PassMessage(Me, "All processing completed for file " & sUVFileName & vbCrLf)

					Catch ex As Exception
						If iAttemptCount < 2 Then
							iAttemptCount += 1
							RaiseEvent PassMessage(Me, "Second attempt for file """ & sUVFileName & """")
							Threading.Thread.Sleep(20000)
						Else
							RaiseEvent NoRecordsProcessed(Me, "ERROR : " & ex.ToString)
							ReportError(ex)

						End If

					End Try
				Loop
			End If
		Catch ex As Exception
			RaiseEvent NoRecordsProcessed(Me, "ERROR : " & ex.ToString)
			ReportError(ex)

		End Try
		Return bReturn
	End Function


	Public Function CreateSQLTable(sUVFileName As String) As String
		Dim oTemp As Object, oDict As clsUVDictItem
		Dim sQueryFields As String = "", sQuery As String = "", sQueryIndex As String = ""
		Dim oCmd As SqlCommand, sAttrKey As String, sKeyField As String
		Dim sReturn As String = "", iTableCount As Integer, iRecordCount As Integer
		Dim aProhibitedWords As New ArrayList From {{"Null"}, {"Select"}, {"Contains"}}
		Dim aAttrsProcessed As New ArrayList
		Dim oColNamesRecs As SqlDataReader, aTemp As New ArrayList, sName As String
		Dim aRemoveList As New List(Of Integer), iRemoveAttr As Integer, sTemp As String

		Try
			'oTemp = GetMySetting("DestinationSQLDatabase")
			oTemp = gsSQLConnectString
			If sDBConnection = "" And oTemp IsNot Nothing Then sDBConnection = oTemp.ToString

			If moDBSQLConnection Is Nothing Then moDBSQLConnection = New SqlConnection(sDBConnection)
			If moDBSQLConnection.State <> System.Data.ConnectionState.Open Then moDBSQLConnection.Open()

			If moDBSQLConnection.State = System.Data.ConnectionState.Open Then

				RaiseEvent PassMessage(Me, "Creating SQL table [" & sUVFileName & "]")

				If bMissingIDsOnly Then
					' Verify table exists
					bMissingIDsOnly = False
					sQuery = "SELECT COUNT(*) FROM sys.tables Tbls WHERE tbls.name = '" & sUVFileName & "' "
					oCmd = New SqlCommand(sQuery, moDBSQLConnection)
					iTableCount = oCmd.ExecuteScalar()

					If iTableCount > 0 Then
						sQuery = "SELECT COUNT(*) FROM [" & sUVFileName & "] "
						oCmd = New SqlCommand(sQuery, moDBSQLConnection)
						iRecordCount = oCmd.ExecuteScalar()
						If iRecordCount > 0 Then bMissingIDsOnly = True

					End If

				End If

				If bMissingIDsOnly Then
					' Identify existing columns if any
					sQuery = "SELECT Cols.name ColName " & vbCrLf
					sQuery &= "FROM sys.columns Cols  " & vbCrLf
					sQuery &= "INNER JOIN sys.tables Tbls ON Cols.object_id = Tbls.object_id " & vbCrLf
					sQuery &= "WHERE tbls.name = '" & sUVFileName & "' "
					oCmd = New SqlCommand(sQuery, moDBSQLConnection)
					oColNamesRecs = oCmd.ExecuteReader
					Do While oColNamesRecs.Read
						sName = oColNamesRecs.Item(0)
						aTemp.Add(sName)
					Loop
					oColNamesRecs.Close()

					For iPos = 0 To mcDictAttr.Count - 1

						oDict = Nothing
						sAttrKey = mcDictAttr(iPos)

						If mcDictItems.ContainsKey(sAttrKey) Then
							oDict = mcDictItems(sAttrKey)
						Else
							If Regex.IsMatch(sAttrKey, "^[a-zA-Z]\d+") Then
								sAttrKey = sAttrKey.Substring(1)
								If Not mcDictAttr.Contains(sAttrKey) Then
									mcDictAttr(iPos) = sAttrKey

								End If
								If mcDictItems.ContainsKey(sAttrKey) Then oDict = mcDictItems(sAttrKey)
							End If
						End If

						If oDict IsNot Nothing Then

							sName = oDict.ColumnHeadingClean
							If Not aTemp.Contains(sName) Then
								aRemoveList.Add(iPos)
							Else
								If Not aAttrsProcessed.Contains(oDict.ColumnHeadingClean) Then
									If Not aProhibitedWords.Contains(oDict.ColumnHeadingClean) Then

										sQueryFields &= IIf(sQueryFields <> "", ", ", "") & oDict.AsSQLColumnDefn(True)

										sReturn &= IIf(sReturn <> "", ", ", "") & " @" & oDict.ColumnHeadingClean
										aAttrsProcessed.Add(oDict.ColumnHeadingClean)
									End If
								End If
							End If
						End If

						If bAbort Then Exit For
					Next

					aRemoveList.Sort()
					For ipos = aRemoveList.Count - 1 To 0 Step -1
						iRemoveAttr = aRemoveList(ipos)
						mcDictAttr.Remove(iRemoveAttr)
						If bAbort Then Exit For
					Next

					aSQLPrimaryKeys.Clear()

					sQuery = "SELECT [" & moKeyField.ColumnHeadingClean & "] KeyCol " & vbCrLf
					sQuery &= "FROM dbo.[" & sUVFileName & "] "
					oCmd = New SqlCommand(sQuery, moDBSQLConnection)

					RaiseEvent PassMessage(Me, "Selecting existing keys from " & moKeyField.ColumnHeadingClean)

					oColNamesRecs = oCmd.ExecuteReader
					Do While oColNamesRecs.Read
						sTemp = oColNamesRecs.Item(0)
						aSQLPrimaryKeys.Add(sTemp)
					Loop
					oColNamesRecs.Close()

					RaiseEvent PassMessage(Me, "Preloaded " & aSQLPrimaryKeys.Count & " existing keys from SQL database")

				Else

					' Verify column or remove as necessary

					sQuery = "If OBJECT_ID('dbo.[" & sUVFileName & "]', 'U') IS NOT NULL " & vbCrLf
					sQuery &= "BEGIN " & vbCrLf
					sQuery &= "DROP TABLE dbo.[" & sUVFileName & "]; " & vbCrLf
					sQuery &= "END" & vbCrLf
					sQuery &= "BEGIN" & vbCrLf

					sQuery &= "CREATE TABLE [dbo].[" & sUVFileName & "](" & vbCrLf

					For iPos = 0 To mcDictAttr.Count - 1

						oDict = Nothing
						sAttrKey = mcDictAttr(iPos)

						If mcDictItems.ContainsKey(sAttrKey) Then
							oDict = mcDictItems(sAttrKey)
						Else
							If Regex.IsMatch(sAttrKey, "^[a-zA-Z]\d+") Then
								sAttrKey = sAttrKey.Substring(1)
								If Not mcDictAttr.Contains(sAttrKey) Then
									mcDictAttr(iPos) = sAttrKey

								End If
								If mcDictItems.ContainsKey(sAttrKey) Then oDict = mcDictItems(sAttrKey)
							End If
						End If

						If oDict IsNot Nothing AndAlso Not aAttrsProcessed.Contains(oDict.ColumnHeadingClean) Then
							If Not aProhibitedWords.Contains(oDict.ColumnHeadingClean) Then

								sQueryFields &= IIf(sQueryFields <> "", ", ", "") & oDict.AsSQLColumnDefn(True)

								sReturn &= IIf(sReturn <> "", ", ", "") & " @" & oDict.ColumnHeadingClean
								aAttrsProcessed.Add(oDict.ColumnHeadingClean)
							End If
						Else

						End If

						If bAbort Then Exit For

					Next

					sQuery &= sQueryFields
					sQuery &= ");" & vbCrLf

					sQuery &= "END" & vbCrLf

					oCmd = New SqlCommand(sQuery, moDBSQLConnection)
					oCmd.ExecuteNonQuery()


				End If

				sReturn = "insert into [dbo].[" & sUVFileName & "] values (" & sReturn & ");"

			End If


		Catch ex As Exception
			ReportError(ex)

		End Try

		Return sReturn
	End Function


	Public Sub CreateSubTables(aSubTables As ArrayList)
		Dim sTemp As String, oSubTable As clsDataTableSubRecs
		Try

			For Each sTemp In aSubTables
				If sTemp <> "" Then
					oSubTable = New clsDataTableSubRecs()
					oSubTable.oParentObj = Me
					maSubTableGenerators.Add(oSubTable)
					oSubTable.GenStructureFromString(sTemp)
					oSubTable.GenerateSubTable()

				End If
			Next

		Catch ex As Exception
			ReportError(ex)

		End Try
	End Sub


	Public Sub RemoveBlankColumns()

		Dim oTemp As Object, oDict As clsUVDictItem
		Dim sFieldText As String = "", sQuery As String = ""
		Dim oCmd As SqlCommand, sAttrKey As String
		Dim sReturn As String = "", iNoOfColsRemoved As Integer = 0

		Try
			If Not bAbort And Not bMissingIDsOnly Then
				'oTemp = GetMySetting("DestinationSQLDatabase")
				oTemp = gsSQLConnectString
				If sDBConnection = "" And oTemp IsNot Nothing Then sDBConnection = oTemp.ToString

				If moDBSQLConnection Is Nothing Then moDBSQLConnection = New SqlConnection(sDBConnection)
				If moDBSQLConnection.State <> System.Data.ConnectionState.Open Then moDBSQLConnection.Open()

				If moDBSQLConnection.State = System.Data.ConnectionState.Open Then

					RaiseEvent PassMessage(Me, "Removing blank columns from SQL table [" & sUVFileName & "]")

					For iPos = 0 To mcDictAttr.Count - 1

						sFieldText &= IIf(sFieldText <> "", "," & vbCrLf, "")
						sAttrKey = mcDictAttr(iPos)

						If mcDictItems.ContainsKey(sAttrKey) Then

							oDict = mcDictItems(sAttrKey)
							sFieldText &= oDict.ColumnHeadingClean

							If Not oDict.KeyField And Not oDict.HasData Then

								iNoOfColsRemoved += 1

								'RaiseEvent PassMessage(Me, "Removing unused column " & oDict.ColumnHeadingClean)

								sQuery = "ALTER TABLE [" & sUVFileName & "]"
								sQuery &= " DROP COLUMN [" & oDict.ColumnHeadingClean & "]"

								oCmd = New SqlCommand(sQuery, moDBSQLConnection)
								oCmd.ExecuteNonQuery()

							ElseIf Not oDict.KeyField And oDict.Length < 100 Then
								Try

									sQuery = "ALTER TABLE [" & sUVFileName & "]"
									sQuery &= " ALTER COLUMN [" & oDict.ColumnHeadingClean & "] NVARCHAR(" & ((CInt((oDict.Length - 1) \ 10) + 1) * 10).ToString & ")"

									oCmd = New SqlCommand(sQuery, moDBSQLConnection)
									oCmd.ExecuteNonQuery()

								Catch ex As Exception

								End Try

							End If
						End If
						If bAbort Then Exit For
					Next

					If iNoOfColsRemoved > 0 Then
						RaiseEvent PassMessage(Me, "Removing " & iNoOfColsRemoved.ToString & " unused columns")
					Else
						RaiseEvent PassMessage(Me, "No unused columns")
					End If

				End If
			End If

		Catch ex As Exception

			ReportError(ex)

		End Try
	End Sub

	Public Sub ReportError(exin As Exception)
		Dim oErr As ErrorItem
		Try
			oErr = glErrors.NewError(exin)
			RaiseEvent evReportError(Me, oErr, oErr.ToString)

		Catch ex As Exception

		End Try
	End Sub

End Class

