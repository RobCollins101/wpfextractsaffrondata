﻿Imports IBMU2.UODOTNET
Module basUV


    '******************************************************************************
    '
    '  OLE Automation Interface 1.0 - Error codes, status codes and keys.
    '
    '  Module   uvoaif.txt  Version  2.0   Date  04/08/96
    '
    '  (c) Copyright 1998 Ardent Software Inc. - All Rights Reserved
    '  This is unpublished proprietary source code of Ardent Software Inc.
    '  The copyright notice above does not evidence any actual or intended
    '  publication of such source code.
    '
    '******************************************************************************
    '
    '       Include file used with VB to access UniObjects OLE Automation Control.
    '
    '  Maintenence log - insert most recent change descriptions at top
    '
    '  Date.... GTAR# WHO Description.........................................
    '  07/24/00 24509 DJD Changed to use new reg keys.
    '  04/30/99 24251 DJD Added support for multiple databases
    '  11/04/98 23801 DJD Changed copyright.
    '  11/05/98 23617 OGO Changed uvrpc references to unirpc.
    '  03/31/98 22815 RJE Added Teminos security functionality
    '  09/11/97 21371 RJE Fixed NLS tokens (from 20403) to be equated properly
    '  07/22/97 20403 DW  Added NLS Locale category tokens.
    '  07/01/97 20403 DW  Match NLS tokens to revised UniVerse NLS tokens.
    '  08/13/96 16749 DW  Added UVS_RNF_RNF_DICT.
    '  07/17/96 18553 DJD Added 80036.
    '  07/31/96 18856 ALC Corrected comment symbol in comment below
    '  07/17/96 18856 ALC Modified comments for 80011 and 80019
    '  06/27/96 18213 DW  Added new error codes.
    '  06/18/96 18213 DW  Fixed NETWORK_XXX to match UvRPC
    '  04/08/96 18213 DW  added NETWORK_xxx Transport codes
    '  12/07/95 17741 NTM updated with extra 39127-39134 codes
    '  11/30/95 17719 NTM updated with UVRPC error codes
    '  06/19/95 16725 NTM added UV_SESSION_OBJECT and UV_DARRAY_OBJECT
    '  05/18/95   -   NTM added 40008 and 40009 error codes
    '  05/11/95   -   NTM saved in DOS format and changed UVE_FIFS from 30 to 30095
    '  04/24/95   -   NTM Adapted from INTCALL.TXT
    '
    '*****************************************************************************


    ' Mark characters

    Public Const I_IM As Integer = 255
    Public Const I_FM As Integer = 254
    Public Const I_VM As Integer = 253
    Public Const I_SM As Integer = 252
    Public Const I_TM As Integer = 251
    Public Const I_SQLNULL As Integer = 128

    ' Limits

    Public Const IC_MAX_SELECT_LIST As Integer = 10      ' Highest select list number

    ' FILEINFO keys (copied from fileinfo.it in PI)

    Public Const FINFO_IS_FILEVAR As Integer = 0     ' Anything
    Public Const FINFO_VOCNAME As Integer = 1    ' PI only
    Public Const FINFO_PATHNAME As Integer = 2     ' ALL
    Public Const FINFO_TYPE As Integer = 3      ' ALL
    Public Const FINFO_HASHALG As Integer = 4    ' LH, SH
    Public Const FINFO_MODULUS As Integer = 5    ' LH, SH
    Public Const FINFO_MINMODULUS As Integer = 6     ' LH
    Public Const FINFO_GROUPSIZE As Integer = 7       ' LH
    Public Const FINFO_LARGERECORDSIZE As Integer = 8  ' LH
    Public Const FINFO_MERGELOAD As Integer = 9       ' LH
    Public Const FINFO_SPLITLOAD As Integer = 10     ' LH
    Public Const FINFO_CURRENTLOAD As Integer = 11    ' LH (percentage)
    Public Const FINFO_NODENAME As Integer = 12       ' ALL. Null if local, else nodename
    Public Const FINFO_IS_AKFILE As Integer = 13     ' LH
    Public Const FINFO_CURRENTLINE As Integer = 14    ' SEQ
    Public Const FINFO_PARTNUM As Integer = 15     ' Distributed, Multivolume
    Public Const FINFO_STATUS As Integer = 16    ' Distributed, Multivolume
    Public Const FINFO_RECOVERYTYPE As Integer = 17   ' ALL
    Public Const FINFO_RECOVERYID As Integer = 18    ' LH
    Public Const FINFO_IS_FIXED_MODULUS As Integer = 19 ' LH

    ' File type values as returned by fileinfo.

    Public Const FILETYPE_MEMORY As Integer = 1
    Public Const FILETYPE_HASHED As Integer = 2
    Public Const FILETYPE_DYNAMIC As Integer = 3
    Public Const FILETYPE_TYPE1 As Integer = 4
    Public Const FILETYPE_SEQ As Integer = 5
    Public Const FILETYPE_MULTIVOLUME As Integer = 6
    Public Const FILETYPE_DISTRIBUTED As Integer = 7

    ' Lock status values returned by RECORDLOCKED().

    Public Const LOCK_MY_FILELOCK As Integer = 3      ' this user has filelock
    Public Const LOCK_MY_READU As Integer = 2     ' this user has readu lock
    Public Const LOCK_MY_READL As Integer = 1     ' this user has readl lock
    Public Const LOCK_NO_LOCK As Integer = 0       ' record not locked
    Public Const LOCK_OTHER_READL As Integer = -1    ' another user has readl lock
    Public Const LOCK_OTHER_READU As Integer = -2    ' another user has readu lock
    Public Const LOCK_OTHER_FILELOCK As Integer = -3     ' another user has filelock

    ' Error numbers.

    ' Numbers relating to the C library on the PC.
    ' These are adapted from the file errno.h.

    Public Const UVE_PC_CLIB_FIRST As Integer = 14000    ' First error number in range
    Public Const UVE_ENOENT As Integer = 14002    ' No such file or directory
    Public Const UVE_EIO As Integer = 14005      ' I/O error
    Public Const UVE_EBADF As Integer = 14009      ' Bad file number
    Public Const UVE_ENOMEM As Integer = 14012    ' No memory available
    Public Const UVE_EACCES As Integer = 14013    ' Permission denied
    Public Const UVE_EINVAL As Integer = 14022    ' Invalid argument
    Public Const UVE_ENFILE As Integer = 14023    ' File table overflow
    Public Const UVE_EMFILE As Integer = 14024    ' Too many open files
    Public Const UVE_ENOSPC As Integer = 14028    ' No space left on device

    ' Numbers relating to the Virtual Socket Library on the PC.

    Public Const UVE_BW_TIMEDOUT As Integer = 14560    ' Connection timed out
    Public Const UVE_BW_CONNREFUSED As Integer = 14561  ' Connection refused
    Public Const UVE_BW_HOSTDOWN As Integer = 14564    ' Host is down
    Public Const UVE_BW_HOSTUNREACH As Integer = 14565  ' Host is unreachable
    Public Const UVE_BW_NOTEMPTY As Integer = 14566    ' Directory not empty
    Public Const UVE_BW_PROCLIM As Integer = 14567   ' Too many processes
    Public Const UVE_BW_USERS As Integer = 14568       ' Too many users
    Public Const UVE_BW_DQUOT As Integer = 14569       ' Disc quota exceeded
    Public Const UVE_BW_STALE As Integer = 14570       ' Stale NFS file handle
    Public Const UVE_BW_REMOTE As Integer = 14571     ' Too many levels of remote in path
    Public Const UVE_BW_NOSTR As Integer = 14572       ' Device is not a stream
    Public Const UVE_BW_TIME As Integer = 14573     ' Timer expired
    Public Const UVE_BW_NOSR As Integer = 14574     ' Out of streams resources
    Public Const UVE_BW_DEADLK As Integer = 14578     ' Deadlock condition.
    Public Const UVE_BW_NOLCK As Integer = 14579       ' No record locks available.
    Public Const UVE_BW_TOOMANYSOCK As Integer = 14582  ' Too many open sockets
    Public Const UVE_BW_RESET As Integer = 14584       ' The socket has reset
    Public Const UVE_BW_NOTCPIP As Integer = 14591   ' TCPIP not loaded
    Public Const UVE_BW_DRVBUSY As Integer = 14592   ' TCPIP busy
    Public Const UVE_COMMFILE_SECURITY As Integer = 14593    ' Unable to read the command file, possible security breach
    Public Const UVE_BW_END As Integer = 14999    ' End of range

    ' Numbers in this group are derived from INFORMATION itself.

    Public Const UVE_FRST As Integer = 10000        ' First PI-specific error number

    ' Errors generated from library routine calls

    Public Const UVE_NOACCESS As Integer = 11000       ' Requested access denied
    Public Const UVE_NOSUPPORT As Integer = 11001     ' Function not supported on this system
    Public Const UVE_NOTRELATIVE As Integer = 11002    ' Relative pathname expected and not given
    Public Const UVE_PATHNOTFOUND As Integer = 11003      ' Pathname could not be found
    Public Const UVE_NOTASSIGNED As Integer = 11004    ' Device not assigned
    Public Const UVE_NODEVICE As Integer = 11005       ' Device not known
    Public Const UVE_ROFS As Integer = 11006        ' Device assigned with Read Only access
    Public Const UVE_BADSTTY As Integer = 11007     ' Bad stty option when device assigned
    Public Const UVE_UNKNOWN_USER As Integer = 11008      ' Attempting to send message to user not in PI
    Public Const UVE_SND_REQ_REC As Integer = 11009    ' Sender requires receive enabled
    Public Const UVE_MSG_REJECTED As Integer = 11010      ' Message rejected by recipient

    Public Const UVE_IID As Integer = 22003      ' Illegal record ID
    Public Const UVE_LRR As Integer = 22004      ' last record read (READNEXT)
    Public Const UVE_NFI As Integer = 22005      ' file.tag is not a file identifier

    ' Generic and visible file system errors

    Public Const UVE_RNF As Integer = 30001      ' Record not found
    Public Const UVE_LCK As Integer = 30002      ' File or record is locked by another user
    Public Const UVE_EXS As Integer = 30012      ' File already exists in an attempt to create
    Public Const UVE_EXCL As Integer = 30014        ' File opened exclusively by another user
    Public Const UVE_NAM As Integer = 30075      ' Bad file name
    Public Const UVE_FIFS As Integer = 30095        ' Fileid is incorrect for session
    Public Const UVE_SELFAIL As Integer = 30097    ' Select Failed
    Public Const UVE_LOCKINVALID As Integer = 30098    ' Lock number provided is invalid
    Public Const UVE_SEQOPENED As Integer = 30099     ' Filed opened for sequential access and hashed access tried
    Public Const UVE_HASHOPENED As Integer = 30100   ' Filed opened for hashed access and sequential access tried
    Public Const UVE_SEEKFAILED As Integer = 30101   ' Seek command failed
    Public Const UVE_INVALIDATKEY As Integer = 30103      ' Invalid Key used for GET/SET at variables
    Public Const UVE_UNABLETOLOADSUB As Integer = 30105  ' Unable to load subroutine on host
    Public Const UVE_BADNUMARGS As Integer = 30106   ' Bad number of arguments for subroutine, either too many or not enough
    Public Const UVE_SUBERROR As Integer = 30107       ' Subroutine failed to complete suceesfully
    Public Const UVE_ITYPEFTC As Integer = 30108       ' IType failed to complete correctly
    Public Const UVE_ITYPEFAILEDTOLOAD As Integer = 30109   ' IType failed to load
    Public Const UVE_ITYPENOTCOMPILED As Integer = 30110     ' The IType has not been compiled
    Public Const UVE_BADITYPE As Integer = 30111        ' It is not an itype or the itype is corrupt
    Public Const UVE_INVALIDFILENAME As Integer = 30112 ' Filename is null
    Public Const UVE_WEOFFAILED As Integer = 30113    ' Weofseq failed
    Public Const UVE_EXECUTEISACTIVE As Integer = 30114   ' An execute is currently active
    Public Const UVE_EXECUTENOTACTIVE As Integer = 30115     ' An execute is currently active
    Public Const UVE_CANT_ACCESS_PF As Integer = 30125     ' Cannot access part files
    Public Const UVE_FAIL_TO_CANCEL As Integer = 30126     ' Failed to cancel an execute
    Public Const UVE_INVALID_INFO_KEY As Integer = 30127     ' Bad key for session information
    Public Const UVE_CREATE_FAILED As Integer = 30128    ' The creation of a sequential file failed
    Public Const UVE_DUPHANDLE_FAILED As Integer = 30129     ' Failed to duplicate a pipe handle

    ' Errors for OPEN

    Public Const UVE_NVR As Integer = 31000      ' No VOC record
    Public Const UVE_NPN As Integer = 31001      ' No pathname in VOC record
    Public Const UVE_VNF As Integer = 31002      ' VOC file record not a File record

    ' Errors for CLEARFILE

    Public Const UVE_CFNEA As Integer = 31100      ' Clear file no exclusive access

    ' Errors for client library, taken from ICI

    Public Const UVE_LNA As Integer = 33200      ' Select list not active
    Public Const UVE_BSLN As Integer = 33211        ' Bad select list number
    Public Const UVE_BPID As Integer = 33212        ' Bad partfile id
    Public Const UVE_BAK As Integer = 33213      ' Bad AK file

    ' Error numbers generated explicitly by the interCALL server:

    Public Const UVE_BAD_COMMAND As Integer = 39000    ' command not recognized by server
    Public Const UVE_NO_LOGOUT As Integer = 39001     ' no way to perform a LOGOUT command
    Public Const UVE_BAD_LENGTH As Integer = 39002   ' data.length not a valid number
    Public Const UVE_NO_VOC As Integer = 39003    ' can't open the VOC file
    Public Const UVE_CLIENT_RESET As Integer = 39004      ' internal - client RESET received OK
    Public Const UVE_INVALID_SRC As Integer = 39005    ' @SYSTEM.RETURN.CODE non-numeric after EXECUTE
    Public Const UVE_TOOLONG_SRC As Integer = 39006    ' @SYSTEM.RETURN.CODE has more than 2 fields
    Public Const UVE_KEY_NOT_IMP As Integer = 39007    ' interCALL server key not implemented
    Public Const UVE_WRITE_FAILURE As Integer = 39008    ' WRITE failed and taken ELSE clause

    ' Errors for the Client/Server interface

    Public Const UVE_NODATA As Integer = 39101    ' Host not responding
    Public Const UVE_RCV_TIMEOUT As Integer = 39103    ' Timeout on receving packets
    Public Const UVE_AT_INPUT As Integer = 39119       ' Server waiting for input
    Public Const UVE_SESSION_NOT_OPEN As Integer = 39120     ' Session is not opened when an action has be tried on it
    Public Const UVE_UVEXPIRED As Integer = 39121     ' The database license has expired
    Public Const UVE_CSVERSION As Integer = 39122     ' Client or server is out of date Client/server functions have been updated
    Public Const UVE_COMMSVERSION As Integer = 39123      ' Client or server is out of date comms support has been updated
    Public Const UVE_BADSIG As Integer = 39124    ' Incorrect client/server being commuincated with
    Public Const UVE_BADDIR As Integer = 39125    ' The directory you are connecting to, either is not a correct database account or does not exist
    Public Const UVE_SERVERERR As Integer = 39126     ' An error has occurred on the server when trying to transmit an error code to the client
    Public Const UVE_BAD_UVHOME As Integer = 39127   ' Unable to find UV account
    Public Const UVE_INVALID_PATH As Integer = 39128      ' Invalid path found in UV.ACCOUNT
    Public Const UVE_INVALIDACCOUNT As Integer = 39129  ' Invalid account name supplied
    Public Const UVE_BAD_UVACCOUNT_FILE As Integer = 39130  ' The UV.ACCOUNT file could not be opened
    Public Const UVE_FTA_NEW_ACCOUNT As Integer = 39131 ' Failed to attach to specified account
    Public Const UVE_NOT_UVACCOUNT As Integer = 39132    ' not a valid database account
    Public Const UVE_FTS_TERMINAL As Integer = 39133      ' failed to setup the terminal for server
    Public Const UVE_ULR As Integer = 39134      ' The user limit has been reached on the server

    Public Const UVE_NO_NLS As Integer = 39135       ' NLS support not available
    Public Const UVE_MAP_NOT_FOUND As Integer = 39136     ' NLS map not found
    Public Const UVE_NO_LOCALE As Integer = 39137     ' NLS locale support not available
    Public Const UVE_LOCALE_NOT_FOUND As Integer = 39138      ' NLS locale not found
    Public Const UVE_CATEGORY_NOT_FOUND As Integer = 39139     ' NLS locale category not found

    Public Const UVE_SR_CREATE_PIPE_FAIL As Integer = 39200   ' Server failed to create the slave pipes
    Public Const UVE_SR_SOCK_CON_FAIL As Integer = 39201      ' The server failed to connect to the socket
    Public Const UVE_SR_GA_FAIL As Integer = 39202       ' Slave failed to give server the Go Ahead message
    Public Const UVE_SR_MEMALLOC_FAIL As Integer = 39203      ' Failed to allocate memory for the message from the slave
    Public Const UVE_SR_SLAVE_EXEC_FAIL As Integer = 39204     ' The slave failed to start correctly
    Public Const UVE_SR_PASS_TO_SLAVE_FAIL As Integer = 39205   ' Failed to the pass the message to the slave correctly
    Public Const UVE_SR_EXEC_ALLOC_FAIL As Integer = 39206     ' Server failed to allocate the memory for the execute buffer correctly
    Public Const UVE_SR_SLAVE_READ_FAIL As Integer = 39207     ' Failed to read from the slave correctly
    Public Const UVE_SR_REPLY_WRITE_FAIL As Integer = 39208   ' Failed to write the reply to the slave (ic_inputreply)
    Public Const UVE_SR_SIZE_READ_FAIL As Integer = 39209    ' Failed to read the size of the message from the slave

    ' OLE Automation specific codes

    Public Const UVE_NOERROR As Integer = 0  ' no error

    Public Const UVE_INVALIDFIELD As Integer = 40001     ' Invalid field offset
    Public Const UVE_SESSIONEXISTS As Integer = 40002   ' Session is already open
    Public Const UVE_BADPARAM As Integer = 40003      ' Bad parameter passed
    Public Const UVE_BADOBJECT As Integer = 40004    ' Incorrect object passed
    Public Const UVE_NOMORE As Integer = 40005   ' NextBlock method used when not in UVS_MORE state
    Public Const UVE_NOTATINPUT As Integer = 40006  ' Reply method used when not in UVS_REPLY state
    Public Const UVE_INVALID_DATAFIELD As Integer = 40007   ' Dictionary entry does not have a valid TYPE field
    Public Const UVE_BAD_DICTIONARY_ENTRY As Integer = 40008    ' Dictionary entry is invalid
    Public Const UVE_BAD_CONVERSION_DATA As Integer = 40009 ' Unable to convert data in field

    ' UNIRPC Error codes

    Public Const UVE_BAD_LOGINNAME As Integer = 80011       ' The user name or password provided is incorrect
    Public Const UVE_BAD_PASSWORD As Integer = 80019         ' The password provided has expired
    Public Const UVE_REM_AUTH_FAILED As Integer = 80036   ' Remote authorisation failed.
    Public Const UVE_ACCOUNT_EXPIRED As Integer = 80144   ' Account has expired
    Public Const UVE_RUN_REMOTE_FAILED As Integer = 80147      ' Unable to run as given user
    Public Const UVE_UPDATE_USER_FAILED As Integer = 80148    ' Unable to update user details
    Public Const UVE_RPC_BAD_CONNECTION As Integer = 81001    ' The connection may be passing corrupt data
    Public Const UVE_RPC_NO_CONNECTION As Integer = 81002      ' The connection is broken
    Public Const UVE_RPC_NOT_INITED As Integer = 81003     ' the rpc has not be initialised
    Public Const UVE_RPC_INVALID_ARG_TYPE As Integer = 81004       ' argument for message is not a valid type
    Public Const UVE_RPC_WRONG_VERSION As Integer = 81005      ' The version of the host RPC does not match
    Public Const UVE_RPC_BAD_SEQNO As Integer = 81006       ' packet message out of step
    Public Const UVE_RPC_NO_MORE_CONNECTIONS As Integer = 81007 ' No more connections available
    Public Const UVE_RPC_BAD_PARAMETER As Integer = 81008      ' bad parameter passed to the rpc
    Public Const UVE_RPC_FAILED As Integer = 81009      ' The RPC failed
    Public Const UVE_RPC_ARG_COUNT As Integer = 81010       ' bad number pf arguments for message
    Public Const UVE_RPC_UNKNOWN_HOST As Integer = 81011        ' The host name specified is not valid, or host not responding
    Public Const UVE_RPC_FORK_FAILED As Integer = 81012   ' rpc failed to fork service correctly
    Public Const UVE_RPC_CANT_OPEN_SERV_FILE As Integer = 81013 ' cannot find or open the unirpcserices file
    Public Const UVE_RPC_CANT_FIND_SERVICE As Integer = 81014     ' Cannot find UVRPC in the host services file
    Public Const UVE_RPC_TIMEOUT As Integer = 81015    ' The connection has timed out
    Public Const UVE_RPC_REFUSED As Integer = 81016    ' The connection was refused or RPC deamon not running
    Public Const UVE_RPC_SOCKET_INIT_FAILED As Integer = 81017   ' Failed to initialize network interface
    Public Const UVE_RPC_SERVICE_PAUSED As Integer = 81018    ' The RPC service has been paused
    Public Const UVE_RPC_BAD_TRANSPORT As Integer = 81019      ' An invalid transport has been used
    Public Const UVE_RPC_BAD_PIPE As Integer = 81020         ' Invalid pipe handle
    Public Const UVE_RPC_PIPE_WRITE_ERROR As Integer = 81021       ' Error writing to pipe
    Public Const UVE_RPC_PIPE_READ_ERROR As Integer = 81022  ' Error reading from pipe

    ' Keys and Parameter values

    ' Blocking strategy values

    Public Const WAIT_ON_LOCKED As Integer = 1   ' Wait if record is locked
    Public Const RETURN_ON_LOCKED As Integer = 2      ' Return with an error if record locked

    ' Locking strategy values

    Public Const EXCLUSIVE_UPDATE As Integer = 1      ' Exclusive update lock - U
    Public Const SHARED_READ As Integer = 2  ' Shared read locks - L
    Public Const NO_LOCKS As Integer = 0     ' No locking

    ' Release strategy values

    Public Const WRITE_RELEASE As Integer = 1     ' Release lock after write
    Public Const READ_RELEASE As Integer = 2       ' Release lock after read
    Public Const EXPLICIT_RELEASE As Integer = 4      ' Release locks explicitely
    Public Const CHANGE_RELEASE As Integer = 8   ' Additionally - release on change of record id

    ' HostType values

    Public Const UVT_UNIX As Integer = 1     ' UNIX
    Public Const UVT_WINNT As Integer = 2      ' Windows NT
    Public Const UVT_WIN95 As Integer = 3      ' Windows 95
    Public Const UVT_NONE As Integer = 0     ' No connection

    ' Transport values

    Public Const NETWORK_DEFAULT As Integer = 0 ' default
    Public Const NETWORK_TCP As Integer = 1  ' TCP/IP
    Public Const NETWORK_LANMAN As Integer = 2   ' Lan Manager Named Pipes

    ' Get/SetAtVariable parameters for host variables AT as integer = @

    Public Const AT_LOGNAME As Integer = 1
    Public Const AT_PATH As Integer = 2
    Public Const AT_USERNO As Integer = 3
    Public Const AT_WHO As Integer = 4
    Public Const AT_TRANSACTION As Integer = 5
    Public Const AT_DATA_PENDING As Integer = 6
    Public Const AT_USER_RETURN_CODE As Integer = 7
    Public Const AT_SYSTEM_RETURN_CODE As Integer = 8
    Public Const AT_NULL_STR As Integer = 9
    Public Const AT_SCHEMA As Integer = 10

    ' Sequential file Seek method - RelPos parameter values

    Public Const UVT_START As Integer = 0      ' start of file
    Public Const UVT_CURR As Integer = 1     ' current position
    Public Const UVT_END As Integer = 2   ' end of file

    ' Command object's CommandStatus values

    Public Const UVS_COMPLETE As Integer = 0       ' Execution complete
    Public Const UVS_REPLY As Integer = 1      ' Waiting for a reply
    Public Const UVS_MORE As Integer = 2     ' More data to come

    ' File object's status

    Public Const UVS_RNF_RNF_DICT As Integer = 1      ' ReadNamedField Record not found in DICT

    ' NLS status

    Public Const UVS_NLS_DEFAULT As Integer = 1 ' Default used
    Public Const UVS_NLS_BAD_MARKS As Integer = 2    ' marks are not distinct

    ' NLS Locale Category keys

    Public Const UVLC_ALL As Integer = 0
    Public Const UVLC_COLLATE As Integer = 1
    Public Const UVLC_CTYPE As Integer = 2
    Public Const UVLC_MONETARY As Integer = 3
    Public Const UVLC_NUMERIC As Integer = 4
    Public Const UVLC_TIME As Integer = 5

    ' Registered object names to be used with CreateObject

    Public Const UV_SESSION_OBJECT As String = "Uniobjects.Unioaifctrl"
    Public Const UV_DARRAY_OBJECT As String = "Uniobjects.UniDynArray"

    Public Const UNI_SESSION_OBJECT As String = "Uniobjects.Unioaifctrl"
    Public Const UNI_DARRAY_OBJECT As String = "Uniobjects.UniDynArray"

    '  END-CODE

    Public glHsgItems As Long

    Public gUVCon As UniSession
	'Public guvCommand As UniCommand
	'Public guvAccountFile As UniFile
	'Public guvPropertyFile As UniFile
	'Public guvSelectList As UniSelectList
	'Public guvRecord As UniDynArray

	Public gUniverseDBSystem As String = "Comsan7"
	Public gUniverseDBAccount As String = "SAFFRON"
	Public gUniverseDBUserID As String
	Public gUniverseDBPassword As String


	Public Function OpenUV(UVSystemID As String, UVAccountID As String, UVUserID As String, UVPassword As String) As Boolean

        Dim bSessOK As Boolean ', lSessErrMsg As Long, lError As Long

        Try

            If bSessOK Then
            Else
                gUVCon = UniObjects.OpenSession(UVSystemID, UVUserID, UVPassword, UVAccountID, "uvcs")

                bSessOK = True

                If gUVCon.IsActive Then
                    bSessOK = True

                    'guvAccountFile = UVCon.CreateUniFile("RENT.ACCOUNTS")
                    'guvPropertyFile = UVCon.CreateUniFile("PROPERTY")
                    'guvCommand = UVCon.CreateUniCommand
                    'guvSelectList = UVCon.CreateUniSelectList(0)

                Else
                    MsgBox("No Connection to Universe database", vbCritical)
                    bSessOK = False
                End If

            End If

        Catch ex As Exception
            MsgBox(ex.ToString)

        End Try
        Return bSessOK
    End Function

End Module
