﻿Imports System.Threading
Imports WPFExtractSaffronData
Imports WPFThreadedBatchProcessing

Public Class clsExtractData

	Public Event FileStarted(CallingObject As clsExtractData, sFileName As String)
	Public Event FileSelectionCompleted(CallingObject As clsExtractData, sFileName As String, iNoOfRecs As Integer)
	Public Event UpdateStepCount(CallingObject As clsExtractData, iStepCount As Integer)
	Public Event FileCompleted(CallingObject As clsExtractData, sFileName As String)
	Public Event FileFailed(CallingObject As clsExtractData, sFileName As String, sReasonFailure As String)
	Public Event ProcessCompleted(CallingObject As clsExtractData)
	Public Event Processfailed(CallingObject As clsExtractData, sFileName As String, sReasonFail As String)
	Public Event PassMessage(CallingObject As clsExtractData, sMessage As String)

	Public Event evReportError(CallingObject As clsExtractData, oErr As ErrorItem, sReason As String)

	Public DestinationFolder As String

	Public WithEvents oFileLoader As clsUVData
	Protected Friend iPrevPercentPoint As Integer = 0
	Protected Friend iNoOfRecords As Integer = 0
	Protected Friend iRecordNo As Integer = 0
	Protected Friend iTotalSteps As Integer = 1000

	Protected Friend sCurrFileName As String = ""
	Protected Friend iNoOFFiles As Integer = 0
	Protected Friend iFileNo As Integer = 0
	Protected Friend sLastReasonFail As String = ""
	Protected Friend bSpecialOnly As Boolean = False
	Protected Friend bMissingOnly As Boolean = False
	Protected Friend dStartTimeOfFile As Date = Now

	Protected Friend LastStepUpd As Date = Now

	Public Property bAbort As Boolean
		Get
			Dim bReturn As Boolean = False
			If oFileLoader IsNot Nothing Then bReturn = oFileLoader.bAbort
			Return bReturn
		End Get
		Set(value As Boolean)
			If oFileLoader IsNot Nothing Then oFileLoader.bAbort = value
		End Set
	End Property

	Function ProcessDataToResult() As Boolean
		Dim bReturn As Boolean = False
		Dim aAllFiles As New ArrayList, sSets As String
		Dim aSetParts As ArrayList
		Dim sFilenameToDo As String, sQueryParams As String
		Dim sFieldNames = "", aFieldNames As ArrayList, sDictPrefix As String = ""
		Dim aSubTables As ArrayList

		Try

			If bSpecialOnly Then
				sSets = GetMySetting("ProcessDataFilesSpecialPath")
			Else
				sSets = GetMySetting("ProcessDataFilesPath")
			End If

			sSets = LoadFile(sSets)

			aAllFiles = New ArrayList(Split(sSets, vbCrLf))

			iNoOFFiles = aAllFiles.Count
			iFileNo = 0

			For Each sSet In aAllFiles
				iFileNo += 1
				aSetParts = New ArrayList(Split(sSet, "|"))
				If aSetParts.Count > 1 Then

					sFilenameToDo = aSetParts(0)
					sQueryParams = aSetParts(1)
					If aSetParts.Count > 2 Then sDictPrefix = aSetParts(2) Else sDictPrefix = ""
					If aSetParts.Count > 3 Then sFieldNames = aSetParts(3) Else sFieldNames = ""
					If aSetParts.Count > 4 Then aSubTables = New ArrayList(Split(aSetParts(4), ";")) Else aSubTables = Nothing
					If sFieldNames > "" Then
						If Not (Left(sFilenameToDo, 1) = "*" Or Left(sFilenameToDo, 2) = "--") Then
							aFieldNames = New ArrayList(Split(sFieldNames.Replace(" ", ","), ","))
							bReturn = ProcessUVFile(sFilenameToDo, sQueryParams, aFieldNames, sDictPrefix, aSubTables)
						End If
					End If
				End If
				If bAbort Or giErrorCountdown < 1 Then Exit For
			Next

			If bAbort Then
				RaiseEvent PassMessage(Me, "Process aborted")
				RaiseEvent ProcessCompleted(Me)

			ElseIf Not bReturn Then
				If sLastReasonFail = "" Then sLastReasonFail = "Unknown reason"
				RaiseEvent Processfailed(Me, sCurrFileName, sLastReasonFail)
			Else

				RaiseEvent ProcessCompleted(Me)

			End If

		Catch ex As Exception
			ReportError(ex)
		End Try

		Return bReturn
	End Function




	Public Function ProcessUVFile(sFilenameToExp As String, sSelectParams As String, aValues As ArrayList, sDictPrefix As String, aSubTables As ArrayList) As Boolean
		Dim bReturn As Boolean = False
		Try


			iPrevPercentPoint = 0
			sCurrFileName = sFilenameToExp

			oFileLoader = New clsUVData

			oFileLoader.sUVFileName = sFilenameToExp
			oFileLoader.sSelectQueryCondition = sSelectParams
			oFileLoader.sDictPrefix = sDictPrefix
			oFileLoader.bMissingIDsOnly = bMissingOnly

			For Each sFieldName In aValues
				oFileLoader.mcDictAttr.Add(sFieldName)
				If bAbort Then Exit For
			Next

			If oFileLoader.LoadDictionary() Then
				If aSubTables IsNot Nothing Then oFileLoader.maSubTables.AddRange(aSubTables)
				bReturn = oFileLoader.LoadData()
			End If

		Catch ex As Exception
			ReportError(ex)
		End Try

		Return bReturn
	End Function




	Public Sub New()

	End Sub

	Private Sub oFileLoader_AllRecordsProcessed(CallingObject As clsUVData) Handles oFileLoader.AllRecordsProcessed

		RaiseEvent FileCompleted(Me, CallingObject.sUVFileName)

	End Sub


	Private Sub oFileLoader_NoRecordsProcessed(CallingObject As clsUVData, sReason As String) Handles oFileLoader.NoRecordsProcessed
		RaiseEvent FileFailed(Me, CallingObject.sUVFileName, sReason)

	End Sub


	Private Sub oFileLoader_RecordProcessed(CallingObject As clsUVData, RecordAt As Integer) Handles oFileLoader.RecordProcessed
		Dim iStepCount As Integer

		Try
			iRecordNo = RecordAt
			iStepCount = Math.Floor(RecordAt / iNoOfRecords * iTotalSteps)
			If iStepCount <> iPrevPercentPoint Or Now.Second <> LastStepUpd.Second Then

				RaiseEvent UpdateStepCount(Me, iStepCount)
				iPrevPercentPoint = iStepCount
				LastStepUpd = Now
			End If
		Catch ex As Exception
			ReportError(ex)
		End Try

	End Sub


	Private Sub oFileLoader_RecordsSelected(CallingObject As clsUVData, RecordCount As Integer) Handles oFileLoader.RecordsSelected
		iNoOfRecords = RecordCount

		RaiseEvent FileSelectionCompleted(Me, CallingObject.sUVFileName, RecordCount)

	End Sub


	Public Sub ReportError(exin As Exception)
		Dim oErr As ErrorItem
		Try
			oErr = glErrors.NewError(exin)
			RaiseEvent evReportError(Me, oErr, oErr.ToString)

		Catch ex As Exception

		End Try
	End Sub

	Private Sub oFileLoader_PassMessage(CallingObject As clsUVData, sMessage As String) Handles oFileLoader.PassMessage

		Try
			RaiseEvent PassMessage(Me, sMessage)
		Catch ex As Exception
			ReportError(ex)
		End Try

	End Sub

	Private Sub oFileLoader_FileProcessingStarted(CallingObject As clsUVData) Handles oFileLoader.FileProcessingStarted
		Try
			RaiseEvent FileStarted(Me, CallingObject.sUVFileName)
		Catch ex As Exception
			ReportError(ex)
		End Try
	End Sub
End Class

