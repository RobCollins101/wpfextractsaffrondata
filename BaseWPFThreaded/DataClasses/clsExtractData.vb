﻿Imports System.Threading
Imports WPFExtractSaffronData
Imports WPFThreadedBatchProcessing

Public Class clsExtractData

    Public Event BatchStarted(CallingObject As clsExtractData, sFileName As String)
    Public Event BatchSelectionCompleted(CallingObject As clsExtractData, sFileName As String, iNoOfRecs As Integer)
    Public Event UpdateStepCount(CallingObject As clsExtractData, iStepCount As Integer)
    Public Event BatchCompleted(CallingObject As clsExtractData, sFileName As String)
    Public Event BatchFailed(CallingObject As clsExtractData, sFileName As String, sReasonFailure As String)
    Public Event ProcessCompleted(CallingObject As clsExtractData)
    Public Event Processfailed(CallingObject As clsExtractData, sFileName As String, sReasonFail As String)
    Public Event PassMessage(CallingObject As clsExtractData, sMessage As String)

    Public Event evReportError(CallingObject As clsExtractData, oErr As ErrorItem, sReason As String)

    Public DestinationFolder As String

    Public WithEvents oOneBatchProcessorClass As clsDoStuffOneBatchData
    Protected Friend iPrevPercentPoint As Integer = 0
    Protected Friend iNoOfRecords As Integer = 0
    Protected Friend iRecordNo As Integer = 0
    Protected Friend iTotalSteps As Integer = 1000

    Protected Friend sCurrFileName As String = ""
    Protected Friend iNoOFFiles As Integer = 0
    Protected Friend iFileNo As Integer = 0
    Protected Friend sLastReasonFail As String = ""
    Protected Friend bSpecialOnly As Boolean = False
    Protected Friend dStartTimeOfFile As Date = Now

    Protected Friend LastStepUpd As Date = Now

    Public Property bAbort As Boolean
        Get
            Return oOneBatchProcessorClass.bAbort
        End Get
        Set(value As Boolean)
            oOneBatchProcessorClass.bAbort = value
        End Set
    End Property

    Function ProcessDataToResult() As Boolean
        Dim bReturn As Boolean = False
        Dim aAllFiles As New ArrayList, sSets As String
        Dim aSetParts As ArrayList
        Dim sFilenameToDo As String, sQueryParams As String
        Dim sFieldNames = "", aFieldNames As ArrayList, sDictPrefix As String = ""

        Try

            If bSpecialOnly Then
                sSets = GetMySetting("ProcessDataFilesSpecialPath")
            Else
                sSets = GetMySetting("ProcessDataFilesPath")
            End If

            sSets = LoadFile(sSets)

            aAllFiles = New ArrayList(Split(sSets, vbCrLf))

            iNoOFFiles = aAllFiles.Count
            iFileNo = 0

            For Each sSet In aAllFiles
                iFileNo += 1
                aSetParts = New ArrayList(Split(sSet, "|"))
                If aSetParts.Count > 1 Then
                    sFilenameToDo = aSetParts(0)
                    sQueryParams = aSetParts(1)
                    sDictPrefix = aSetParts(2)
                    sFieldNames = aSetParts(3)
                    If sFieldNames > "" Then
                        If Not (Left(sFilenameToDo, 1) = "*" Or Left(sFilenameToDo, 2) = "--") Then
                            aFieldNames = New ArrayList(Split(sFieldNames.Replace(" ", ","), ","))
                            bReturn = ProcessUVFile(sFilenameToDo, sQueryParams, aFieldNames, sDictPrefix)
                        End If
                    End If
                End If
                If bAbort Or giErrorCountdown < 1 Then Exit For
            Next

            If bAbort Then
                RaiseEvent PassMessage(Me, "Process aborted")
                RaiseEvent ProcessCompleted(Me)

            ElseIf Not bReturn Then
                If sLastReasonFail = "" Then sLastReasonFail = "Unknown reason"
                RaiseEvent Processfailed(Me, sCurrFileName, sLastReasonFail)
            Else

                RaiseEvent ProcessCompleted(Me)

            End If

        Catch ex As Exception
            ReportError(ex)
        End Try

        Return bReturn
    End Function




    Public Function ProcessUVFile(sFilenameToExp As String, sSelectParams As String, aValues As ArrayList, sDictPrefix As String) As Boolean
        Dim bReturn As Boolean = False
        Try


            iPrevPercentPoint = 0
            sCurrFileName = sFilenameToExp

            oOneBatchProcessorClass = New clsDoStuffOneBatchData

            oOneBatchProcessorClass.sBatchIdentifier = sFilenameToExp
            oOneBatchProcessorClass.sSelectQueryCondition = sSelectParams
            oOneBatchProcessorClass.sDictPrefix = sDictPrefix

            For Each sFieldName In aValues
                oOneBatchProcessorClass.mcDictAttr.Add(sFieldName)
                If bAbort Then Exit For
            Next

            If oOneBatchProcessorClass.PreparingProcess() Then
                bReturn = oOneBatchProcessorClass.MainDataProcess()
            End If

        Catch ex As Exception
            ReportError(ex)
        End Try

        Return bReturn
    End Function




    Public Sub New()

    End Sub

    Private Sub oBatchLoader_AllRecordsProcessed(CallingObject As clsDoStuffOneBatchData) Handles oOneBatchProcessorClass.AllBatchRecordsProcessed

        RaiseEvent BatchCompleted(Me, CallingObject.sBatchIdentifier)

    End Sub


    Private Sub oBatchLoader_NoRecordsProcessed(CallingObject As clsDoStuffOneBatchData, sReason As String) Handles oOneBatchProcessorClass.NoBatchRecordsProcessed
        RaiseEvent BatchFailed(Me, CallingObject.sBatchIdentifier, sReason)

    End Sub


    Private Sub oBatchLoader_RecordProcessed(CallingObject As clsDoStuffOneBatchData, RecordAt As Integer) Handles oOneBatchProcessorClass.RecordProcessed
        Dim iStepCount As Integer

        Try
            iRecordNo = RecordAt
            iStepCount = Math.Floor(RecordAt / iNoOfRecords * iTotalSteps)
            If iStepCount <> iPrevPercentPoint Or Now.Second <> LastStepUpd.Second Then

                RaiseEvent UpdateStepCount(Me, iStepCount)
                iPrevPercentPoint = iStepCount
                LastStepUpd = Now
            End If
        Catch ex As Exception
            ReportError(ex)
        End Try

    End Sub


    Private Sub oBatchLoader_RecordsSelected(CallingObject As clsDoStuffOneBatchData, RecordCount As Integer) Handles oOneBatchProcessorClass.BatchRecordsSelected
        iNoOfRecords = RecordCount

        RaiseEvent BatchSelectionCompleted(Me, CallingObject.sBatchIdentifier, RecordCount)

    End Sub


    Public Sub ReportError(exin As Exception)
        Dim oErr As ErrorItem
        Try
            oErr = glErrors.NewError(exin)
            RaiseEvent evReportError(Me, oErr, oErr.ToString)

        Catch ex As Exception

        End Try
    End Sub

    Private Sub oBatchLoader_PassMessage(CallingObject As clsDoStuffOneBatchData, sMessage As String) Handles oOneBatchProcessorClass.PassMessage

        Try
            RaiseEvent PassMessage(Me, sMessage)
        Catch ex As Exception
            ReportError(ex)
        End Try

    End Sub

    Private Sub oBatchLoader_FileProcessingStarted(CallingObject As clsDoStuffOneBatchData) Handles oOneBatchProcessorClass.BatchProcessingStarted
        Try
            RaiseEvent BatchStarted(Me, CallingObject.sBatchIdentifier)
        Catch ex As Exception
            ReportError(ex)
        End Try
    End Sub
End Class

