﻿Imports System.Threading

Public Class clsExampleListData
    Inherits SortedList(Of Integer, clsExampleRecordData)

    Public Property MaxPrimeValue As Long = 1000000
    Public Event NewPrimeFound(iNumFound As Long)
    Public Event UpdatePercentagePoint(iPercent As Integer)
    Public sPrimeList As String = ""
    Public bAbort As Boolean = False
    Public lLastSquare As Long = 1, lNextSquare As Long = 4

    Function ProcessDataToResult() As String
        Dim sResult As String = ""

        If Me.Count = 0 Then GenerateValues()

        sResult = sPrimeList

        Return sResult
    End Function


    Public Sub GenerateValues()
        Dim i As Long, i2 As Long, iTest As Long, iMax As Long

        Me.Clear()

        Me.Add(1, New clsExampleRecordData("1", "1 is prime"))
        Me.Add(2, New clsExampleRecordData("2", "2 is prime"))
        For i = 3 To MaxPrimeValue Step 2
            If i < 4 Then
                Me.Add(i, New clsExampleRecordData(i.ToString, i.ToString & " is prime"))
            Else
                If i >= lNextSquare Then
                    lLastSquare = Math.Floor(Math.Sqrt(i))
                    lNextSquare = (lLastSquare + 1) ^ 2
                End If

                'iMax = Math.Floor(Math.Sqrt(i))
                iMax = lLastSquare
                For i2 = 2 To Me.Count - 1
                    iTest = Me.Keys(i2)
                    If i Mod iTest = 0 Then Exit For
                    If iTest >= iMax Then
                        iTest = 0
                        Exit For
                    End If
                    iTest = 0
                    If bAbort Then Exit For
                Next
                If iTest = 0 Then
                    sPrimeList &= IIf(sPrimeList <> "", vbCrLf, "").ToString & i.ToString & " is prime"
                    Me.Add(i, New clsExampleRecordData(i.ToString, i.ToString & " is prime"))
                    RaiseEvent NewPrimeFound(i)
                End If
            End If
            If bAbort Then Exit For
        Next

    End Sub


    Public Sub New()

    End Sub

    Public Sub New(NewMaxValue As Long)
        MaxPrimeValue = NewMaxValue
    End Sub
End Class

Public Class clsExampleRecordData
    Public Property sValue As String
    Public Property sValue2 As String

    Public Sub New(Value1, Value2)
        sValue = Value1
        sValue2 = Value2
    End Sub

    Public Sub New()

    End Sub
End Class
