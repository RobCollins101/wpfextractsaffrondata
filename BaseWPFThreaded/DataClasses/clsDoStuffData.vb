﻿Imports System.Collections.Generic
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports System.Text.RegularExpressions

Public Class clsDoStuffOneBatchData


    ' Protected Friend mcDictItems As New SortedList(Of String, clsUVDictItem)



    Protected Friend sBatchIdentifier As String = ""
    Protected Friend sSelectQueryCondition As String = ""
    Protected Friend sDictPrefix As String = ""


    Protected Friend iAllRecordsSelected As Integer = 0
    Protected Friend iCurrRecordPosition As Integer = 0

    Public Event BatchProcessingStarted(CallingObject As clsDoStuffOneBatchData)
    Public Event PassMessage(CallingObject As clsDoStuffOneBatchData, sMessage As String)
    Public Event NoBatchRecordsProcessed(CallingObject As clsDoStuffOneBatchData, sReason As String)
    Public Event BatchRecordsSelected(CallingObject As clsDoStuffOneBatchData, RecordCount As Integer)
    Public Event RecordProcessed(CallingObject As clsDoStuffOneBatchData, RecordAt As Integer)
    Public Event AllBatchRecordsProcessed(CallingObject As clsDoStuffOneBatchData)

    Public Event evReportError(CallingObject As clsDoStuffOneBatchData, oErr As ErrorItem, sReason As String)


    Protected Friend bAbort As Boolean = False
    Protected Friend sValDelim As String = "|"

    Protected Friend dStartOfRecordProcessing As Date = Now

    Public sDBConnection As String = ""
    Protected Friend oDBSQLConnection As SqlConnection

    Public Sub New()
        Dim oTemp As Object = Nothing

        oTemp = GetMySetting("DestinationSQLDatabase")
        If sDBConnection = "" And oTemp IsNot Nothing Then sDBConnection = oTemp.ToString

    End Sub

    Protected Friend Function PreparingProcess() As Boolean
        'Dim sCommand As String ', selListDict As UniSelectList
        'Dim oDictRec As clsUVDictItem, oDictTest As clsUVDictItem, oOthDictRec As clsUVDictItem
        Dim iPos As Integer, iMax As Integer, bOK As Boolean
        Dim bReturn As Boolean = False, bIgnore As Boolean = False
        Try

            RaiseEvent PassMessage(Me, "Staring pre batch work")

            If bOK Then

                If Not bAbort Then
                    iMax = 0
                    'TODO  make the pre batch work happen here

                    For iPos = 0 To iMax


                        If bAbort Then Exit For
                    Next

                    bReturn = True
                    RaiseEvent PassMessage(Me, "All dictionary items loaded")
                Else
                    RaiseEvent PassMessage(Me, "dictionary items loading aborted")

                End If
            End If
        Catch ex As Exception
            ReportError(ex)
        End Try
        Return bReturn
    End Function

    Public Function MainDataProcess() As Boolean
        Dim bDestinationPrepared As Boolean = False ', oWriterStream As IO.TextWriter = Nothing
        Dim bReturn As Boolean = False, iRecCountSave As Integer = 100
        Dim sInsertTemplate As String = "", iAttemptCount As Integer = 1, bOK As Boolean = True
        Dim bDataSourceValid As Boolean, bDataSelectDone As Boolean

        Try

            If Not bAbort Then

                Do While iAttemptCount > 0 And iAttemptCount < 3 And Not bAbort And giErrorCountdown > 0
                    Try
                        'bOK = some prapratory validation work
                        If bOK Then

                            If bAbort Then
                                RaiseEvent PassMessage(Me, "Batch process being aborted")

                            Else
                                RaiseEvent BatchProcessingStarted(Me)

                                'TODO Make selection of object

                                bDataSourceValid = True

                                If bDataSourceValid Then

                                    'TODO Select records into a collection of some kind

                                    iAllRecordsSelected = 0 ' Cound of records

                                    bDataSelectDone = True

                                    If bDataSelectDone Then
                                        RaiseEvent BatchRecordsSelected(Me, iAllRecordsSelected)

                                        For iCurrRecordPosition = 0 To iAllRecordsSelected

                                            Try

                                                'TODO do the processing here for one record

                                            Catch ex As Exception
                                                RaiseEvent PassMessage(Me, "Error on record " & iCurrRecordPosition & vbCrLf & ex.Message)
                                            End Try

                                            If bAbort Then
                                                RaiseEvent PassMessage(Me, "Batch Processing for " & sBatchIdentifier & " aborted")
                                                Exit For
                                            Else

                                                RaiseEvent RecordProcessed(Me, iCurrRecordPosition)
                                            End If
                                        Next

                                        If Not bAbort Then
                                            bReturn = True

                                            RaiseEvent AllBatchRecordsProcessed(Me)
                                        End If
                                    Else
                                        RaiseEvent NoBatchRecordsProcessed(Me, "No Records selected")
                                    End If
                                Else
                                    RaiseEvent NoBatchRecordsProcessed(Me, "Batch selection could not be opened")

                                End If
                                PostBatchProcessing()

                            End If


                        Else
                            If sBatchIdentifier = "" Then
                                RaiseEvent NoBatchRecordsProcessed(Me, "No batch identifier provided")
                            Else

                                RaiseEvent NoBatchRecordsProcessed(Me, "Selector was not opened for " & sBatchIdentifier)
                            End If

                        End If
                        iAttemptCount = 0

                        RaiseEvent PassMessage(Me, "All processing completed for Batch  " & sBatchIdentifier & vbCrLf)

                    Catch ex As Exception
                        If iAttemptCount < 2 Then
                            iAttemptCount += 1
                            RaiseEvent PassMessage(Me, "Second attempt for batch """ & sBatchIdentifier & """")
                            Threading.Thread.Sleep(20000)
                        Else
                            RaiseEvent NoBatchRecordsProcessed(Me, "ERROR : " & ex.ToString)
                            ReportError(ex)

                        End If

                    End Try
                Loop
            End If
        Catch ex As Exception
            RaiseEvent NoBatchRecordsProcessed(Me, "ERROR : " & ex.ToString)
            ReportError(ex)

        End Try
        Return bReturn
    End Function


    Public Sub PostBatchProcessing()

        Dim sFieldText As String = "", sQuery As String = "", s
        Dim sReturn As String = ""

        Try
            If Not bAbort Then
                'oTemp = GetMySetting("DestinationSQLDatabase")
                'If sDBConnection = "" And oTemp IsNot Nothing Then sDBConnection = oTemp.ToString

                'If oDBSQLConnection Is Nothing Then oDBSQLConnection = New SqlConnection(sDBConnection)
                'If oDBSQLConnection.State <> System.Data.ConnectionState.Open Then oDBSQLConnection.Open()

                'If oDBSQLConnection.State = System.Data.ConnectionState.Open Then

                '    RaiseEvent PassMessage(Me, "Removing blank columns from SQL table [" & sBatchIdentifier & "]")

                '    For iPos = 0 To mcDictAttr.Count - 1

                '        sFieldText &= IIf(sFieldText <> "", "," & vbCrLf, "")
                '        sAttrKey = mcDictAttr(iPos)

                '        If mcDictItems.ContainsKey(sAttrKey) Then

                '            oDict = mcDictItems(sAttrKey)
                '            sFieldText &= oDict.ColumnHeadingClean

                '            If Not oDict.KeyField And Not oDict.HasData Then

                '                iNoOfColsRemoved += 1

                '                'RaiseEvent PassMessage(Me, "Removing unused column " & oDict.ColumnHeadingClean)

                '                sQuery = "ALTER TABLE [" & sBatchIdentifier & "]"
                '                sQuery &= " DROP COLUMN [" & oDict.ColumnHeadingClean & "]"

                '                oCmd = New SqlCommand(sQuery, oDBSQLConnection)
                '                oCmd.ExecuteNonQuery()

                '            ElseIf Not oDict.KeyField And oDict.Length < 100 Then

                '                sQuery = "ALTER TABLE [" & sBatchIdentifier & "]"
                '                sQuery &= " ALTER COLUMN [" & oDict.ColumnHeadingClean & "] NVARCHAR(" & ((CInt((oDict.Length - 1) \ 10) + 1) * 10).ToString & ")"

                '                oCmd = New SqlCommand(sQuery, oDBSQLConnection)
                '                oCmd.ExecuteNonQuery()

                '            End If
                '        End If
                '        If bAbort Then Exit For
                '    Next

                '    If iNoOfColsRemoved > 0 Then
                '        RaiseEvent PassMessage(Me, "Removing " & iNoOfColsRemoved.ToString & " unused columns")
                '    Else
                '        RaiseEvent PassMessage(Me, "No unused columns")
                '    End If

                'End If
            End If

        Catch ex As Exception

            ReportError(ex)

        End Try
    End Sub

    Public Sub ReportError(exin As Exception)
        Dim oErr As ErrorItem
        Try
            oErr = glErrors.NewError(exin)
            RaiseEvent evReportError(Me, oErr, oErr.ToString)

        Catch ex As Exception

        End Try
    End Sub

End Class



'Public Class clsUVDictItem
'    Protected Friend UVDictName As String = ""
'    Protected Friend UVDictNameOnExport As String = ""
'    Protected Friend ColumnHeading As String = ""
'    Protected Friend DictType As String = ""
'    Protected Friend AttrNo As String = ""
'    Protected Friend IConv As String = ""
'    Protected Friend OConv As String = ""
'    Protected Friend Length As Integer = 10
'    Protected Friend KeyField As Boolean = False
'    Protected Friend HasData As Boolean = False


'    Function ColumnHeadingClean() As String
'        Dim sReturn As String
'        sReturn = StrConv(UVDictNameOnExport.Replace(",", " ").Replace(":", " ").Replace("@", " ").Replace(UV_VM, " ").Replace(UV_SM, " ").Replace(Oth_VM, " ").Replace(Oth_SM, " ").Replace(".", " ").Replace("(", " ").Replace(")", " ").Replace("<", " ").Replace(">", " ").Replace("/", " ").Replace("\", " ").Replace("[", " ").Replace("]", " ").Replace("-", " ").Replace("+", " ").Replace("?", " ").Replace("'", " ").Replace("`", " ").Replace("""", " ").Replace("*", " ").Replace("#", " ").Replace("|", " ").Replace("$", " ").Replace("%", " ").Replace("^", " ").Replace("&", " ").Replace(",", " "), VbStrConv.ProperCase).Replace(" ", "")
'        If Left(sReturn, 1) = "_" Then sReturn = AttrNo & sReturn
'        If IsNumeric(Left(sReturn, 1)) Then sReturn = "Attr" & sReturn

'        Return sReturn
'    End Function

'End Class
