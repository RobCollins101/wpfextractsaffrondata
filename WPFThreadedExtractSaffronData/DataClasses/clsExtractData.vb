﻿Imports System.Threading
Imports WPFExtractSaffronData
Imports WPFThreadedBatchProcessing

Public Class clsExtractData

    Public Event FileStarted(CallingObject As clsExtractData, sFileName As String)
    Public Event FileSelectionCompleted(CallingObject As clsExtractData, sFileName As String, iNoOfRecs As Integer)
    Public Event UpdateStepCount(CallingObject As clsExtractData, iStepCount As Integer)
    Public Event FileCompleted(CallingObject As clsExtractData, sFileName As String)
    Public Event FileFailed(CallingObject As clsExtractData, sFileName As String, sReasonFailure As String)
    Public Event ProcessCompleted(CallingObject As clsExtractData)
    Public Event Processfailed(CallingObject As clsExtractData, sFileName As String, sReasonFail As String)
    Public Event PassMessage(CallingObject As clsExtractData, sMessage As String)

    Public Event evReportError(CallingObject As clsExtractData, oErr As ErrorItem, sReason As String)

    Public DestinationFolder As String

    Public WithEvents oFileLoader As clsUVData
    Protected Friend iPrevPercentPoint As Integer = 0
    Protected Friend iNoOfRecords As Integer = 0
    Protected Friend iRecordNo As Integer = 0
    Protected Friend iTotalSteps As Integer = 1000

    Protected Friend sCurrFileName As String = ""
    Protected Friend iNoOFFiles As Integer = 0
    Protected Friend iFileNo As Integer = 0
    Protected Friend sLastReasonFail As String = ""
    Protected Friend bSpecialOnly As Boolean = False

    Protected Friend LastStepUpd As Date = Now

    Public Property bAbort As Boolean
        Get
            Dim bReturn As Boolean = False
            If oFileLoader IsNot Nothing Then bReturn = oFileLoader.bAbort
            Return bReturn
        End Get
        Set(value As Boolean)
            If oFileLoader IsNot Nothing Then oFileLoader.bAbort = value
        End Set
    End Property

    Function ProcessDataToResult() As Boolean
        Dim bReturn As Boolean = False
        Dim aAllFiles As New ArrayList, sSets As String = ""
        Dim aSetParts As ArrayList
        Dim sFilenameToDo As String, sQueryParams As String
        Dim sFieldNames = "", aFieldNames As ArrayList, sDictPrefix As String = ""
        Dim oTemp As Object

        Try

            If bSpecialOnly Then
                oTemp = GetMySetting("ProcessDataFilesSpecialPath")
                If oTemp IsNot Nothing Then sSets = LoadFile(oTemp.ToString())

                If sSets = "" Then
                    oTemp = GetMySetting("ProcessDataFilesSpecial")
                    If oTemp IsNot Nothing Then sSets = oTemp.ToString
                End If

            Else
                oTemp = GetMySetting("ProcessDataFilesPath")
                If oTemp IsNot Nothing Then sSets = LoadFile(oTemp.ToString())
                If sSets = "" Then
                    oTemp = GetMySetting("ProcessDataFiles")
                    If oTemp IsNot Nothing Then sSets = oTemp.ToString
                End If

            End If

            aAllFiles = New ArrayList(Split(sSets, vbCrLf))

            'bReturn = ProcessPropertyFile()

            'If bReturn Then

            iNoOFFiles = aAllFiles.Count
            iFileNo = 0

            For Each sSet In aAllFiles
                iFileNo += 1
                aSetParts = New ArrayList(Split(sSet, "|"))
                If aSetParts.Count > 1 Then
                    sFilenameToDo = Trim(aSetParts(0))
                    sQueryParams = Trim(aSetParts(1))
                    sDictPrefix = Trim(aSetParts(2))
                    sFieldNames = Trim(aSetParts(3))
                    If sFieldNames > "" Then
                        If Not (Left(sFilenameToDo, 1) = "*" Or Left(sFilenameToDo, 2) = "--") Then
                            aFieldNames = New ArrayList(Split(sFieldNames.Replace(" ", ","), ","))
                            bReturn = ProcessUVFile(sFilenameToDo, sQueryParams, aFieldNames, sDictPrefix)
                        End If
                    End If

                    If bAbort Or giErrorCountdown < 1 Then Exit For
                End If
                'If Not bReturn Or bAbort Then Exit For
            Next

            If bAbort Then

                'RaiseEvent ProcessCompleted(Me)

            ElseIf Not bReturn Or giErrorCountdown < 1 Then
                If sLastReasonFail = "" Then sLastReasonFail = "Unknown reason"
                RaiseEvent Processfailed(Me, sCurrFileName, sLastReasonFail)
            Else

                RaiseEvent ProcessCompleted(Me)

            End If
            'Else
            'If sLastReasonFail = "" Then sLastReasonFail = "Unknown reason"
            'RaiseEvent Processfailed(Me, sCurrFileName, sLastReasonFail)

            'End If

        Catch ex As Exception
            ReportError(ex)
        End Try

        Return bReturn
    End Function




    Public Function ProcessUVFile(sFilenameToExp As String, sSelectParams As String, aValues As ArrayList, sDictPrefix As String) As Boolean
        Dim bReturn As Boolean = False
        Try


            iPrevPercentPoint = 0
            iNoOfRecords = 0
            sCurrFileName = sFilenameToExp

            oFileLoader = New clsUVData

            oFileLoader.sUVFileName = sFilenameToExp
            oFileLoader.sSelectQueryCondition = sSelectParams
            oFileLoader.sDictPrefix = sDictPrefix

            oFileLoader.sDestinationFilename = DestinationFolder & IIf(Right(DestinationFolder, 1) <> "\", "\", "").ToString & "Export_" & sCurrFileName

            For Each sFieldName In aValues
                oFileLoader.mcDictAttr.Add(sFieldName)
            Next

            If oFileLoader.LoadDictionary() Then
                bReturn = oFileLoader.LoadData()
            End If

        Catch ex As Exception
            ReportError(ex)
        End Try

        Return bReturn
    End Function





    'Public Function ProcessPropertyFile() As Boolean
    '    Dim bReturn As Boolean = False
    '    Try

    '        iPrevPercentPoint = 0
    '        iNoOfRecords = 0
    '        sCurrFileName = "PROPERTY"

    '        oFileLoader = New clsUVData

    '        oFileLoader.sUVFileName = sCurrFileName
    '        oFileLoader.sSelectQueryCondition = "IF A1 # ""V"" BY A0"

    '        oFileLoader.sDestinationFilename = DestinationFolder & IIf(Right(DestinationFolder, 1) <> "\", "\", "").ToString & "Export_" & sCurrFileName

    '        oFileLoader.mcDictAttr.Add("0") ' PROPERTY ID : 
    '        'FK to RENT.PROPERTY, HEATING, DOOR.ENTRY, ELEC.LIGHT, WINDOWS, ENERGY.EFFICIENCY, PLUMBING, ROOFS
    '        oFileLoader.mcDictAttr.Add("PROPERTY.STATUS") ' 1 : property status
    '        oFileLoader.mcDictAttr.Add("STREET.NAME") ' 2 : street name
    '        oFileLoader.mcDictAttr.Add("ADDRESS") ' 3 : ADDRESS VM delimited
    '        oFileLoader.mcDictAttr.Add("CURRENT.TENANCY.NO") ' 16 : Current tenancy : 
    '        'FK to TENANCY
    '        oFileLoader.mcDictAttr.Add("RENT.ACC.NUMBER") ' 17 : rent account number : 
    '        'FK to RENT.ACCOUNT, RENT.HEADERS
    '        oFileLoader.mcDictAttr.Add("MAIL.PROPERTY.ADDRESS") ' 18 : contact mail address
    '        oFileLoader.mcDictAttr.Add("PROPERTY.TEL.NO") ' 19 : property tel no
    '        oFileLoader.mcDictAttr.Add("PROPERTY.TYPE") ' 4 : property type
    '        oFileLoader.mcDictAttr.Add("PROPERTY.DESC.CODE") ' 5 : property desc code
    '        oFileLoader.mcDictAttr.Add("FINANCE.CODE") ' 6 : finance code
    '        oFileLoader.mcDictAttr.Add("FINANCE.CODE.DESC") ' 7 : finance code description
    '        oFileLoader.mcDictAttr.Add("SURVEYOR.PATCH.NO") ' 8 : Surveyors Patch Number
    '        oFileLoader.mcDictAttr.Add("PARENT.PROP.NO") ' 9 : Parent Property Number
    '        oFileLoader.mcDictAttr.Add("SUB.PRROPERTY.NOS") ' 10 : Sub Property Nos
    '        oFileLoader.mcDictAttr.Add("ESTATE.NO") ' 11 : estate number
    '        oFileLoader.mcDictAttr.Add("BLOCK.NO") ' 12 : block number
    '        oFileLoader.mcDictAttr.Add("DWELLING.NO") ' 13 : dwelling number : 
    '        'FK to PROPERTY
    '        oFileLoader.mcDictAttr.Add("LOCATION.NO") ' 14 : location number
    '        oFileLoader.mcDictAttr.Add("PAST.TENANCY.NOS") ' 16 : past tenancy numbers (VM) :
    '        'FK to Tenancy
    '        oFileLoader.mcDictAttr.Add("OCCUPIER.CODE") ' 15 : occupier code
    '        ' : 20 : (MV)
    '        oFileLoader.mcDictAttr.Add("TFR.APP.NUMBER") ' 21 : active transfer app number (VM) : 
    '        'FK to APPLICANTS
    '        oFileLoader.mcDictAttr.Add("CONT.PROP.NOS") ' 22 : contractor app nos
    '        oFileLoader.mcDictAttr.Add("HEIGHT.OF.BUILDING") ' 23 : building height (MD2)
    '        oFileLoader.mcDictAttr.Add("PROPERTY.FLOORS") ' 24 : property floors (MD0)
    '        oFileLoader.mcDictAttr.Add("FLOOR.OF.PROPERTY") ' 25 : floor property is on
    '        oFileLoader.mcDictAttr.Add("NO.DOUBLE.BEDROOMS") ' 26 : number of double bedrooms
    '        oFileLoader.mcDictAttr.Add("BATHROOM.LOCATION") ' 27 : location of bathroom
    '        oFileLoader.mcDictAttr.Add("KITCHEN.LOCATION") ' 28 : kitchen location
    '        oFileLoader.mcDictAttr.Add("W.C..LOCATION") ' 29 : location of WC
    '        oFileLoader.mcDictAttr.Add("LIVINGROOM.NOS") ' 30 : number of living rooms
    '        oFileLoader.mcDictAttr.Add("HALL.NOS") ' 31 : hall numbers
    '        oFileLoader.mcDictAttr.Add("UTILITY.NOS") ' 32 : number of utility rooms
    '        oFileLoader.mcDictAttr.Add("GARAGE.NOS") ' 33 : number of garages
    '        oFileLoader.mcDictAttr.Add("OUTBUILD.NOS") ' 34 : number of out buildings
    '        oFileLoader.mcDictAttr.Add("GARAGE.NOS") ' 33 : number of garages
    '        oFileLoader.mcDictAttr.Add("DATE.RTB.START") ' 37 : date right to buy started
    '        oFileLoader.mcDictAttr.Add("SOLD.OR.LEASED") ' 38 : sold or leased
    '        oFileLoader.mcDictAttr.Add("SHARED.OWNERSHIP") ' 39 : shared ownership
    '        oFileLoader.mcDictAttr.Add("DATE.SOLD.LEASED") ' 40 : date sold leased
    '        oFileLoader.mcDictAttr.Add("DATE.LEASE.END") ' 41 : date lease end
    '        oFileLoader.mcDictAttr.Add("SUIT.DISABLED") ' 42 : suitable for disabled
    '        oFileLoader.mcDictAttr.Add("MAINTENANCE.ITEMS") ' 43 : maintenance items
    '        oFileLoader.mcDictAttr.Add("DATE.DEMOLISH.DUE") ' 44 : date demolished / disposed due
    '        oFileLoader.mcDictAttr.Add("DATE.DEMOLISHED") ' 45 : date demolished / disposed done
    '        oFileLoader.mcDictAttr.Add("CURR.VOID.PROP.NO") ' 46 : current void property number : 
    '        'FK to VOIDS
    '        oFileLoader.mcDictAttr.Add("PROP.LEASED.FROM") ' 47 : property leased from
    '        oFileLoader.mcDictAttr.Add("DATE.END.LEASE.2") ' 48 : datte leased to
    '        oFileLoader.mcDictAttr.Add("COMMENTS") ' 49 : property comments
    '        oFileLoader.mcDictAttr.Add("DATE.LAST.DECOR") ' 50 : date last decorated
    '        oFileLoader.mcDictAttr.Add("DATE.NEXT.DECOR") ' 51 : date next decoration due
    '        oFileLoader.mcDictAttr.Add("OUTSTANDING.REQS") ' 52 : outstanding requests
    '        oFileLoader.mcDictAttr.Add("PRE.WORK.REQS") ' 53 : pre work requests (MV)
    '        oFileLoader.mcDictAttr.Add("JOB.ORDER.RAISED") ' 54 : job order raised (MV) :
    '        'FK to JOB.ORDER
    '        oFileLoader.mcDictAttr.Add("POST.INSPECT.REQS") ' 55 : post inspection requests (VM)
    '        oFileLoader.mcDictAttr.Add("GTEES.THIS.PROP") ' 56 : Job Order guarantees (VM)
    '        oFileLoader.mcDictAttr.Add("CONTRACTS") ' 57 :  contract ids (VM) : 
    '        'FK to CONTRACTS 
    '        oFileLoader.mcDictAttr.Add("SERVICE.ITEMS") ' 58 : Service Items (VM)
    '        oFileLoader.mcDictAttr.Add("MAINT.PROGRAMS") ' 59 : Planned Maintenance programs (VM)
    '        oFileLoader.mcDictAttr.Add("OUTSTANDING.WORK") ' 60 : Outstanding work (VM)
    '        oFileLoader.mcDictAttr.Add("COUNCIL.REF.NO") ' 61 : Council property reference number
    '        oFileLoader.mcDictAttr.Add("REQUEST.NOS") ' 62 : request numbers (VM)
    '        oFileLoader.mcDictAttr.Add("CONTRACT.GUARANTEE") ' 63 : Contract guarantee
    '        oFileLoader.mcDictAttr.Add("VOID.PROPERTY.NOS") ' 64 : void property numbers (MV)
    '        oFileLoader.mcDictAttr.Add("PROPERTY.AREA") ' 65 : property area
    '        oFileLoader.mcDictAttr.Add("RATING.AUTHORITY") ' 66 : Rating authority
    '        oFileLoader.mcDictAttr.Add("WATER.AUTHORITY") ' 67 : water authority
    '        oFileLoader.mcDictAttr.Add("ELECTR.DISTRICT") ' 68 : electricity district
    '        oFileLoader.mcDictAttr.Add("GAS.DISTRICT") ' 69 : gas district
    '        oFileLoader.mcDictAttr.Add("ESTATE.MANAGER") ' 70 : estate manager
    '        oFileLoader.mcDictAttr.Add("PROPERTY.ORIGIN") ' 71 : property origin
    '        oFileLoader.mcDictAttr.Add("ACCESS.FLOOR") ' 72 : floor of access to property
    '        oFileLoader.mcDictAttr.Add("EXT.ACCESS.STEPS") ' 73 : external steps to access
    '        oFileLoader.mcDictAttr.Add("INT.ACCESS.STEPS") ' 74 : internal steps from access
    '        oFileLoader.mcDictAttr.Add("LIFT.AVAILABLE") ' 75 : lift available
    '        oFileLoader.mcDictAttr.Add("FUT.TENANCY.NO") ' 76 : Future tenancy number
    '        oFileLoader.mcDictAttr.Add("FUT.VOID.NO") ' 77 : future void no
    '        oFileLoader.mcDictAttr.Add("BLOCK.FLOORS") ' 78 : floors in block 
    '        oFileLoader.mcDictAttr.Add("FUT.RENT.ACC.NO") ' 79 : future rent account no 
    '        oFileLoader.mcDictAttr.Add("TOTAL.AREA.OF.PROP") ' 80 : total arera of property (MD2) 
    '        oFileLoader.mcDictAttr.Add("SINGLE.BED.NO") ' 81 : number of single bedrooms
    '        oFileLoader.mcDictAttr.Add("DOUBLE.BED.NO") ' 82 : number of double bedrooms
    '        oFileLoader.mcDictAttr.Add("COUNCIL.TENANT") ' 83 : Council tenant 
    '        oFileLoader.mcDictAttr.Add("PARKING.SPACE.TYPE") ' 84 : parking space type 
    '        oFileLoader.mcDictAttr.Add("ADAPT.DISABLED") ' 85 : adapted for disabled 
    '        oFileLoader.mcDictAttr.Add("RATEABLE.VALUE") ' 86 : rateable value (MD2)
    '        oFileLoader.mcDictAttr.Add("DECORATION.GROUP") ' 87 : decoratIOn group 
    '        oFileLoader.mcDictAttr.Add("TV.AERIAL.TYPE") ' 88 : tv aerial type 
    '        oFileLoader.mcDictAttr.Add("WARDEN") ' 89 : warden 
    '        oFileLoader.mcDictAttr.Add("GARDEN.LOCATION") ' 90 : garden location type 
    '        oFileLoader.mcDictAttr.Add("COMMUNAL.SERVICES") ' 91 : communcal facilities (MV)
    '        oFileLoader.mcDictAttr.Add("BALCONY") ' 92 : balcony 
    '        oFileLoader.mcDictAttr.Add("DOOR.ENTRY.SYSTEM") ' 93 : door entry system 
    '        oFileLoader.mcDictAttr.Add("WATER.HEATING.TYPE") ' 94 : water heating type 
    '        oFileLoader.mcDictAttr.Add("SPACE.HEATING.TYPE") ' 95 : space heating type 
    '        oFileLoader.mcDictAttr.Add("SUPP.SPACE.HEATING") ' 96 : supplementary space heating type 
    '        oFileLoader.mcDictAttr.Add("A97") ' 97 : alarm type 
    '        oFileLoader.mcDictAttr.Add("LISTED.PROPERTY.NO") ' 98 : listed property.no 
    '        oFileLoader.mcDictAttr.Add("OTHER.FEATURES") ' 99 : other features not listed elsewhere 
    '        oFileLoader.mcDictAttr.Add("COOKING.FACILITIES") ' 100 : Cooking facilities 
    '        oFileLoader.mcDictAttr.Add("APP.NUMBERS") ' 101 : Housing application numbers (MV) : 
    '        'FK to APPLICANTS 
    '        oFileLoader.mcDictAttr.Add("BUILT.YEAR") ' 102 : Year of building 
    '        oFileLoader.mcDictAttr.Add("COUNCIL.OWNED") ' 103 : Property owned by Council 
    '        oFileLoader.mcDictAttr.Add("OAP.OCCUPIED") ' 104 : OAP occupied 
    '        oFileLoader.mcDictAttr.Add("SUIT.OAP.ONLY") ' 105 : Suitable for OAP only 
    '        oFileLoader.mcDictAttr.Add("DISABLED.OCCUPIED") ' 106 : Occupied by disabled person 
    '        oFileLoader.mcDictAttr.Add("RO.RESTRICTION") ' 107 : Rent Office restriction 
    '        oFileLoader.mcDictAttr.Add("RENT") ' 110 :  RENT (MD2) on property
    '        oFileLoader.mcDictAttr.Add("RATES") ' 111 :  rates (MD2) on property
    '        oFileLoader.mcDictAttr.Add("COMBINED") ' 112 : Combines charge (MD2) on property
    '        oFileLoader.mcDictAttr.Add("WATER.RATES") ' 113 :  water rates (MD2) on property
    '        oFileLoader.mcDictAttr.Add("HEATING") ' 114 : heating (MD2) on property
    '        oFileLoader.mcDictAttr.Add("MISCELLANEOUS") ' 115 : Miscallaneous charges (MD2) on property
    '        oFileLoader.mcDictAttr.Add("TOTAL.RENT") ' 116 : total rent (MD2) on property
    '        oFileLoader.mcDictAttr.Add("RENT.PERIOD") ' 117 : rent pereiod on property (see RENT.PROPERTY) 
    '        oFileLoader.mcDictAttr.Add("LIVING.ROOM.SIZES") ' 118 : Living room sizes (MD2)
    '        oFileLoader.mcDictAttr.Add("BEDROOM.SIZES") ' 119 : bedroom sizes (MD2)
    '        oFileLoader.mcDictAttr.Add("DINING.ROOM.AVAIL") ' 120 : Dining room available
    '        oFileLoader.mcDictAttr.Add("DINING.ROOM.SIZE") ' 121 : Dining room size (MD2)
    '        oFileLoader.mcDictAttr.Add("KITCHEN.AVAIL") ' 122 : Kitchen availalbe 
    '        oFileLoader.mcDictAttr.Add("KITCHEN.SIZE") ' 123 : Kitchen size 
    '        oFileLoader.mcDictAttr.Add("BATHROOM.AVAIL") ' 124 : Bathroom available 
    '        oFileLoader.mcDictAttr.Add("INTERNAL.WC") ' 125 : internal WC 
    '        oFileLoader.mcDictAttr.Add("EXTERNAL.WC") ' 126 : external WC 
    '        oFileLoader.mcDictAttr.Add("HOT.WATER") ' 128 : Hot water 
    '        oFileLoader.mcDictAttr.Add("TENANTS.NAMES") ' 132 : Names of tenant - note unreliable pattern (SMV) 
    '        oFileLoader.mcDictAttr.Add("MEMBERS.NAMES") ' 133 : Names of household members - note unreliable pattern (SMV) 
    '        oFileLoader.mcDictAttr.Add("HOUSING.BENEFIT") ' 137 : Housing benefit flag 
    '        oFileLoader.mcDictAttr.Add("A138") ' 138 : Permitted number of occupiers
    '        oFileLoader.mcDictAttr.Add("SCHEME.NUMBERS") ' 139 : Scheme number 
    '        oFileLoader.mcDictAttr.Add("NON-COUNCIL.RENT") ' 140 : Non Council tenant Rent (MD2) 
    '        oFileLoader.mcDictAttr.Add("NON-COUNCIL.RATES") ' 141 : non-council tenant rates
    '        oFileLoader.mcDictAttr.Add("NON-COUNCIL.COMB") ' 142 : non council tenant combined
    '        oFileLoader.mcDictAttr.Add("NON-COUNCIL.WATER") ' 143 : non council tenant water 
    '        oFileLoader.mcDictAttr.Add("NON-COUNCIL.HEAT") ' 144 : non council tenant heat 
    '        oFileLoader.mcDictAttr.Add("NON-COUNCIL.MISC") ' 145 : non council tenant misc 
    '        oFileLoader.mcDictAttr.Add("NON-COUNCIL.TOTAL") ' 146 : non council tenant total 
    '        oFileLoader.mcDictAttr.Add("CONTACT.PHONE") ' 147 : contact phone no 
    '        oFileLoader.mcDictAttr.Add("ALARM.SYSTEM") ' 148 : alarm system 
    '        oFileLoader.mcDictAttr.Add("SERVICE.CHARGE") ' 149 : service charge 
    '        oFileLoader.mcDictAttr.Add("NON-COUNCIL.ALARM") ' 150 : non council tenant alarm 
    '        oFileLoader.mcDictAttr.Add("NON-COUNCIL.SERVC") ' 151 : non council tenant service  
    '        oFileLoader.mcDictAttr.Add("DATE.BOILER") ' 152 : date boiler installed (D4)
    '        oFileLoader.mcDictAttr.Add("MAKE.OF.BOILER") ' 153 : make of boiler 
    '        oFileLoader.mcDictAttr.Add("GARAGE.RENT") ' 154 : garage rent 
    '        oFileLoader.mcDictAttr.Add("NON-COUNCIL.GARAGE") ' 155 : non council garage 
    '        oFileLoader.mcDictAttr.Add("TENANCY.CEASE.MESS") ' 160 : Tenancy ceased message 
    '        oFileLoader.mcDictAttr.Add("TENANCY.CREATION.M") ' 161 : Tenancy creation message (MV) 
    '        oFileLoader.mcDictAttr.Add("ARCHIVE.JOB.NUMBER") ' 173 : archive job numbers (MV) 
    '        oFileLoader.mcDictAttr.Add("CAPITAL.VALUE") ' 180 : Capital value 
    '        oFileLoader.mcDictAttr.Add("RENT.ZONE") ' 224 : rent zone 
    '        oFileLoader.mcDictAttr.Add("RENTAL.JOB.NUMBERS") ' 278 : Rental Job Numbers
    '        oFileLoader.mcDictAttr.Add("A279") ' 279 : (MV)  unidentified but has 8 digit numbers
    '        oFileLoader.mcDictAttr.Add("LAST.GAS.SERVICE") ' 295 : Last gas service (D4) 
    '        oFileLoader.mcDictAttr.Add("NEXT.GAS.SERVICE") ' 296 : Next gas service (D4) 
    '        oFileLoader.mcDictAttr.Add("DATE.AQUIRED") ' 316 : Date Property Aquired (D4) 

    '        If oFileLoader.LoadDictionary() Then
    '            bReturn = oFileLoader.LoadData()
    '        End If

    '    Catch ex As Exception
    '        ReportError(ex)
    '    End Try

    '    Return bReturn
    'End Function


    Public Sub New()

    End Sub

    Private Sub oFileLoader_AllRecordsProcessed(CallingObject As clsUVData) Handles oFileLoader.AllRecordsProcessed

        RaiseEvent FileCompleted(Me, CallingObject.sUVFileName)

    End Sub


    Private Sub oFileLoader_NoRecordsProcessed(CallingObject As clsUVData, sReason As String) Handles oFileLoader.NoRecordsProcessed
        RaiseEvent FileFailed(Me, CallingObject.sUVFileName, sReason)

    End Sub


    Private Sub oFileLoader_RecordProcessed(CallingObject As clsUVData, RecordAt As Integer) Handles oFileLoader.RecordProcessed
        Dim iStepCount As Integer

        Try
            iRecordNo = RecordAt
            iStepCount = Math.Floor(RecordAt / iNoOfRecords * iTotalSteps)
            If iStepCount <> iPrevPercentPoint Or Now.Second <> LastStepUpd.Second Then

                RaiseEvent UpdateStepCount(Me, iStepCount)
                iPrevPercentPoint = iStepCount
                LastStepUpd = Now
            End If
        Catch ex As Exception
            ReportError(ex)
        End Try

    End Sub


    Private Sub oFileLoader_RecordsSelected(CallingObject As clsUVData, RecordCount As Integer) Handles oFileLoader.RecordsSelected
        iNoOfRecords = RecordCount

        RaiseEvent FileSelectionCompleted(Me, CallingObject.sUVFileName, RecordCount)
        'RaiseEvent FileStarted(Me, CallingObject.sUVFileName)

    End Sub


    Public Sub ReportError(exin As Exception)
        Dim oErr As ErrorItem
        Try
            oErr = glErrors.NewError(exin)
            RaiseEvent evReportError(Me, oErr, oErr.ToString)

        Catch ex As Exception

        End Try
    End Sub

    Private Sub oFileLoader_PassMessage(CallingObject As clsUVData, sMessage As String) Handles oFileLoader.PassMessage

        Try
            RaiseEvent PassMessage(Me, sMessage)
        Catch ex As Exception
            ReportError(ex)
        End Try

    End Sub

    Private Sub oFileLoader_FileProcessingStarted(CallingObject As clsUVData) Handles oFileLoader.FileProcessingStarted
        Try
            RaiseEvent FileStarted(Me, CallingObject.sUVFileName)
        Catch ex As Exception
            ReportError(ex)
        End Try
    End Sub
End Class

