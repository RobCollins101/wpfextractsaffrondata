﻿Imports System.Collections.Generic
Imports System.IO
Imports IBMU2.UODOTNET

Public Class clsUVData


    Protected Friend mcDictItems As New SortedList(Of String, clsUVDictItem)

    'Protected Friend mcDataRecords As New List(Of Hashtable)

    Protected Friend sUVFileName As String = ""
    Protected Friend sSelectQueryCondition As String = ""
    Protected Friend sDictPrefix As String = ""
    Protected Friend mcDictAttr As New List(Of String)

    Protected Friend moUVDictAsFile As UniDictionary
    Protected Friend moUVDictItem As UniDynArray
    Protected Friend moUVSelItem As UniDynArray
    Protected Friend moUVFile As UniFile
    Protected Friend moUVRecord As UniDynArray
    Protected Friend moUVCommand As UniCommand
    Protected Friend sUVSelListName As String = ""

    Protected Friend sDestinationFilename As String = ""
    Protected Friend sFieldDelimiterUsed As String = "|"

    Protected Friend iUVRecordsSelected As Integer = 0
    Protected Friend iUVRecordPosition As Integer = 0
    Protected Friend dStartOfRecordProcessing As Date = Now

    Public Event FileProcessingStarted(CallingObject As clsUVData)
    Public Event PassMessage(CallingObject As clsUVData, sMessage As String)
    Public Event NoRecordsProcessed(CallingObject As clsUVData, sReason As String)
    Public Event RecordsSelected(CallingObject As clsUVData, RecordCount As Integer)
    Public Event RecordProcessed(CallingObject As clsUVData, RecordAt As Integer)
    Public Event AllRecordsProcessed(CallingObject As clsUVData)

    Public Event evReportError(CallingObject As clsUVData, oErr As ErrorItem, sReason As String)

    Protected Friend sUVResponse As String

    Protected Friend bAbort As Boolean = False

    Protected Friend Function LoadDictionary() As Boolean
        Dim sCommand As String, selListDict As UniSelectList
        Dim oDictRec As clsUVDictItem, iPos As Integer
        Dim bReturn As Boolean = False, oOthDictRec As clsUVDictItem
        Try
            RaiseEvent PassMessage(Me, "Loading dictionary items for " & sUVFileName)
            mcDictItems.Clear()
            If sUVFileName <> "" And OpenUV(gHousingSystem, gHousingAccount, gHousingUserID, gHousingPassword) Then
                moUVCommand = gUVCon.CreateUniCommand
                sCommand = "SSELECT DICT " & sUVFileName & " IF F1 = ""A"" ""S"" ""D"" "
                moUVCommand.Command = sCommand
                moUVCommand.Execute()

                'Do While UVCommand.Response = UVE_NOERROR Or UVCommand.CommandStatus = UVS_COMPLETE
                '    Select Case guvCommand.CommandStatus
                '        Case UVS_REPLY
                '            guvCommand.Reply((InputBox(guvCommand.Response, "Dictionary Selection - More input required")))
                '        Case UVS_COMPLETE
                '            Exit Do
                '    End Select
                'Loop

                moUVDictAsFile = gUVCon.CreateUniDictionary(sUVFileName)
                moUVDictAsFile.Open()

                If moUVDictAsFile.IsFileOpen Then

                    If sUVSelListName = "" Then sUVSelListName = "SELECT.OF." & sUVFileName

                    If moUVCommand.CommandAtSelected <> 0 Then
                        selListDict = gUVCon.CreateUniSelectList(0)
                        moUVDictAsFile.RecordID = selListDict.Next
                        Do Until selListDict.LastRecordRead
                            Try
                                moUVDictAsFile.Read()
                                moUVDictItem = moUVDictAsFile.Record

                                If moUVDictItem.Extract(1).StringValue = "A" Or moUVDictItem.Extract(1).StringValue = "S" Then

                                    If Not (IsNumeric(moUVDictAsFile.RecordID) And moUVDictAsFile.RecordID <> "" And moUVDictItem.Extract(2).StringValue <> moUVDictAsFile.RecordID) Then

                                        oDictRec = New clsUVDictItem
                                        oDictRec.UVDictName = moUVDictAsFile.RecordID
                                        oDictRec.DictType = moUVDictItem.Extract(1).StringValue
                                        oDictRec.AttrNo = moUVDictItem.Extract(2).StringValue
                                        oDictRec.ColumnHeading = moUVDictItem.Extract(3).StringValue
                                        oDictRec.IConv = Trim(moUVDictItem.Extract(7).StringValue)
                                        oDictRec.OConv = Trim(moUVDictItem.Extract(8).StringValue)
                                        If Text.RegularExpressions.Regex.IsMatch(oDictRec.UVDictName, "^A\d+$") Then
                                            oDictRec.UVDictNameOnExport = Replace(oDictRec.ColumnHeading, " ", "") & "~" & CInt(oDictRec.AttrNo).ToString("000")
                                        ElseIf IsNumeric(oDictRec.AttrNo) Then
                                            oDictRec.UVDictNameOnExport = Replace(StrConv(Replace(oDictRec.UVDictName, ".", " "), VbStrConv.ProperCase), " ", "") & "~" & CInt(Val(oDictRec.AttrNo)).ToString("000")
                                        Else
                                            oDictRec.UVDictNameOnExport = Replace(StrConv(Replace(oDictRec.UVDictName, ".", " "), VbStrConv.ProperCase), " ", "")
                                        End If
                                        If Not mcDictItems.ContainsKey(oDictRec.UVDictName) Then mcDictItems.Add(oDictRec.UVDictName, oDictRec)

                                    End If
                                End If
                            Catch ex As Exception
                                ReportError(ex)
                            End Try
                            moUVDictAsFile.RecordID = selListDict.Next
                            If bAbort Then Exit Do
                        Loop

                    End If
                    If selListDict IsNot Nothing Then selListDict.Dispose()
                    selListDict = Nothing

                End If
                'End If

                moUVCommand.Dispose()
                moUVCommand = Nothing

                If Not bAbort Then
                    For iPos = 0 To 300
                        'oDictRec = New clsUVDictItem
                        'oDictRec.UVDictName = iPos.ToString
                        'oDictRec.DictType = "A"
                        'oDictRec.AttrNo = iPos.ToString
                        'oDictRec.UVDictNameOnExport = "Attr" & iPos.ToString("000")
                        'If mcDictItems.ContainsKey("A" & iPos) Then
                        '    oOthDictRec = mcDictItems.Item("A" & iPos)
                        '    oDictRec.UVDictNameOnExport = oOthDictRec.UVDictNameOnExport
                        'End If
                        'If mcDictItems.ContainsKey(oDictRec.UVDictName) Then
                        '    mcDictItems.Item(oDictRec.UVDictName).UVDictNameOnExport = oDictRec.UVDictNameOnExport
                        'Else
                        '    mcDictItems.Add(oDictRec.UVDictName, oDictRec)
                        'End If
                        'If Not mcDictItems.ContainsKey(oDictRec.AttrNo.ToString) Then mcDictItems.Add(oDictRec.AttrNo.ToString, oDictRec)



                        oDictRec = New clsUVDictItem
                        oDictRec.UVDictName = iPos.ToString
                        oDictRec.DictType = "A"
                        oDictRec.AttrNo = iPos.ToString
                        oDictRec.UVDictNameOnExport = "Attr" & iPos.ToString("000")
                        If mcDictItems.ContainsKey("A" & iPos) Then
                            oOthDictRec = mcDictItems.Item("A" & iPos)
                            oDictRec.UVDictNameOnExport = oOthDictRec.UVDictNameOnExport & IIf(Right(oOthDictRec.UVDictNameOnExport, 3) = CInt(oOthDictRec.AttrNo).ToString("000"), "", "_" & CInt(oOthDictRec.AttrNo).ToString("000")).ToString
                            oDictRec.ColumnHeading = oOthDictRec.ColumnHeading
                            oDictRec.OConv = oOthDictRec.OConv
                            oDictRec.IConv = oOthDictRec.IConv
                        Else
                            For Each oOthDictRec In mcDictItems.Values
                                If oOthDictRec.AttrNo = iPos.ToString And (oOthDictRec.OConv = "" OrElse (Left(oOthDictRec.OConv, 1) <> "A" And Left(oOthDictRec.OConv, 1) <> "F" And Left(oOthDictRec.OConv, 1) <> "T")) Then
                                    oDictRec.UVDictNameOnExport = oOthDictRec.UVDictNameOnExport & IIf(Right(oOthDictRec.UVDictNameOnExport, 3) = CInt(oOthDictRec.AttrNo).ToString("000"), "", "_" & CInt(oOthDictRec.AttrNo).ToString("000")).ToString
                                    oDictRec.ColumnHeading = oOthDictRec.ColumnHeading
                                    oDictRec.OConv = oOthDictRec.OConv
                                    oDictRec.IConv = oOthDictRec.IConv
                                    Exit For
                                End If
                            Next
                        End If
                        If mcDictItems.ContainsKey(oDictRec.UVDictName) Then
                            mcDictItems.Item(oDictRec.UVDictName).UVDictNameOnExport = oDictRec.UVDictNameOnExport
                        Else
                            oDictRec.KeyField = (iPos = 0)
                            If Not mcDictItems.ContainsKey(oDictRec.UVDictName) Then mcDictItems.Add(oDictRec.UVDictName, oDictRec)

                        End If
                        If Not mcDictItems.ContainsKey(oDictRec.AttrNo.ToString) Then mcDictItems.Add(oDictRec.AttrNo.ToString, oDictRec)


                        If bAbort Then Exit For
                    Next

                    bReturn = True
                    RaiseEvent PassMessage(Me, "All dictionary items loaded")
                Else
                    RaiseEvent PassMessage(Me, "dictionary items loading aborted")

                End If
            End If
        Catch ex As Exception
            ReportError(ex)
        End Try
        Return bReturn
    End Function

    Public Function LoadData() As Boolean
        Dim sCommand As String, selListRecords As UniSelectList
        Dim oFileRec As Hashtable, iPos As Integer
        Dim sAttrKey As String, sValue As String, sRawValue As String, oDictItem As clsUVDictItem
        Dim bFilePrepared As Boolean = False, oWriterStream As IO.TextWriter = Nothing
        Dim sDataLine As String, aTemp As String(), iRecCountSave As Integer = 100
        Dim bReturn As Boolean = False

        Try
            If Not bAbort Then
                'mcDataRecords.Clear()
                If sUVFileName <> "" And OpenUV(gHousingSystem, gHousingAccount, gHousingUserID, gHousingPassword) Then
                    RaiseEvent FileProcessingStarted(Me)
                    moUVCommand = gUVCon.CreateUniCommand
                    sCommand = "SSELECT " & sUVFileName & " " & sSelectQueryCondition
                    moUVCommand.Command = sCommand
                    moUVCommand.Execute()

                    iUVRecordsSelected = moUVCommand.CommandAtSelected

                    'Do While UVCommand.Response = UVE_NOERROR Or UVCommand.CommandStatus = UVS_COMPLETE
                    '    Select Case guvCommand.CommandStatus
                    '        Case UVS_REPLY
                    '            guvCommand.Reply((InputBox(guvCommand.Response, "Dictionary Selection - More input required")))
                    '        Case UVS_COMPLETE
                    '            Exit Do
                    '    End Select
                    'Loop

                    moUVFile = gUVCon.CreateUniFile(sUVFileName)
                    moUVFile.Open()

                    sDataLine = ""

                    If bAbort Then
                        RaiseEvent PassMessage(Me, "File items loading aborted")

                    Else

                        If moUVFile.IsFileOpen Then

                            If sUVSelListName = "" Then sUVSelListName = "SELECT.OF." & sUVFileName

                            If moUVCommand.CommandAtSelected > 0 Then
                                RaiseEvent RecordsSelected(Me, iUVRecordsSelected)

                                selListRecords = gUVCon.CreateUniSelectList(0)
                                ''selListDict.GetList(UVSelListName)
                                'moUVSelItem = selListRecords.ReadList()
                                'If moUVSelItem.Length(1) < 1 Then
                                '    moUVSelItem = selListRecords.ReadList()
                                '    selListRecords = gUVCon.CreateUniSelectList(0)
                                '    'selListDict.GetList(0)

#Region "ProcessRecords"

                                dStartOfRecordProcessing = Now
                                moUVFile.RecordID = selListRecords.Next
                                Do Until selListRecords.LastRecordRead Or giErrorCountdown < 1
                                    Try
                                        If Not bAbort Then

                                            iUVRecordPosition += 1

                                            If moUVFile.RecordID <> "" Then

                                                moUVFile.Read()
                                                moUVRecord = moUVFile.Record

                                                oFileRec = New Hashtable
                                                oFileRec("0") = moUVFile.RecordID
                                                For Each sAttrKey In mcDictAttr
                                                    sValue = ""
                                                    If mcDictItems.ContainsKey(sAttrKey) Then

                                                        oDictItem = mcDictItems.Item(sAttrKey)
                                                        If oDictItem.AttrNo = "0" Then sRawValue = moUVFile.RecordID Else sRawValue = moUVRecord.Extract(CInt(oDictItem.AttrNo)).StringValue
                                                        If oDictItem.OConv <> "" Then sValue = gUVCon.Oconv(sRawValue, oDictItem.OConv) Else sValue = sRawValue
                                                    Else
                                                        If IsNumeric(sAttrKey) Then
                                                            If sAttrKey <= 0 Then
                                                                sRawValue = moUVFile.RecordID
                                                            Else
                                                                sRawValue = moUVRecord.Extract(CInt(Val(sAttrKey))).StringValue
                                                            End If
                                                            sValue = sRawValue
                                                        End If
                                                    End If
                                                    oFileRec(sAttrKey) = sValue

                                                    If bAbort Then Exit For

                                                Next
                                            End If
                                        End If
                                        If bAbort Then
                                            RaiseEvent PassMessage(Me, "File items loading aborted")
                                            Exit Do

                                        Else
                                            'mcDataRecords.Add(oFileRec)

                                            If moUVFile.RecordID <> "" Then
                                                If Not bFilePrepared And sDestinationFilename <> "" Then
                                                    bFilePrepared = True
                                                    If IO.File.Exists(sDestinationFilename) Then IO.File.Delete(sDestinationFilename)
                                                    oWriterStream = IO.File.CreateText(sDestinationFilename)
                                                    ReDim aTemp(mcDictAttr.Count - 1)
                                                    For iPos = 0 To mcDictAttr.Count - 1
                                                        sAttrKey = mcDictAttr(iPos)
                                                        If mcDictItems.ContainsKey(sAttrKey) Then
                                                            aTemp(iPos) = sDictPrefix & mcDictItems.Item(sAttrKey).UVDictNameOnExport
                                                        Else
                                                            aTemp(iPos) = sDictPrefix & sAttrKey & "~Unknown"
                                                        End If
                                                        If bAbort Then Exit For

                                                    Next
                                                    sDataLine = Join(aTemp, sFieldDelimiterUsed)
                                                    'sDataLine = Join(mcDictAttr.ToArray, sFieldDelimiterUsed)
                                                    oWriterStream.WriteLine(sDataLine)
                                                    oWriterStream.Flush()
                                                End If
                                                If oWriterStream IsNot Nothing Then
                                                    'sDataLine = sDataLine & IIf(sDataLine <> "", vbCrLf, "").ToString
                                                    sDataLine = ""
                                                    ReDim aTemp(mcDictAttr.Count - 1)
                                                    For iPos = 0 To mcDictAttr.Count - 1
                                                        sAttrKey = mcDictAttr(iPos)
                                                        aTemp(iPos) = oFileRec(sAttrKey)
                                                        If bAbort Then Exit For
                                                    Next

                                                    sDataLine &= Join(aTemp, sFieldDelimiterUsed)
                                                    oWriterStream.WriteLine(sDataLine)
                                                    If iUVRecordPosition Mod iRecCountSave = 0 Then
                                                        oWriterStream.Flush()
                                                        'sDataLine = ""
                                                    End If

                                                End If

                                            End If

                                            RaiseEvent RecordProcessed(Me, iUVRecordPosition)
                                        End If
                                    Catch ex As Exception
                                        If Not selListRecords.LastRecordRead Then
                                            RaiseEvent PassMessage(Me, "Error on record " & moUVFile.RecordID & vbCrLf & ex.Message)
                                            Try : moUVFile.RecordID = selListRecords.Next : Catch ex2 As Exception : End Try
                                        End If
                                        giErrorCountdown -= 1

                                    End Try

                                    moUVFile.RecordID = selListRecords.Next
                                Loop

                                If sDataLine <> "" Then
                                    oWriterStream.WriteLine(sDataLine)
                                    sDataLine = ""
                                End If
                                oWriterStream.Flush()

                                'End If
                                selListRecords.Dispose()
                                selListRecords = Nothing

#End Region

                                If oWriterStream IsNot Nothing Then
                                    oWriterStream.Close()
                                    oWriterStream = Nothing
                                End If
                                If Not bAbort Then
                                    bReturn = True

                                    RaiseEvent AllRecordsProcessed(Me)
                                End If
                            Else
                                RaiseEvent NoRecordsProcessed(Me, "No Records selected")
                            End If
                        Else
                            RaiseEvent NoRecordsProcessed(Me, "File " & sUVFileName & " could not be opened")

                        End If
                    End If
                    moUVCommand.Dispose()
                    moUVCommand = Nothing

                    moUVFile.Close()
                    moUVFile.Dispose()
                Else
                    If sUVFileName = "" Then
                        RaiseEvent NoRecordsProcessed(Me, "No filename provided")
                    Else

                        RaiseEvent NoRecordsProcessed(Me, "Universe was not opened")
                    End If

                End If
            End If
        Catch ex As Exception
            RaiseEvent NoRecordsProcessed(Me, "ERROR : " & ex.ToString)
            ReportError(ex)

        End Try
        Return bReturn
    End Function

    Public Sub ReportError(exin As Exception)
        Dim oErr As ErrorItem
        Try
            oErr = glErrors.NewError(exin)
            RaiseEvent evReportError(Me, oErr, oErr.ToString)

        Catch ex As Exception

        End Try
    End Sub

End Class

Public Class clsUVDictItem
    Protected Friend UVDictName As String = ""
    Protected Friend UVDictNameOnExport As String = ""
    Protected Friend ColumnHeading As String = ""
    Protected Friend DictType As String = ""
    Protected Friend AttrNo As String = ""
    Protected Friend IConv As String = ""
    Protected Friend OConv As String = ""
    Protected Friend Length As Integer = 200
    Protected Friend KeyField As Boolean = False
    Protected Friend HasData As Boolean = False


    Function ColumnHeadingClean() As String
        Dim sReturn As String
        sReturn = StrConv(UVDictNameOnExport.Replace(",", " ").Replace(":", " ").Replace("@", " ").Replace(UV_VM, " ").Replace(UV_SM, " ").Replace(Oth_VM, " ").Replace(Oth_SM, " ").Replace(".", " ").Replace("(", " ").Replace(")", " ").Replace("<", " ").Replace(">", " ").Replace("/", " ").Replace("\", " ").Replace("[", " ").Replace("]", " ").Replace("-", " ").Replace("+", " ").Replace("?", " ").Replace("'", " ").Replace("`", " ").Replace("""", " ").Replace("*", " ").Replace("#", " ").Replace("|", " ").Replace("$", " ").Replace("%", " ").Replace("^", " ").Replace("&", " ").Replace(",", " "), VbStrConv.ProperCase).Replace(" ", "")
        If Left(sReturn, 1) = "_" Then sReturn = AttrNo & sReturn
        If IsNumeric(Left(sReturn, 1)) Then sReturn = "Attr" & sReturn

        Return sReturn
    End Function
End Class
