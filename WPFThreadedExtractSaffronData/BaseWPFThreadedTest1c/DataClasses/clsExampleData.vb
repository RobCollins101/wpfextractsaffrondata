﻿Imports System.Threading

Public Class clsExampleListData
    Inherits SortedList(Of Integer, clsExampleRecordData)

    Public Event NewPrimeFound(iNumFound As Integer)
    Public sPrimeList As String = ""

    Function ProcessDataToResult() As String
        Dim sResult As String = ""

        If Me.Count = 0 Then GenerateValues()

        'For Each oKey In Me.Keys
        '    oVal = Me.Item(oKey)
        '    sResult = sResult & IIf(sResult <> "", vbCrLf, "").ToString & oVal.sValue2
        'Next

        sResult = sPrimeList

        Return sResult
    End Function


    Public Sub GenerateValues()
        Dim i As Integer, i2 As Integer, iTest As Integer, iMax As Integer

        Me.Clear()

        Me.Add(1, New clsExampleRecordData("1", "1 is prime"))
        Me.Add(2, New clsExampleRecordData("2", "2 is prime"))
        For i = 3 To 1000000 Step 2
            If i < 4 Then
                Me.Add(i, New clsExampleRecordData(i.ToString, i.ToString & " is prime"))
            Else
                iMax = Math.Sqrt(i)
                For i2 = 2 To Me.Count - 1
                    iTest = Me.Keys(i2)
                    If i Mod iTest = 0 Then Exit For
                    If iTest >= iMax Then
                        iTest = 0
                        Exit For
                    End If
                    iTest = 0
                Next
                If iTest = 0 Then
                    sPrimeList &= IIf(sPrimeList <> "", vbCrLf, "").ToString & i.ToString & " is prime"
                    Me.Add(i, New clsExampleRecordData(i.ToString, i.ToString & " is prime"))
                    RaiseEvent NewPrimeFound(i)
                End If
            End If
        Next

    End Sub


    Public Sub New()

    End Sub
End Class

Public Class clsExampleRecordData
    Public Property sValue As String
    Public Property sValue2 As String

    Public Sub New(Value1, Value2)
        sValue = Value1
        sValue2 = Value2
    End Sub

    Public Sub New()

    End Sub
End Class
