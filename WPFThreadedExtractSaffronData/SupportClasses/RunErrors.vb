Imports System
Public Class RunErrors
    '    Inherits ArrayList
    Inherits List(Of ErrorItem)

    Public Function NewError(ByVal ex As System.Exception) As ErrorItem
        Dim oNewErr As ErrorItem = Nothing
        If TypeOf ex Is Threading.ThreadAbortException Then
            Wait(10)
            'Application.DoEvents()
        Else
            oNewErr = New ErrorItem(ex)
            MyBase.Add(oNewErr)
            If bLocalTest Then MsgBox("ERROR " & oNewErr.ToString)

        End If
        Return oNewErr
    End Function

    Public Function NewError(ByVal ex As System.Exception, ByVal sExtra As String) As ErrorItem
        Dim oNewErr As ErrorItem = Nothing
        If TypeOf ex Is Threading.ThreadAbortException Then
            'My.Application.DoEvents()
            Wait(10)
        Else
            oNewErr = New ErrorItem(ex, sExtra)
            MyBase.Add(oNewErr)
            If bLocalTest Then MsgBox("ERROR " & oNewErr.ToString)
        End If

        Return oNewErr
    End Function


    Public Function NewError(ByVal ex As System.Exception, ByVal aExtra As ArrayList) As ErrorItem
        Dim oNewErr As ErrorItem = Nothing
        If TypeOf ex Is Threading.ThreadAbortException Then
            'My.Application.DoEvents()
            Wait(10)
        Else
            oNewErr = New ErrorItem(ex, aExtra)
            MyBase.Add(oNewErr)
            If bLocalTest Then MsgBox("ERROR " & oNewErr.ToString)
        End If

        Return oNewErr
    End Function


    Public Function NewError(ByVal sModuleName As String, ByVal sFunctionName As String, ByVal lLineNo As Long, ByVal sMessage As String, _
            ByVal sErrorType As String, ByVal sExtra As String) As ErrorItem
        Dim oNewErr As New ErrorItem()

        oNewErr.sModule = sModuleName
        oNewErr.sFunction = sFunctionName
        If lLineNo > 0 Then oNewErr.sLineNo = lLineNo
        oNewErr.sErrorMsg = sMessage
        oNewErr.sErrorType = sErrorType
		oNewErr.sExtraDetail = sExtra

        MyBase.Add(oNewErr)
        If bLocalTest Then MsgBox("ERROR " & oNewErr.ToString)

        Return oNewErr
    End Function

    Public Sub WriteErrors(ByVal sErrorDest As String)
        Dim iMax As Integer, iPos As Integer, oError As ErrorItem
        Dim wrOut As System.IO.StreamWriter, sLine As String, sType As String

        Try

            If sErrorDest = "" Then sErrorDest = gsTempPath & "\" & My.Application.Info.AssemblyName & "ErrorsOut.txt"
            iMax = MyBase.Count - 1
            If iMax >= 0 Then
                wrOut = New System.IO.StreamWriter(sErrorDest, True)
                wrOut.WriteLine("------------------------------------------------------------------")
                wrOut.WriteLine("ERRORS - " & Format(Now(), "long"))
                For iPos = 0 To iMax
                    sType = MyBase.Item(iPos).GetType.ToString
                    'If sType = "wsTDCOpsCT.ErrorItem" Then
                    oError = MyBase.Item(iPos)
                    sLine = Now().ToString & vbCrLf & oError.ToString
                    wrOut.WriteLine(sLine)
                    'End If
                Next iPos
                wrOut.WriteLine("------------------------------------------------------------------")
                wrOut.Flush()
                wrOut.Close()
                wrOut.Dispose()
                wrOut = Nothing
            End If

            MyBase.Clear()

        Catch ex As Exception
            MsgBox(ex.ToString)

        End Try

    End Sub

    Public Sub ResetErrors(ByVal sErrorDest As String)
        'Dim wrOut As System.IO.StreamWriter, sLine As String, sType As String
        'Dim fs As System.IO.File

        If sErrorDest = "" Then sErrorDest = gsTempPath & "\" & My.Application.Info.AssemblyName & "_" & "ErrorsOut.txt"

        'fs.Delete(sErrorDest)
        IO.File.Delete(sErrorDest)

    End Sub
End Class
